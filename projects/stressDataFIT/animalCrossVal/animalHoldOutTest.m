%clear; close all;

directory = './L20/';
files = dir([directory '*.mat']);
files = {files.name};

XVals = linspace(0,1,1e3);
YVals = zeros(numel(XVals),numel(files),3);
AUCVals = zeros(numel(files),3);

for file = 1:numel(files)
    % load file
    load([directory files{file}]);
    
    %     modelRefitOpts = modelOpts;
    %     modelRefitOpts.W = data.nTrain(s,1);%sum(data.nTest);
    %     modelRefit = GP.LMC_FA(modelRefitOpts,dataOpts,model.kernel.LMCkernels);
    %     modelRefit.updateKernels = false;
    %
    %     % update model via resilient backpropagation (requires data.y as input)
    %     inds = find(data.sessionLabTrain==s);
    %     data.y = data.XfftTrain(:,:,inds);
    %
    %     data.labTest = data.labTrain(inds,:);
    %
    %     evals2 = algorithms.rprop(data.s,data,modelRefit,algOpts,plotOpts);
    %
    %     if saveOpts.saveOutput
    %         save(files{file});
    %         fprintf('Workspace saved ...\n');
    %     end
    
    scoresRefit = exp(modelRefit.logScores);
    
    labVals = 1:3;
    for lab = labVals
        [~,score_svm] = predict(model.SVMModel{lab},scoresRefit');
        [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = ...
            perfcurve(data.labTest==lab,score_svm(:,2),'true');
        [~,inds] = unique(Xsvm);
        YVals(:,file,lab) = interp1(Xsvm(inds),Ysvm(inds),XVals);
        AUCVals(file,lab) = AUCsvm;
    end
end

colors = get(gca,'colororder');
alpha = .2;
labVals = 1:3;
figure,
for lab = labVals
    plot(XVals,sum(YVals(:,:,lab),2)/numel(files),'Color',colors(lab,:),'LineWidth',2)
    hold on
end
% for lab = labVals
%     for file = 1:numel(files)
%         plot(XVals,YVals(:,file,lab),'Color',min(colors(lab,:)+alpha,1))
%     end
% end
legend({'Homecage' 'FIT' 'FIT+CD1'},'Location','Best')
hold off
xlabel('False positive rate','FontSize',12)
ylabel('True positive rate','FontSize',12)
title('Average ROC for Heldout Mouse','FontSize',14)
drawnow
pause(.5)

figure,
boxplot(AUCVals,{'Homecage' 'FIT' 'FIT+CD1'})
title('AUC Box Plot for Heldout Mouse','FontSize',14)

fprintf('Average AUC: %d\n',sum(AUCVals(:))/numel(AUCVals))