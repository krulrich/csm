clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

files = dir('*.mat');
files = {files.name};

xVals = linspace(0,1,1e3);
labTestAll = [];
scoresRefitAll = [];
for file = 4%1:numel(files)
    % load file
    load(files{file});
    
%     modelRefitOpts = modelOpts;
%     modelRefitOpts.W = data.nTrain(s,1);%sum(data.nTest);
%     modelRefit = GP.LMC_FA(modelRefitOpts,dataOpts,model.kernel.LMCkernels);
%     modelRefit.updateKernels = false;
%     
%     % update model via resilient backpropagation (requires data.y as input)
%     inds = find(data.sessionLabTrain==s);
%     data.y = data.XfftTrain(:,:,inds);
%     
%     data.labTest = data.labTrain(inds,:);
%     
%     evals2 = algorithms.rprop(data.s,data,modelRefit,algOpts,plotOpts);
%     
%     if saveOpts.saveOutput
%         save(files{file});
%         fprintf('Workspace saved ...\n');
%     end
    
    labTestAll = [labTestAll; data.labTest];
    scoresRefitAll = [scoresRefitAll exp(modelRefit.logScores)];

end

labVals = 1:3;
figure,
for lab = labVals
    [~,score_svm] = predict(model.SVMModel{lab},scoresRefitAll');
    %[~,score_svm] = predict(cmodel,scoresRefit');
    [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = perfcurve(labTestAll==lab,score_svm(:,2),'true');
    plot(Xsvm,Ysvm,'LineWidth',2)
    hold on
end
legend({'Homecage' 'FIT' 'FIT+CD1'},'Location','Best')
hold off
xlabel('False positive rate','FontSize',12)
ylabel('True positive rate','FontSize',12)
title('ROC for 1-vs-All Classification by SVM','FontSize',14)
drawnow