XTrainTemp = reshape(abs(data.XfftTrain),[size(data.XfftTrain,1)*size(data.XfftTrain,2),size(data.XfftTrain,3)]);
[U,V] = nnmf(XTrainTemp,50);
XTrainTemp2 = V;%(:,1:10)';
for l = 1:numel(unique(data.labTrain))
    labels = -1*ones(size(data.labTrain));
    labels(data.labTrain == l) = 1;
    
    weights = (labels==1)*sum(labels==-1) + (labels==-1)*sum(labels==1);
    [scoresSVM,idx] = datasample(XTrainTemp2,numel(labels),2,'Weights',weights);
    labelsSVM = labels(idx);
    
    % 2) obtain the SVM weights on classifying factor scores
    cmodel = fitcsvm(scoresSVM',labelsSVM);%,'KernelFunction',opts.kernelFunction);
    model2.SVMModel{l} = cmodel;
end

XTestTemp = reshape(abs(data.XfftTest),[size(data.XfftTest,1)*size(data.XfftTest,2),size(data.XfftTest,3)]);
XTestTemp2 = pinv(U)*XTestTemp;

