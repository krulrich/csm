clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Load saved model
fileName = 'stressOutputAll_20160330_groups_r11';
load(strcat('../savedWorkspace/',fileName));
data = matfile(strcat('../dataMatfile/dataStore_',fileName));
dataPost = matfile(strcat('../cataMatfile/dataStorePost_',fileName));

%% Set save options
saveOpts.saveOutput = true;
saveOpts.saveFigures = false;
saveOpts.fileName = 'stressOutputAll_20160330_groups_r11_testMaternal_5';

%% Set data options
dataOptsMaternal = dataOpts;
dataOptsMaternal.chans = 1:5;
dataOptsMaternal.path = '/home2/kru2/data/neuroscience/lfp/StressMaternal';
dataOptsMaternal.type = 'MatSep';
D = dir([dataOptsMaternal.path '/LFPData']);
dataOptsMaternal.sessionNames = {D.name};
dataOptsMaternal.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOptsMaternal.sessionNames(3:2:end),'un',0), 'un',0);
dataOptsMaternal.sessionNames = dataOptsMaternal.sessionNames(9);%([1:6 8:12]);
dataOptsMaternal.sessions = numel(dataOptsMaternal.sessionNames); % number of sessions/animals/trials

%% Set load options
loadOptsMaternal.sampleData = true; % sample a subset of data windows
loadOptsMaternal.sampleFact = .5; % fraction of all data windows to sample
loadOptsMaternal.testSet = false; % split data into testing and training set
loadOptsMaternal.trainRatio = .7; % fraction of data used in training set
loadOptsMaternal.valRatio = .05; % fraction of data used in validation set
loadOptsMaternal.testRatio = .25; % fraction of data used in testing set
loadOptsMaternal.useMatFile = true; % use matfile to store large data
loadOptsMaternal.dataStoreName = sprintf('dataStoreMaternal_%s.mat',saveOpts.fileName);

%% Load and preprocess Data
dataMaternal = loadDataWindows(dataOptsMaternal,loadOptsMaternal);
fprintf('Loading maternal stress data complete ... \n')

% %% Test model on maternal dataset
% % create a new model for testing (since model form is data dependent)
% modelOptsMaternal = modelOpts;
% modelOptsMaternal.W = sum(dataMaternal.Wm);
% kernelsMaternal = model.kernel.copy(); % perform a full copy of model
% modelMaternal = GP.LMC_FA(modelOptsMaternal,dataOpts,kernelsMaternal.LMCkernels);
% modelMaternal.keepChannels(1:5);
% for l = 1:modelOptsMaternal.L
%     modelMaternal.LMCkernels{l}.normalizeCovariance(); 
% end
% modelMaternal.updateKernels = false;
% clear kernelsMaternal
% 
% % update model via resilient backpropagation (requires data.y as input)
% dataMaternal.y = dataMaternal.Xfft;
% evals2 = algorithms.rprop(dataMaternal.s,dataMaternal,modelMaternal, ...
%     algOpts,plotOpts);
% 
% if saveOpts.saveOutput
%     save(saveOpts.fileName);
%     fprintf('Workspace saved ...\n');
% end
% 
% %% Show factor scores on testing set
% scoresMaternal = exp(modelMaternal.logScores);
% figure, imagesc(min(scoresMaternal,2)), colorbar
% xlabel('Window number (5 sec)')
% ylabel('Factor number')
% title('Heldout Factor Scores over Time','FontSize',14)
% drawnow
% 
% %% Analysis
% scoreAvgMaternal = zeros(dataOptsMaternal.sessions,modelOpts.L,3);
% scoreMedMaternal = zeros(dataOptsMaternal.sessions,modelOpts.L,3);
% for l = 1:modelOpts.L
%     for sess = 1:dataOptsMaternal.sessions
%         indsSess = find(dataMaternal.sessionLab==sess);
%         for t = 1:3
%             indsLab = find(dataMaternal.lab==t);
%             inds = intersect(indsSess,indsLab);
%             scoreAvgMaternal(sess,l,t) = sum(scoresMaternal(l,inds))/numel(inds);
%             scoreMedMaternal(sess,l,t) = median(scoresMaternal(l,inds));
%         end
%     end
% end
% 
% svmModel = 2;
% [~,scoreSVM] = predict(model.SVMModel{svmModel}, scoreMedMaternal(:,1:6,3));
% [~,ind] = sort(scoreSVM(:,2));
% 
% results = cell(dataOptsMaternal.sessions,2);
% for sess = 1:dataOptsMaternal.sessions
%     results{sess,1} = dataOptsMaternal.sessionNames{sess};
%     results{sess,2} = scoreSVM(sess,2);
% end
% results(ind,:)
