function data = loadDataWindows(dataOpts,loadOpts)

if loadOpts.useMatFile
    delete(loadOpts.dataStoreName); % remove previous data storage on hd
    data = matfile(loadOpts.dataStoreName,'Writable',true);
end

if ~isfield(dataOpts,'type')
    dataOpts.type = 'PredictionIso';
end

addpath(genpath(dataOpts.path))

idxE = 0;
idxValE = 0;
idxTestE = 0;

for sess = 1:dataOpts.sessions
    % get mouse name (session name)
    dataOpts.mouseName = dataOpts.sessionNames{sess};
    fprintf('Loading data for %s (%d/%d) ...\n', ...
        dataOpts.mouseName,sess,dataOpts.sessions)
    
    % load all data for session sess
    [X_home,regionNames] = ...
        loadStressData(dataOpts,strcat('_',dataOpts.type,'_Homecage_LFP'));
    [X_fit,~] = loadStressData(dataOpts,strcat('_',dataOpts.type,'_FIT_LFP'));
    load(strcat(dataOpts.mouseName,'_',dataOpts.type,'_FIT_TIME'));
    data.regionNames = regionNames(dataOpts.chans);
    
    % concatenate data and labels for session sess
    time_fit = ceil(INT_TIME/dataOpts.numSeconds);
    X = cat(3,X_home, ...
        X_fit(:,:,time_fit(1):(time_fit(1)+time_fit(2)-1)), ...
        X_fit(:,:,time_fit(3):(time_fit(3)+time_fit(4)-1)));
    labels = [ones(size(X_home,3),1); 2*ones(time_fit(2),1);  ...
        3*ones(time_fit(4),1)];
    
    % randomly sample a subset of data from session sess
    if loadOpts.sampleData
        W = size(X,3);
        weights = W*((labels==1)/sum(labels==1) + ...
            (labels==2)/sum(labels==2) + (labels==3)/sum(labels==3));
        inds = sort(datasample(1:W,ceil(loadOpts.sampleFact*W), ...
            'Replace',false,'Weights',weights));
        
        %inds = dividerand(size(X,3),loadOpts.sampleFact,0,1-loadOpts.sampleFact);
        X = X(:,:,inds);
        labels = labels(inds);
    end
    
    % take Fourier transform
    data.N = size(X,1);
    data.Ns = ceil(data.N/2);
    Xfft = 1/sqrt(data.N)*fft(X);
    Xfft = 2*(Xfft(2:data.Ns+1,:,:));
    
    % split data into training and testing sets
    if loadOpts.testSet
        [trainInd,valInd,testInd] = dividerand(size(X,3), ...
            loadOpts.trainRatio,loadOpts.valRatio,loadOpts.testRatio);
        data.nTrain(sess,1) = numel(trainInd);
        data.nVal(sess,1) = numel(valInd);
        data.nTest(sess,1) = numel(testInd);
        idxB = idxE + 1;
        idxValB = idxValE + 1;
        idxTestB = idxTestE + 1;
        idxE = idxB + data.nTrain(sess,1) - 1;
        idxValE = idxValB + data.nVal(sess,1) - 1;
        idxTestE = idxTestB + data.nTest(sess,1) - 1;
        if sess == 1
            data.XTrain = X(:,dataOpts.chans,trainInd);
            data.XVal = X(:,dataOpts.chans,valInd);
            data.XTest = X(:,dataOpts.chans,testInd);
            data.XfftTrain = Xfft(:,dataOpts.chans,trainInd);
            data.XfftVal = Xfft(:,dataOpts.chans,valInd);
            data.XfftTest = Xfft(:,dataOpts.chans,testInd);
        else
            data.XTrain(:,:,idxB:idxE) = X(:,dataOpts.chans,trainInd);
            data.XVal(:,:,idxValB:idxValE) = X(:,dataOpts.chans,valInd);
            data.XTest(:,:,idxTestB:idxTestE) = X(:,dataOpts.chans,testInd);
            data.XfftTrain(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,trainInd);
            data.XfftVal(:,:,idxValB:idxValE) = Xfft(:,dataOpts.chans,valInd);
            data.XfftTest(:,:,idxTestB:idxTestE) = Xfft(:,dataOpts.chans,testInd);
        end
        data.labTrain(idxB:idxE,1) = labels(trainInd);
        data.labVal(idxValB:idxValE,1) = labels(valInd);
        data.labTest(idxTestB:idxTestE,1) = labels(testInd);
        data.sessionLabTrain(idxB:idxE,1) = sess;
        data.sessionLabVal(idxValB:idxValE,1) = sess;
        data.sessionLabTest(idxTestB:idxTestE,1) = sess;
    else
        data.Wm(sess,1) = size(X,3);
        idxB = idxE + 1;
        idxE = idxB + data.Wm(sess,1) - 1;
        if sess == 1
            data.X = X(:,dataOpts.chans,:);
            data.Xfft = Xfft(:,dataOpts.chans,:);
        else
            data.X(:,:,idxB:idxE) = X(:,dataOpts.chans,:);
            data.Xfft(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,:);
        end
        data.lab(idxB:idxE,1) = labels;
        data.sessionLab(idxB:idxE,1) = sess;
    end
    
    clear X labels Xfft X_home X_fit time_fit
end

% store DFT frequency locations
data.sampRate = data.N/dataOpts.numSeconds;
data.tau = 0:1/data.sampRate:(dataOpts.numSeconds-1/data.sampRate);
data.s = (data.sampRate/data.N):(data.sampRate/data.N):(data.sampRate/2);

end