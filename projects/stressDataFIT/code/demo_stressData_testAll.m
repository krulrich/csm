clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Load saved model
fileName = 'stressOutputAll_20160330_groups_r11';
load(strcat('../savedWorkspace/',fileName));
data = matfile(strcat('../dataMatfile/dataStore_',fileName));
dataPost = matfile(strcat('../cataMatfile/dataStorePost_',fileName));

%% Set save options
saveOpts.saveOutput = true;
saveOpts.saveFigures = false;
saveOpts.fileName = 'stressOutputAll_20160330_groups_r11_testAll';

%% Set data options
dataOptsAll = dataOpts;
dataOptsAll.chans = 1:7;
dataOptsAll.path = '/home2/kru2/data/neuroscience/lfp/StressData';
dataOptsAll.type = 'PredictionIso';
D = dir([dataOptsAll.path '/LFPData']);
dataOptsAll.sessionNames = {D.name};
dataOptsAll.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOptsAll.sessionNames(3:2:end),'un',0), 'un',0);
%dataOptsAll.sessionNames = dataOptsAll.sessionNames(1:2);
dataOptsAll.sessions = numel(dataOptsAll.sessionNames);

dataOptsAll2 = dataOptsAll;
dataOptsAll2.path = '/home2/kru2/data/neuroscience/lfp/StressDataPost';
D = dir([dataOptsAll2.path '/LFPData']);
dataOptsAll2.sessionNames = {D.name};
dataOptsAll2.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOptsAll2.sessionNames(3:2:end),'un',0), 'un',0);
%dataOptsAll2.sessionNames = dataOptsAll2.sessionNames(1:2);
dataOptsAll2.sessions = numel(dataOptsAll2.sessionNames);

%% Set load options
loadOptsAll.sampleData = false; % sample a subset of data windows
loadOptsAll.testSet = false; % split data into testing and training set
loadOptsAll.useMatFile = true; % use matfile to store large data
loadOptsAll.dataStoreName = sprintf('dataStoreAll_%s.mat',saveOpts.fileName);

loadOptsAll2.sampleData = false; % sample a subset of data windows
loadOptsAll2.testSet = false; % split data into testing and training set
loadOptsAll2.useMatFile = true; % use matfile to store large data
loadOptsAll2.dataStoreName = sprintf('dataStoreAll2_%s.mat',saveOpts.fileName);

%% Load and preprocess Data
dataAll = loadDataWindows(dataOptsAll,loadOptsAll);
fprintf('Loading pre stress data complete ... \n')

dataAll2 = loadDataWindows(dataOptsAll2,loadOptsAll2);
fprintf('Loading post stress data complete ... \n')

%% Test model on datasets
% create a new model for testing (since model form is data dependent)
modelOptsAll = modelOpts;
modelOptsAll.W = sum(dataAll.Wm) + sum(dataAll2.Wm);
modelAll = GP.LMC_FA(modelOptsAll,dataOpts,model.kernel.LMCkernels);
modelAll.updateKernels = false;

% update model via resilient backpropagation (requires data.y as input)
dataAll.y = cat(3, dataAll.Xfft, dataAll2.Xfft);
evals2 = algorithms.rprop(dataAll.s,dataAll,modelAll,algOpts,plotOpts);

if saveOpts.saveOutput
    save(saveOpts.fileName);
    fprintf('Workspace saved ...\n');
end

%% Show factor scores on testing set
scoresAll = exp(modelAll.logScores);
figure, imagesc(min(scoresAll,2)), colorbar
xlabel('Window number (5 sec)')
ylabel('Factor number')
title('Heldout Factor Scores over Time','FontSize',14)
drawnow
