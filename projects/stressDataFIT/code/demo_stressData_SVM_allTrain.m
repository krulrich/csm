clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Set save options
saveOpts.saveOutput = true;
saveOpts.saveFigures = false;
saveOpts.fileName = 'stressOutputAll_20160330_groups_r12';

%% Set data options
dataOpts.numSeconds = 5;
dataOpts.highFreq = 50;
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5;
dataOpts.chans = 1:7; % index of channels to consider
dataOpts.dataSet = 'stressDataFIT';
dataOpts.path = '/home2/kru2/data/neuroscience/lfp/StressData';
%dataOpts.path = '/home/kru2/Documents/data/neuroscience/lfp/StressData';    
D = dir([dataOpts.path '/LFPData']);
dataOpts.sessionNames = {D.name};
dataOpts.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOpts.sessionNames(3:2:end),'un',0), 'un',0);
%dataOpts.sessionNames = dataOpts.sessionNames(1:3);
dataOpts.sessions = numel(dataOpts.sessionNames); % number of sessions/animals/trials

dataOptsPost = dataOpts;
dataOptsPost.path = '/home2/kru2/data/neuroscience/lfp/StressDataPost';
D = dir([dataOptsPost.path '/LFPData']);
dataOptsPost.sessionNames = {D.name};
dataOptsPost.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOptsPost.sessionNames(3:2:end),'un',0), 'un',0);
%dataOptsPost.sessionNames = dataOptsPost.sessionNames(1:15);
dataOptsPost.sessions = numel(dataOptsPost.sessionNames); % number of sessions/animals/trials

%% Set load options
loadOpts.sampleData = true; % sample a subset of data windows
loadOpts.sampleFact = .5; % fraction of all data windows to sample
loadOpts.testSet = true; % split data into testing and training set
loadOpts.trainRatio = .7; % fraction of data used in training set
loadOpts.valRatio = .05; % fraction of data used in validation set
loadOpts.testRatio = .25; % fraction of data used in testing set
loadOpts.useMatFile = true; % use matfile to store large data
loadOpts.dataStoreName = sprintf('dataStore_%s.mat',saveOpts.fileName);

loadOptsPost.sampleData = true; % sample a subset of data windows
loadOptsPost.sampleFact = .5; % fraction of all data windows to sample
loadOptsPost.testSet = true; % split data into testing and training set
loadOptsPost.trainRatio = .7; % fraction of data used in training set
loadOptsPost.valRatio = .05; % fraction of data used in validation set
loadOptsPost.testRatio = .25; % fraction of data used in testing set
loadOptsPost.useMatFile = true; % use matfile to store large data
loadOptsPost.dataStoreName = sprintf('dataStorePost_%s.mat',saveOpts.fileName);

%% Load and preprocess Data
data = loadDataWindows(dataOpts,loadOpts);
fprintf('Loading pre-stress data complete ... \n')

dataPost = loadDataWindows(dataOptsPost,loadOptsPost);
fprintf('Loading post-stress data complete ... \n')

%%
intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;

labTemp = cat(1,data.labTrain,dataPost.labTrain);
sessLabTemp = cat(1,-1*data.sessionLabTrain,dataPost.sessionLabTrain);

strGroup = ismember(sessLabTemp,find(intRatio > 0));
ctrlGroup = ismember(sessLabTemp,find(intRatio == 0));
resilGroup = ismember(sessLabTemp,find(intRatio > 1));
nonResilGroup = ismember(sessLabTemp,find(intRatio > 0 & intRatio < 1));

%% Train model on training set
% create a model
modelOpts.C = numel(dataOpts.chans); % number of channels
modelOpts.Q = 20; % number of Gaussian components in SM kernel
modelOpts.R = 2; % rank of each coregionalization matrix
modelOpts.L = 25; % number of factors
modelOpts.W = sum(data.nTrain) + sum(dataPost.nTrain); % total number of windows
modelOpts.maxW = 500; % maximum number of windows in a partition
modelOpts.lambda = 50;%20; % lagrange multiplier (SVM tuning parameter)
model = GP.LMC_FA_SVM_v3(modelOpts,dataOpts); % create model

% update model via resilient backpropagation
data.y = cat(3,data.XfftTrain,dataPost.XfftTrain); % model requires data.y as input (use training data)
algOpts.kernelFunction = 'linear';
%algOpts.labels = cat(1,data.labTrain,dataPost.labTrain);

% algOpts.labels = -1*ones(numel(labTemp),7);
% algOpts.labels(labTemp==1,1) = 1;
% algOpts.labels(labTemp==2,2) = 1;
% algOpts.labels(labTemp==3,3) = 1;
% algOpts.labels(:,4:7) = 0;
% algOpts.labels(strGroup,4) = 1;
% algOpts.labels(ctrlGroup,4) = -1;
% algOpts.labels(strGroup,5) = -1;
% algOpts.labels(ctrlGroup,5) = 1;
% algOpts.labels(resilGroup,6) = 1;
% algOpts.labels(nonResilGroup,6) = -1;
% algOpts.labels(resilGroup,7) = -1;
% algOpts.labels(nonResilGroup,7) = 1;


% algOpts.labels = zeros(numel(labTemp),8);
% algOpts.labels(:,1) = -1;
% algOpts.labels(strGroup,1) = 1;
% algOpts.labels(ctrlGroup,1) = -1;
% algOpts.labels(:,2) = -1 * algOpts.labels(:,1);
% algOpts.labels(:,3) = algOpts.labels(:,1) .* (labTemp>1);
% algOpts.labels(:,4) = algOpts.labels(:,2) .* (labTemp>1);
% algOpts.labels(:,5) = algOpts.labels(:,1) .* (labTemp>2);
% algOpts.labels(:,6) = algOpts.labels(:,2) .* (labTemp>2);
% algOpts.labels(resilGroup,7) = 1;
% algOpts.labels(nonResilGroup,7) = -1;
% algOpts.labels(:,8) = -1 * algOpts.labels(:,7);

algOpts.labels = zeros(numel(labTemp),3);
algOpts.labels(:,1) = -1;
algOpts.labels(strGroup,1) = 1;
algOpts.labels(ctrlGroup,1) = -1;
%algOpts.labels(:,2) = algOpts.labels(:,1) .* (labTemp==2);
%algOpts.labels(:,3) = algOpts.labels(:,1) .* (labTemp==3);
algOpts.labels(nonResilGroup,2) = 1;
algOpts.labels(resilGroup,2) = -1;
%algOpts.labels(:,5) = algOpts.labels(:,4) .* (labTemp==2);
%algOpts.labels(:,6) = algOpts.labels(:,4) .* (labTemp==3);
algOpts.labels(:,3:4) = -1;
algOpts.labels(labTemp==2,3) = 1;
algOpts.labels(labTemp==3,4) = 1;



algOpts.iters = 250;
algOpts.evalInterval = 10;
plotOpts.plot = false;
if plotOpts.plot
     figure(2);
     plotOpts.color = 'k';
     plotOpts.legendNames = {'CSM Kernel'};
     plotOpts.ax = gca;
end
evals = algorithms.rprop(data.s,data,model,algOpts,plotOpts);

if saveOpts.saveOutput
    save(saveOpts.fileName);
    fprintf('Workspace saved ...\n');
end

%% Show some model details on training data
% plot factor scores on training set
scores = exp(model.kernel.logScores);
if plotOpts.plot
     figure, imagesc(min(scores,2)), colorbar
     xlabel('Window number (5 sec)')
     ylabel('Factor number')
     title('Factor Scores over Time','FontSize',14)
end

% plot inferred cross-spectrum for single window
if plotOpts.plot
     figure, model.kernel.plotCsd(1,'minFreq',0,'maxFreq',30,'names',data.regionNames)
end

%% Test model on heldout testing set
% create a new model for testing (since model form is data dependent)
modelRefitOpts = modelOpts;
modelRefitOpts.W = sum(data.nTest) + sum(dataPost.nTest);
modelRefit = GP.LMC_FA(modelRefitOpts,dataOpts,model.kernel.LMCkernels);
modelRefit.updateKernels = false;

% update model via resilient backpropagation (requires data.y as input)
data.y = cat(3,data.XfftTest,dataPost.XfftTest);
evals2 = algorithms.rprop(data.s,data,modelRefit,algOpts,plotOpts);

if saveOpts.saveOutput
    save(saveOpts.fileName);
    fprintf('Workspace saved ...\n');
end

% stop script from showing future figures and analysis
if ~plotOpts.plot
     return;
end


%% Show factor scores on testing set
scoresRefit = exp(modelRefit.logScores);
figure, imagesc(min(scoresRefit,2)), colorbar
xlabel('Window number (5 sec)')
ylabel('Factor number')
title('Heldout Factor Scores over Time','FontSize',14)
drawnow

labRefitScore = zeros(modelRefitOpts.W,3);
for lab = 1:3
    [~,score_svm] = predict(model.SVMModel{lab},scoresRefit(end-4:end,:)');
    labRefitScore(:,lab) = score_svm(:,2);
end
[~,labTestHat] = max(labRefitScore,[],2);

Cmat = confusionmat(cat(1,data.labTest,dataPost.labTest),labTestHat);
figure
plotconfusion(util.oneHot(data.labTest),util.oneHot(labTestHat,1:3));
set(gca,'xticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''}, ...
    'yticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''})
h = gca;
h.YTickLabelRotation = 90;
drawnow

if saveOpts.saveFigures
    saveas(gcf,'confusionMatrix','pdf')
end

%%
labVals = 1:3;
figure,
for lab = labVals
    [~,score_svm] = predict(model.SVMModel{lab},scoresRefit(end-4:end,:)');
    %[~,score_svm] = predict(cmodel,scoresRefit');
    [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = perfcurve(cat(1,data.labTest,dataPost.labTest)==lab,score_svm(:,2),'true');
    plot(Xsvm,Ysvm,'LineWidth',2)
    hold on
end
legend({'Homecage' 'FIT' 'FIT+CD1'},'Location','Best')
hold off
xlabel('False positive rate','FontSize',12)
ylabel('True positive rate','FontSize',12)
title('ROC for 1-vs-All Classification by SVM','FontSize',14)
drawnow
pause(.5)
if saveOpts.saveFigures
    saveas(gcf,'ROCcurve','pdf')
end

%% Find most informative factors
[~,i] = sort(model.SVMModel{1}.Beta,1,'descend');
[~,j] = sort(model.SVMModel{2}.Beta,1,'descend');
[~,k] = sort(model.SVMModel{3}.Beta,1,'descend');
fprintf('Most informative factors:\n')
[i j k]

%% Display all kernel factors
lVals = 1:modelOpts.L;
for l = lVals
    figure('name',num2str(l),'units','normalized','outerposition',[0 0 1 1])
    model.kernel.LMCkernels{l}.plotCsd('minFreq',0,'maxFreq',20, ...
        'names',data.regionNames,'showAmplitude',false)
    %ax = subplot(7,7,(C-3)*C+2);
    %text(0.5,0.5,['Factor ' num2str(l)],'FontSize',16)
    %set( ax, 'visible', 'off')
    drawnow
    pause(.5)
    if saveOpts.saveFigures
        saveas(gcf, sprintf('factor%02d',l),'pdf')
    end
    close
end

%% Display covariance function
lvals = 1:modelOpts.L;
tauCorr = linspace(0,2,1e3);
for l = lvals
    figure
    plot(tauCorr,model.kernel.LMCkernels{l}.covFuns(tauCorr),'LineWidth',2)
    xlabel('Time (seconds)','FontSize',12)
    title(['Correlation Functions for Factor ' num2str(l)],'FontSize',14)
    legend(data.regionNames,0)
    drawnow
    pause(.5)
    if saveOpts.saveFigures
        saveas(gcf, sprintf('factor%02d_corrFun',l),'pdf')
    end
    close
end

%l = 6;
%figure, plot(data.tau,model.kernel.LMCkernels{l}.covFuns(data.tau))

%% Plot categories scatter plot
% fvals = [4,8,10,11];
% categories = {'Homecage','FIT','FIT+CD1'};
% f = figure;
% %figure('units','normalized','outerposition',[0 0 1 1])
% [h,axx] = gplotmatrix(scores(fvals,:)',[],categories(data.labTrain)','brg','.ox',[],'on', ...
%     'grpbars',cellfun(@(x) ['Factor ' num2str(x)],num2cell(fvals),'un',0));
% 
% ax = findobj(f,'Type','Axes');
% for i=1:length(ax)
%     handx = get(ax(i),'xlabel');
%     set(handx,'FontSize',14)
%     handy = get(ax(i),'ylabel');
%     set(handy,'FontSize',14)
% end
% %get(legend(axx(1,end))).FontSize = 14;
 

%% Plot factor scores
scores = exp(modelRefit.logScores);
lVals = 1:modelOpts.L;

for l = lVals
    figure('name',num2str(l))
    for sess = 1:dataOpts.sessions
        yvals = scores(l,data.sessionLabTest==sess);
        nWl = hist(data.labTest(find(data.sessionLabTest==sess),1),1:3);
        xvals = [linspace(0,1,nWl(1)) linspace(1.01,2,nWl(2)) linspace(2.01,3,nWl(3))];
        plot(xvals,yvals,'-','MarkerSize',10)
        hold on
    end
    plot([1,1],get(gca,'ylim'),'k-','LineWidth',2)
    plot([2,2],get(gca,'ylim'),'k-','LineWidth',2)
    hold off
    ylabel('Factor Score','FontSize',12)
    set(gca,'xtick',[.5,1.5,2.5],'xticklabel',{'Homecage','FIT','FIT+CD1'}, ...
        'FontSize',12)
    title(sprintf('Factor %02d Heldout Usage vs Normalized Time',l),'FontSize',14);
    
    drawnow
    pause(.5)
    if saveOpts.saveFigures
        saveas(gcf,sprintf('factor%02d_heldoutScores',l),'pdf')
    end
    close
end


%% Plot factor scores for each session
scores = exp(model.kernel.logScores);

for i = 1:dataOpts.sessions
    figure;
    sessScores = scores(:,data.sessionLabTrain==i);
    
    imagesc(dataOpts.numSeconds/60*(0:(size(sessScores,2)-1)),1:modelOpts.L, ...
        min(sessScores,1.2))
    hold on
    colorbar
    loc1 = dataOpts.numSeconds/60* ...
        find(data.labTrain(find(data.sessionLabTrain==i),1)==2,1,'first');
    loc2 = dataOpts.numSeconds/60* ...
        find(data.labTrain(find(data.sessionLabTrain==i),1)==3,1,'first');
    line([loc1 loc1],[0 99],'Color','r','LineWidth',2)
    line([loc2 loc2],[0 99],'Color','r','LineWidth',2)
    hold off
    
    xlabel('Time (minutes)','FontSize',12)
    ylabel('Factor number','FontSize',12)
    title(['Factor Scores over Time for ' dataOpts.sessionNames{i}],'FontSize',14)
    drawnow
    pause(.5)
    if saveOpts.saveFigures
        saveas(gcf,['scores_' dataOpts.sessionNames{i}],'pdf')
    end
    close
end


%% Plot factor scores
% W = sum(data.nTrain) + sum(data.nTest);
% scoresAll = zeros(L,W);
% scoresAll(:,data.trainInd) = scores;
% scoresAll(:,data.testInd) = scoresRefit;
% 
% for l=1:L
%     figure,
%     plot((0:(W-1))*dataOpts.numSeconds/60,scoresAll(l,:),'b-')
%     hold on
%     val1 = find(labels==2,1,'first')*dataOpts.numSeconds/60;
%     val2 = find(labels==3,1,'first')*dataOpts.numSeconds/60;
%     plot([val1,val1],get(gca,'ylim'),'r--')
%     plot([val2,val2],get(gca,'ylim'),'r--')
%     hold off
%     
%     xlim([0,20])
%     xlabel('Time (minutes)')
%     title(['Factor ' num2str(l) ' Scores'],'FontSize',12)
%     
%     saveas(gcf,['score' sprintf('%02d',l)],'pdf')
%     eval(sprintf('!pdfcrop score%02d.pdf score%02d.pdf',l,l))
%     close
% end

























%% Show factor scores on testing set
scoresRefitPost = exp(modelPost.logScores);
figure, imagesc(min(scoresRefitPost,2)), colorbar
xlabel('Window number (5 sec)')
ylabel('Factor number')
title('Heldout Factor Scores over Time','FontSize',14)

labRefitScore = zeros(modelPostOpts.W,3);
for lab = 1:3
    labRefitScore(:,lab) = scoresRefitPost'*model.SVMModel{lab}.Beta + model.SVMModel{lab}.Bias;
end
[~,labTestHat] = max(labRefitScore,[],2);

Cmat = confusionmat(dataPost.lab,labTestHat);
figure, plotconfusion(util.oneHot(dataPost.lab),util.oneHot(labTestHat,1:3));
set(gca,'xticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''}, ...
    'yticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''})
h = gca;
h.YTickLabelRotation = 90;
drawnow;
pause(.5);
if saveOpts.saveFigures
    saveas(gcf,'confusionMatrixPost','pdf')
end

%%

intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;


dataPostLab = dataPost.lab;
indsControl = ismember(dataPost.sessionLab,find(intRatio==0));
Cmat = confusionmat(dataPostLab(indsControl),labTestHat(indsControl));
figure, plotconfusion(util.oneHot(dataPostLab(indsControl)), ...
    util.oneHot(labTestHat(indsControl),1:3));
set(gca,'xticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''}, ...
    'yticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''})
title('Confusion Matrix for Control Group')
h = gca;
h.YTickLabelRotation = 90;
drawnow;
pause(.1);
if saveOpts.saveFigures
    saveas(gcf,'confusionMatrixControl','pdf')
end

indsStress = ismember(dataPost.sessionLab,find(intRatio~=0));
Cmat = confusionmat(dataPostLab(indsStress),labTestHat(indsStress));
figure, plotconfusion(util.oneHot(dataPostLab(indsStress)), ...
    util.oneHot(labTestHat(indsStress),1:3));
set(gca,'xticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''}, ...
    'yticklabel',{'Homecage' 'FIT' 'FIT+CD1' ''})
title('Confusion Matrix for Stress Group')
h = gca;
h.YTickLabelRotation = 90;
drawnow;
pause(.1);
if saveOpts.saveFigures
    saveas(gcf,'confusionMatrixStress','pdf')
end



%%

intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;



labVals = 1:3;
dataPostLab = dataPost.lab;
figure,
colors = get(gca,'colororder');
for lab = labVals
    [~,score_svm] = predict(model.SVMModel{lab},scoresRefit');
    [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = perfcurve(data.labTest==lab, ...
        score_svm(:,2),'true');
    plot(Xsvm,Ysvm,'LineStyle',':','Color',colors(lab,:),'LineWidth',1)
    hold on
    
    indsControl = ismember(dataPost.sessionLab,find(intRatio==0));
    [~,score_svm] = predict(model.SVMModel{lab},scoresRefitPost(:,indsControl)');
    [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = perfcurve(dataPostLab(indsControl)==lab, ...
        score_svm(:,2),'true');
    plot(Xsvm,Ysvm,'LineStyle','-','Color',colors(lab,:),'LineWidth',2)
    hold on
    
    indsStress = ismember(dataPost.sessionLab,find(intRatio~=0));
    [lab_svm(:,lab),score_svm] = predict(model.SVMModel{lab},scoresRefitPost(:,indsStress)');
    [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = perfcurve(dataPostLab(indsStress)==lab, ...
        score_svm(:,2),'true');
    plot(Xsvm,Ysvm,'LineStyle','--','Color',colors(lab,:),'LineWidth',2)
    hold on
end
drawnow;
pause(.1);
legend({'Homecage Pre' 'Homecage Ctrl' 'Homecage Str' 'FIT Pre' 'FIT Ctrl' ...
    'FIT Str' 'FIT+CD1 Pre' 'FIT+CD1 Ctrl' 'FIT+CD1 Str'},'Location','Best')
hold off
xlabel('False positive rate','FontSize',12)
ylabel('True positive rate','FontSize',12)
title('ROC for 1-vs-All Classification by SVM Post Stress','FontSize',14)

if saveOpts.saveFigures
    saveas(gcf,'ROCcurvePost','pdf')
end


%% Plot factor scores for each session
scoresRefitPost = exp(modelPost.logScores);

for i = 1:dataOptsPost.sessions
    figure;
    sessScores = scoresRefitPost(:,dataPost.sessionLab==i);
    
    imagesc(dataOptsPost.numSeconds/60*(0:(size(sessScores,2)-1)),1:modelOpts.L, ...
        min(sessScores,1.2))
    hold on
    colorbar
    loc1 = dataOptsPost.numSeconds/60* ...
        find(dataPost.lab(find(dataPost.sessionLab==i),1)==2,1,'first');
    loc2 = dataOptsPost.numSeconds/60* ...
        find(dataPost.lab(find(dataPost.sessionLab==i),1)==3,1,'first');
    line([loc1 loc1],[0 99],'Color','r','LineWidth',2)
    line([loc2 loc2],[0 99],'Color','r','LineWidth',2)
    hold off
    
    xlabel('Time (minutes)','FontSize',12)
    ylabel('Factor number','FontSize',12)
    title(['Factor Scores over Time for ' dataOptsPost.sessionNames{i}],'FontSize',14)
    drawnow
    pause(.5)
    if saveOpts.saveFigures
        saveas(gcf,['scores_' dataOptsPost.sessionNames{i}],'pdf')
    end
    close
end





%% Analysis!
%scores = min(exp(model.kernel.logScores),1.2);
%scoresPost = min(exp(modelPost.logScores),1.2);
scores = exp(model.kernel.logScores);
scoresPost = exp(modelPost.logScores);
scoresPost = bsxfun(@rdivide,scoresPost,sum(scoresPost,1));


intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;


lVals = 1:modelOpts.L;%[14 29 17 3 22 10 7 6 27 21 11 9];%:modelOpts.L;

scoreProp = zeros(dataOpts.sessions,numel(lVals));
for lInd = 1:numel(lVals)
    l = lVals(lInd);
    for sess = 1:dataOpts.sessions
        indsSess = find(data.sessionLabTrain==sess);
        indsLab1 = find(data.labTrain==1);
        indsLab2 = find(data.labTrain==2);
        indsLab3 = find(data.labTrain==3);
        inds1 = intersect(indsSess,indsLab1);
        inds2 = intersect(indsSess,indsLab2);
        inds3 = intersect(indsSess,indsLab3);
        scoreAvg1(sess,lInd) = sum(scores(l,inds1))/numel(inds1);
        scoreAvg2(sess,lInd) = sum(scores(l,inds2))/numel(inds2);
        scoreAvg3(sess,lInd) = sum(scores(l,inds3))/numel(inds3);
        scoreProp(sess,lInd) = scoreAvg2(sess,lInd) - scoreAvg1(sess,lInd);
    end
end

scorePropPost = zeros(dataOptsPost.sessions,numel(lVals));
for lInd = 1:numel(lVals)
    l = lVals(lInd);
    for sess = 1:dataOptsPost.sessions
        indsSess = find(dataPost.sessionLab==sess);
        indsLab1 = find(dataPost.lab==1);
        indsLab2 = find(dataPost.lab==2);
        indsLab3 = find(dataPost.lab==3);
        inds1 = intersect(indsSess,indsLab1);
        inds2 = intersect(indsSess,indsLab2);
        inds3 = intersect(indsSess,indsLab3);
        scoreAvg1Post(sess,lInd) = sum(scoresPost(l,inds1))/numel(inds1);
        scoreAvg2Post(sess,lInd) = sum(scoresPost(l,inds2))/numel(inds2);
        scoreAvg3Post(sess,lInd) = sum(scoresPost(l,inds3))/numel(inds3);
        scoreMed1Post(sess,lInd) = median(scoresPost(l,inds1),2);
        scoreMed2Post(sess,lInd) = median(scoresPost(l,inds2),2);
        scoreMed3Post(sess,lInd) = median(scoresPost(l,inds3),2);
        scorePropPost(sess,lInd) = scoreAvg3Post(sess,lInd) - scoreAvg2Post(sess,lInd);
    end
end

% scoresPost = exp(modelPost.logScores);
% for sess = 1:dataOptsPost.sessions
%     indsSess = find(dataPost.sessionLab==sess);
%     indsLab1 = find(dataPost.lab==1);
%     indsLab3 = find(dataPost.lab==3);
%     inds1 = intersect(indsSess,indsLab1);
%     inds3 = intersect(indsSess,indsLab3);
%     
%     [~,scoreSVM1] = predict(model.SVMModel{3},scoresPost(:,inds1)');
%     [~,scoreSVM3] = predict(model.SVMModel{3},scoresPost(:,inds3)');
%     scoreSVM1 = model.SVMModel{3}.Beta(lVals(1:9))'*scoresPost(lVals(1:9),inds1);
%     scoreSVM3 = model.SVMModel{3}.Beta(lVals(1:9))'*scoresPost(lVals(1:9),inds3);
%     
%     scoreAvg1 = sum( scoreSVM1(:,2) )/numel(inds1);
%     scoreAvg3 = sum( scoreSVM3(:,2) )/numel(inds3);
%     
%     scorePropPost(sess) = scoreAvg3 - scoreAvg1;
% end

% create cell array for intRatio
intRatioPreCell = repmat(cellstr('#'),dataOpts.sessions,1);
intRatioPostCell = cell(dataOptsPost.sessions,1);
for sess = 1:dataOptsPost.sessions
    mouseName = strsplit(dataOptsPost.sessionNames{sess},'_');
    mouseName = mouseName{1};
    
    miceOrigNames = cellfun(@(y)y{1}, ...
        cellfun(@(x)strsplit(x,'_'),dataOpts.sessionNames,'un',0),'un',0);
    
    indsC = strfind(miceOrigNames,mouseName);
    inds = find(not(cellfun('isempty', indsC)));
    
    if ~isempty(inds)
        if intRatio(sess) == 0
            intRatioPostCell{sess} = 'C';
            intRatioPreCell{inds(1)} = 'C';
        else
            intRatioPostCell{sess} = num2str(intRatio(sess),'%0.2f');
            intRatioPreCell{inds(1)} = num2str(intRatio(sess),'%0.2f');
        end
    else
        intRatioPostCell{sess} = ['*' num2str(intRatio(sess),'%0.2f')];
    end
end

% match scores before to post stress
scorePropMatch = zeros(size(scorePropPost));
intRatioPreIndex = zeros(dataOpts.sessions,1);
%scoreProp2Match = zeros(size(scoreProp2Post));
for sess = 1:dataOptsPost.sessions
    mouseName = strsplit(dataOptsPost.sessionNames{sess},'_');
    mouseName = mouseName{1};
    
    miceOrigNames = cellfun(@(y)y{1}, ...
        cellfun(@(x)strsplit(x,'_'),dataOpts.sessionNames,'un',0),'un',0);
    
    indsC = strfind(miceOrigNames,mouseName);
    inds = find(not(cellfun('isempty', indsC)));
    
    if ~isempty(inds)
        scorePropMatch(sess,:) = scoreProp(inds(1),:);
        intRatioPreIndex(inds(1)) = sess;
    else
        intRatioPreIndex(sess) = -1;
    end
end

X = scoreAvg3Post(intRatio~=0,21:25) - scoreAvg1Post(intRatio~=0,21:25);
y = intRatio(intRatio~=0);
[B FitInfo] = lasso(X,y,'Alpha',.01,'CV',19)
lassoPlot(B,FitInfo,'PlotType','CV');
figure, plot(X*B(:,FitInfo.IndexMinMSE),y,'kx')


X = scoreAvg1Post(intRatio~=0,:)./(scoreAvg2Post(intRatio~=0,:)+scoreAvg1Post(intRatio~=0,:));
y = intRatio(intRatio~=0);
[B FitInfo] = lasso(X,y,'Alpha',.01,'CV',19)
lassoPlot(B,FitInfo,'PlotType','CV');
figure, plot(X*B(:,FitInfo.IndexMinMSE),y,'kx')

mdl3 = stepwiselm(X,y); figure, plot(mdl3)
% 
% inds = [3 25 5 16 10];
% mdl2 = LinearModel.fit(scoreAvg3Post(intRatio~=0,inds) ./ ...
%     scoreAvg2Post(intRatio~=0,inds),intRatio(intRatio~=0))
% figure
% plot(mdl2)
% 
% %X = scorePropPost(intRatio~=0,:);
% fPlus = [5,16];%[2,3];%[5,6,8]; %[3 5 6 7];%
% fMinus = [3,25];%[1:4,7,9,10,11]; %[1 2 4 8 9];%
% %X = scorePropPost(intRatio~=0,fPlus)*model.SVMModel{3}.Beta(fPlus);
% X = sum(scorePropPost(intRatio~=0,fPlus),2) - sum(scorePropPost(intRatio~=0,fMinus),2);
% %X = scorePropPost(intRatio~=0,fPlus);
% Xcont = sum(scorePropPost(intRatio==0,fPlus),2) - sum(scorePropPost(intRatio==0,fMinus),2);
% Xpre = sum(scoreProp(:,fPlus),2) - sum(scoreProp(:,fMinus),2);
% %X = scoreProp(intRatio~=0,fMinus);
% y = intRatio(intRatio~=0);
% 
% mdl = LinearModel.fit(X(:,:),y(:))
% figure
% plot(mdl)
% title(['Interaction Ratio vs. Factor (' sprintf('+%02d',lVals(fPlus)) ...
%     sprintf('-%02d',lVals(fMinus)) ')'])




%hold on
%plot(Xcont,predict(mdl,Xcont),'o')
%plot(Xpre,predict(mdl,Xpre),'*')
%hold off



% drawnow
% pause(.5)
% if saveOpts.saveFigures
%     saveas(gcf,['fitRatio_' sprintf('+%02d',lVals(fPlus)) sprintf('-%02d',lVals(fMinus))],'pdf')
% end


% [~,inds] = sort(scorePropDiff);
% for i = 1:numel(inds)
%     ind = inds(i);
%     if intRatio(ind) ~= 0
%         fprintf('Score: %0.2f  Mouse: %s   IntRatio: %0.2f\n',scorePropDiff(ind), ...
%             dataOptsPost.sessionNames{ind}, intRatio(ind));
%     end
% end

%%
% cMat = scoreAvg1;
% intRatioCell = intRatioPreCell;
% sessNames = dataOpts.sessionNames;
% % cMat = scoreAvg3Post;
% % intRatioCell = intRatioPostCell;
% % sessNames = dataOptsPost.sessionNames;
% 
% taskStr = 'Pre-Homecage';
% 
% sig = 1/10;
% 
% simMat = exp(-squareform(pdist(cMat,'cosine'))/2/sig);
% cats = util.GCModulMax3(simMat);
% [~,inds] = sort(cats);
% 
% figure('Position', [100, 100, 839, 716]);
% imagesc(simMat(inds,inds))
% colorbar
% set(gca,'ytick',1:size(simMat,1), ...
%     'yticklabel',strrep(sessNames(inds),'_',''),'FontSize',8)
% for k=1:max(cats)
%     [~,i(k)] = max(sum(cMat(inds==k,:),1));
% end
% [~,xtickind] = unique(cats(inds),'first');
% %set(gca,'xtick',xtickind,'xticklabel',i)
% 
% util.xticklabel_rotate(1/2:size(simMat,1)-1/2,90,intRatioCell(inds),'FontSize',8)
% 
% xlabel('Interaction Ratio','FontSize',10)
% 
% title(['Grouped Similarity Matrix, ' taskStr],'FontSize',12)
% saveas(gcf,['similarity_' taskStr],'pdf')

%% Diffusion


scores = exp(model.kernel.logScores);
labs = data.labTrain(:,1);
labSess = data.sessionLabTrain(:,1);

scores = [scores exp(modelPost.logScores)];
labs = [labs; dataPost.lab(:,1)];
labSess = [-1*labSess; dataPost.sessionLab(:,1)];

%[~,k] = sort(abs(model.SVMModel{3}.Beta),1,'descend');
%scores = bsxfun(@rdivide,scores,sum(scores,1));
%scores = scores(k(1:8),:);

labGroup = [ones(sum(labSess < 0),1); ...
    2*(intRatio(labSess(labSess>0)) == 0) + 3*(intRatio(labSess(labSess>0)) > 0)];
%labs = dataPost.lab(1:3:end,1);
%labSess = dataPost.sessionLab(1:3:end,1);

sig = std(scores(:));%1/3; % set to standard deviation of examples
alpha = 1/2;
k=10;
t=1;
skip = 4; % only for computational purposes!
scoresSample = scores(:,1:skip:end);

%L = exp(-squareform(pdist(scoresSample','cosine'))/2/sig);
L = 2/pi*acos(squareform(pdist(scoresSample','cosine')));
D = diag(sum(L,2));
La = D^(-1/2)*L*D^(-1/2);
Da = diag(sum(La,2));
M = Da\La;
[phi,lambda,psi] = svd(M);

%PsiSample = psi(:,2:end)*(lambda(2:end,2:end).^t);

Psi = zeros(size(scores,2),k);
maxPer = 2000;
indB = 1;
while indB < size(scores,2)
    indE = min(indB + maxPer - 1, size(scores,2));
    % project new data into diffusion map
    %LNew = exp(-pdist2(scoresSample',scores(:,indB:indE)','cosine')/2/sig);
    LNew = 2/pi*acos(pdist2(scoresSample',scores(:,indB:indE)','cosine'));
    Psi(indB:indE,:) = ...
        (Da\(D^(-1/2)*LNew*diag(sum(LNew,1))^(-1/2)))'*phi(:,1:(k));
    indB = indE + 1;
end


%%

labSessUnique = unique(labSess);
PsiTaskAvg = zeros(numel(labSessUnique),3,size(Psi,2));
ScoreTaskAvg = zeros(numel(labSessUnique),3,size(scores,1));
for sInd = 1:numel(labSessUnique) % loop over session
    s = labSessUnique(sInd);
    for t = 1:3 % loop over task
        inds = (labs==t & labSess==s);
        PsiTaskAvg(sInd,t,:) = sum(Psi(inds,:),1)/sum(inds);
        ScoreTaskAvg(sInd,t,:) = sum(scores(:,inds),2)/sum(inds);
    end
    diffusionDist(sInd,1) = norm(squeeze(PsiTaskAvg(sInd,2,:) - PsiTaskAvg(sInd,3,:)));
    scoreDist(sInd,1) = pdist(squeeze(ScoreTaskAvg(sInd,[2,3],:)),'cosine');
end
figure, plot(intRatio,diffusionDist(44:end),'x')
figure, plot(intRatio,scoreDist(44:end),'x')

%
labGroupAnimal = [ones(43,1); 2+double(intRatio>0)];
figure
boxplot(diffusionDist,labGroupAnimal)

bins = linspace(0,max(diffusionDist),8);
dd1 = diffusionDist(1:43);
dd2 = diffusionDist(44:end);
sd1 = scoreDist(1:43);
sd2 = scoreDist(44:end);
y1 = hist(dd1,bins);
y2 = hist(dd2(intRatio==0),bins);
y3 = hist(dd2(intRatio>0),bins);
yAll = [y1;y2;y3]';
yAll = bsxfun(@rdivide,yAll,sum(yAll,1));
figure
bar(bins,yAll)
legend('Original','Control','Stress','Location','best')
title('Diffusion Distance Histogram','FontSize',14)
xlabel('Diffusion Distance','FontSize',12)
drawnow
pause(.5)
if saveOpts.saveFigures
    saveas(gcf,'diffusionDistanceHistogram','pdf')
end

% two sample t-test to find difference between means
[h,p] = ttest2(dd2(intRatio==0),dd2(intRatio>0))



ddDiff = dd1(intRatioPreIndex>0) - dd2(intRatioPreIndex(intRatioPreIndex>0));
irMatch = intRatio(intRatioPreIndex(intRatioPreIndex>0));
bins = linspace(min(ddDiff),max(ddDiff),8);
y1 = hist(ddDiff(irMatch==0),bins);
y2 = hist(ddDiff(irMatch>0),bins);
yAll = [y1;y2]';
yAll = bsxfun(@rdivide,yAll,sum(yAll,1));
figure
bar(bins,yAll)
legend('Control','Stress','Location','best')
title('Diffusion Distance Difference Histogram','FontSize',14)
xlabel('Diffusion Distance Difference','FontSize',12)
drawnow
pause(.5)
if saveOpts.saveFigures
    saveas(gcf,'diffusionDistanceDifferenceHistogram','pdf')
end

[h,p] = ttest2(ddDiff(irMatch==0),ddDiff(irMatch>0))


%%
figure
colorOrder = get(groot,'defaultAxesColorOrder');
nf = 5;
labSessUnique = unique(labSess);
for sInd = 1:numel(labSessUnique)%datasample(1:numel(labSessUnique),20) % loop over session
    s = labSessUnique(sInd);
    inds = (labSess == s);
    sPsi = Psi(inds,1:3);
    sPsiSmooth = filtfilt(1/nf*ones(1,nf),1,sPsi);
    
    sLabs = labs(inds);
    sLabsInd = zeros(3,numel(sLabs));
    sLabsInd(sub2ind(size(sLabsInd),sLabs',1:numel(sLabs))) = 1;
    sLabsSmooth = filter2(1/nf*ones(1,nf),sLabsInd);
    
    for n = 1:size(sPsiSmooth,1)-1
        plot3(sPsiSmooth(n:n+1,1),sPsiSmooth(n:n+1,2),sPsiSmooth(n:n+1,3), ...
            'Color', sLabsSmooth(:,n)'*colorOrder(1:3,:))%colors{sLabs(n)},'MarkerSize',10)
        hold on
    end
    
    
    %figure, plot3(sPsi(:,1),sPsi(:,2),sPsi(:,3))
    %hold on
    
    %plot3(sPsiSmooth(:,1),sPsiSmooth(:,2),sPsiSmooth(:,3))
    %hold on
end

%%

colors = {'r','b','g'};
styles = {'*','x','o'};
figure
for g = 1:3 % loop over groups
    for t = 1:3 % loop over tasks
        labsInds = labs==t;
        plot3(Psi(labGroup==g & labsInds,1), ...
            Psi(labGroup==g & labsInds,2), ...
            Psi(labGroup==g & labsInds,3),[colors{t} styles{g}])
        %plot(Psi(labGroup==g & labsInds,1), ...
        %    Psi(labGroup==g & labsInds,2),[colors{t} styles{g}])
        hold on
    end
end

% show interaction ratio as text
t = 1; % task
for i = 1:35
   if intRatio(i)~=0
       s = find(labSessUnique == i);
       text(PsiTaskAvg(s,t,1),PsiTaskAvg(s,t,2),PsiTaskAvg(s,t,3), ...
           num2str(intRatio(i)),'HorizontalAlignment','left','FontSize',12, ...
           'BackgroundColor','y');
   end
end

hold off




%for i = 1:35
%    if intRatio(i)~=0
%        text(Psi2(i,1),Psi2(i,2),Psi2(i,3),['   ' ...
%            num2str(intRatio(i))],'HorizontalAlignment','left','FontSize',12);
%    end
%end

legend({'Homecage' 'FIT' 'FIT+CD1'},0)
title('Embedding of Factor Scores in 3D Diffusion Space','FontSize',12)





sess=20;
PsiSess = Psi(labSess==sess,:);
labSessSess = labs(labSess==sess);
colors = {'r-*','b-*','g-*'};
figure
plot3(Psi(:,1),Psi(:,2),Psi(:,3),'k.','MarkerSize',6)
hold on
for n = 1:size(PsiSess,1)-1
    plot3(PsiSess(n:n+1,1),PsiSess(n:n+1,2),PsiSess(n:n+1,3),colors{labSessSess(n)},'MarkerSize',10)
    hold on
end
plot3(PsiTaskAvg(sess+43,:,1),PsiTaskAvg(sess+43,:,2),PsiTaskAvg(sess+43,:,3),'m*','MarkerSize',14)
sess=3;
PsiSess = Psi(labSess==sess,:);
labSessSess = labs(labSess==sess);
colors = {'r-o','b-o','g-o'};
for n = 1:size(PsiSess,1)-1
    plot3(PsiSess(n:n+1,1),PsiSess(n:n+1,2),PsiSess(n:n+1,3),colors{labSessSess(n)},'MarkerSize',10)
    hold on
end
plot3(PsiTaskAvg(sess+43,:,1),PsiTaskAvg(sess+43,:,2),PsiTaskAvg(sess+43,:,3),'m*','MarkerSize',14)

hold off

title('Evolution of Brain States in Diffusion Space for Two Mice','FontSize',12)


%%
colorOrder = uint8(255*get(groot,'defaultAxesColorOrder'));
diffMap3D = pointCloud(Psi(:,1:3),'Color',colorOrder(labs,:));
[diffMap3D,indsKeep] = ...
    pcdenoise(diffMap3D,'NumNeighbors',12,'Threshold',1);

figure
pcshow(diffMap3D,'MarkerSize',12)

K = boundary(diffMap3D.Location,.8);
figure(2), clf
trisurf(K, diffMap3D.Location(:,1), ...
    diffMap3D.Location(:,2), diffMap3D.Location(:,3), ...
    ones(size(diffMap3D.Location(:,1))));
colormap([.5 .5 .5]);
colorbar off
axis vis3d
axis off
l = light('Position',[40,-35,-20])
lighting phong
shading interp
colorbar EastOutside

% look into pcshowpair to compare two point clouds

%%
K = boundary(Psi(labs==1,1),Psi(labs==1,2),Psi(labs==1,3));
figure, trisurf(K,Psi(labs==1,1),Psi(labs==1,2),Psi(labs==1,3),labGroup(labs==1))
axis vis3d
axis off
l = light('Position',[-50 -15 29])
lighting none
shading interp
colorbar EastOutside


%%
dim1 = 2;
dim2 = 3;

% colors = {'r','b','g'};
% styles = {'*','x','o'};
% figure
% for g = [1,2,3] % loop over groups
%     for t = 1:3 % loop over tasks
%         labsInds = labs==t;
%         plot(1e3*Psi(labGroup==g & labsInds,dim1), ...
%             1e3*Psi(labGroup==g & labsInds,dim2), ...
%             [colors{t} styles{g}])
%         %plot(Psi(labGroup==g & labsInds,1), ...
%         %    Psi(labGroup==g & labsInds,2),[colors{t} styles{g}])
%         hold on
%     end
% end

figure
m = 64;
pad = 10;
nbins = 30;
cmap = [bone(m); summer(m); autumn(m)];
for t = 3:-1:1
    [n,c] = hist3(Psi(labs==t,[dim1,dim2]),[nbins,nbins]);
    n = imgaussfilt(padarray(n,[pad,pad]),1);
    cmin = min(n(:));
    cmax = max(n(:));
    C = min(m,round((m-1)*(n-cmin)/(cmax-cmin))+1) + (t-1)*m;
    xdiff = c{1}(2)-c{1}(1);
    xvals = linspace(c{1}(1)-pad*xdiff,c{1}(nbins)+pad*xdiff,nbins+2*pad);
    ydiff = c{2}(2)-c{2}(1);
    yvals = linspace(c{2}(1)-pad*ydiff,c{2}(nbins)+pad*ydiff,nbins+2*pad);
    contour(yvals,xvals,C','LineWidth',2);
    colormap(cmap)
    hold on
end

%axis(1e3*[min(Psi(:,dim1)) max(Psi(:,dim1)) ...
%    min(Psi(:,dim2)) max(Psi(:,dim2)) ] ...
%    + 2*[-1 1 -1 1])

% % show interaction ratio as text
% t = 1; % task
% for i = 1:35
%    if intRatio(i)~=0
%        s = find(labSessUnique == i);
%        text(1e3*PsiTaskAvg(s,t,dim1),1e3*PsiTaskAvg(s,t,dim2), ...
%            num2str(intRatio(i)),'HorizontalAlignment','left','FontSize',12, ...
%            'BackgroundColor','y');
%    end
% end

hold off
title('Embedding of Factor Scores in 2D Diffusion Space','FontSize',12)

CH = convhull(1e3*Psi(:,dim1),1e3*Psi(:,dim2));
nCH = numel(CH);
CH = CH(1:ceil(nCH/15):nCH-1);

freezeColors

for i = 1:numel(CH)
    % Place second set of axes on same plot
    handaxes2 = axes('Position', [.5+35*Psi(CH(i),dim1) .48+35*Psi(CH(i),dim2) 0.1 0.1]);%[.45+300*Psi(CH(i),dim1) .46+310*Psi(CH(i),dim2) 0.1 0.1]);
    colormap default
    labsPie = 1:size(scores,1);
    labsPie = cellstr(num2str(labsPie(:)));
    [~,inds] = sort(scores(:,CH(i)),'descend');
    for j = 5:size(scores,1)
        labsPie{inds(j)} = '';
    end
    pie(scores(:,CH(i))/sum(scores(:,CH(i))),labsPie)%.*exp(2i*pi*(1:30)'/30))
    
end


%%
y = intRatio(intRatio~=0);

stressGroupInds = find(intRatio~=0);
clear X
for ind = 1:numel(stressGroupInds)
    sess = stressGroupInds(ind);
    indsStress = ismember(dataPost.sessionLab,sess);
%     Cmat = confusionmat(dataPostLab(indsStress),labTestHat(indsStress));
%     Cmat = Cmat';
%     X(ind,1) = sum(diag(Cmat))/sum(sum(Cmat));
%     X(ind,2) = Cmat(1,1)/sum(Cmat(:,1));
%     X(ind,3) = Cmat(2,2)/sum(Cmat(:,2));
%     X(ind,4) = Cmat(3,3)/sum(Cmat(:,3));
%     X(ind,5) = Cmat(2,1)/sum(Cmat(:,1));
%     X(ind,6) = Cmat(2,1)/sum(Cmat(2,1:2));
%     X(ind,7) = Cmat(2,2)/sum(Cmat(2,:));
%     X(ind,8) = Cmat(3,3)/sum(Cmat(3,:));
%     X(ind,9) = Cmat(1,1)/sum(Cmat(1,:));
%     X(ind,10) = (Cmat(2,1) + Cmat(3,1))/(sum(Cmat(2,1:2)) + sum(Cmat(3,[1,3])));
%     X(ind,11) = sum(sum(Cmat(2:3,2:3)))/sum(sum(Cmat(:,2:3)));
%     X(ind,12) = sum(sum(Cmat(2:3,2:3)))/sum(sum(Cmat(2:3,:)));
%     Cmat
    clear lab_svm AUCsvm
    for lab = 1:3
        [lab_svm(:,lab),score_svm] = predict(model.SVMModel{lab}, ...
            scoresRefitPost(:,indsStress)');
        [~,~,~,AUCsvm(lab),~] = perfcurve(dataPostLab(indsStress)==lab, ...
            score_svm(:,2),'true');
    end
    X(ind,13) = sum(sum(lab_svm,2)==1)/size(lab_svm,1);
    X(ind,14) = sum(sum(lab_svm,2)==-3)/size(lab_svm,1);
end

mdl = LinearModel.fit(X(:,14),y)
figure
plot(mdl)

%% Show video of time-(cross)frequency analysis
% categories = {'Homecage','FIT','FIT+CD1'};
% F(nTrain) = struct('cdata',[],'colormap',[]);
% figure, clf;
% for w = 1:nTrain
%     clf
%     model.kernel.plotpsd(w,0,20,regionNames);
%     ax = subplot(7,7,(C-3)*C+2);
%     text(0.5,0.5,['Time ' num2str(5*w) ' s: ' categories{labTrain(w)}], ...
%         'FontSize',16)
%     set( ax, 'visible', 'off')
%     drawnow
%     F(w) = getframe(gcf);
% end
% 
% vidObj = VideoWriter('testvid.avi');
% vidObj.FrameRate = 5;
% open(vidObj);
% for j = 1:nTrain
%     writeVideo(vidObj,F(j));
% end
% close(vidObj)
