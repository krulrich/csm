clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Set save options
saveOpts.saveOutput = true;
saveOpts.saveFigures = true;
saveOpts.fileName = 'stressOutput_20160225';

%% Set data options
dataOpts.numSeconds = 5;
dataOpts.highFreq = 50;
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5;
dataOpts.chans = 1:7; % index of channels to consider
dataOpts.dataSet = 'stressDataFIT';
dataOpts.path = '/home2/kru2/data/neuroscience/lfp/StressData';
%dataOpts.path = '/home/kru2/Documents/data/neuroscience/lfp/StressData';
D = dir([dataOpts.path '/LFPData']);
dataOpts.sessionNames = {D.name};
dataOpts.sessionNames = cellfun(@(y)[y{1} '_' y{2}], ...
    cellfun(@(x)strsplit(x,'_'),dataOpts.sessionNames(3:2:end),'un',0), 'un',0);
%dataOpts.sessionNames = dataOpts.sessionNames(1:5);
dataOpts.sessions = numel(dataOpts.sessionNames); % number of sessions/animals/trials

%% Set load options
loadOpts.sampleData = true; % sample a subset of data windows
loadOpts.sampleFact = .2; % fraction of all data windows to sample
loadOpts.testSet = true; % split data into testing and training set
loadOpts.trainRatio = .7; % fraction of data used in training set
loadOpts.valRatio = .05; % fraction of data used in validation set
loadOpts.testRatio = .25; % fraction of data used in testing set
loadOpts.useMatFile = true; % use matfile to store large data
loadOpts.dataStoreName = 'dataStoreAnimalCrossVal.mat';

modelOpts.C = numel(dataOpts.chans); % number of channels
modelOpts.Q = 20; % number of Gaussian components in SM kernel
modelOpts.R = 2; % rank of each coregionalization matrix
modelOpts.maxW = 500; % maximum number of windows in a partition
modelOpts.lambda = 100;%20; % lagrange multiplier (SVM tuning parameter)

algOpts.kernelFunction = 'linear';
algOpts.iters = 151;
algOpts.evalInterval = 10;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};

%% Load and preprocess Data
data = loadDataWindows(dataOpts,loadOpts);
fprintf('Loading complete ... \n')

%% Train model on training set
Svals = datasample(1:dataOpts.sessions,20,'Replace',false);
Lvals = 15:5:35;

XVals = linspace(0,1,1e3);
YVals = zeros(numel(XVals),numel(Svals),numel(Lvals),3);
AUCVals = zeros(numel(Svals),numel(Lvals),3);

for l = 1:numel(Lvals)
    for s = 1:numel(Svals)
        fprintf('Running model for L = %d (%d/%d),   Sess = %d (%d/%d) ... \n', ...
            Lvals(l),l,numel(Lvals),Svals(s),s,numel(Svals))
        saveOpts.fileName = sprintf('stressOutput_animalHoldout_L%d_Sess%d', ...
            Lvals(l), Svals(s));
        
        % set parameters (consecutive integers are needed to index matfile)
        modelOpts.W = 0;
        data.y = [];
        algOpts.labels = [];
        for sN = [1:(Svals(s)-1) (Svals(s)+1):dataOpts.sessions]
            modelOpts.W = modelOpts.W + data.nTrain(sN,:); % total number of windows
            inds = find(data.sessionLabTrain==sN); % find indices for session sN
            data.y = cat(3, data.y, data.XfftTrain(:,:,inds)); % append training data for session sN
            algOpts.labels = cat(1, algOpts.labels, data.labTrain(inds,:)); % append label data for session sN
        end
            
        % create a new model
        modelOpts.L = Lvals(l); % number of factors
        model = GP.LMC_FA_SVM(modelOpts,dataOpts); % create model
        
        % update model via resilient backpropagation
        figure(2);
        plotOpts.ax = gca;
        evals = algorithms.rprop(data.s,data,model,algOpts,plotOpts);
        
        if saveOpts.saveOutput
            save(saveOpts.fileName);
            fprintf('Workspace saved ...\n');
        end
        
        %% Test model on heldout testing set
        % create a new model for testing (since model form is data dependent)
        modelRefitOpts = modelOpts;
        modelRefitOpts.W = data.nTrain(Svals(s),1);%sum(data.nTest);
        modelRefit = GP.LMC_FA(modelRefitOpts,dataOpts,model.kernel.LMCkernels);
        modelRefit.updateKernels = false;
        
        % set current data and test labels to heldout animal
        inds = find(data.sessionLabTrain==Svals(s));
        data.y = data.XfftTrain(:,:,inds);
        data.labTest = data.labTrain(inds,:);
        labTest = data.labTest;
        
        % update model via resilient backpropagation (requires data.y as input)
        evals2 = algorithms.rprop(data.s,data,modelRefit,algOpts,plotOpts);
        
        if saveOpts.saveOutput
            save(saveOpts.fileName);
            fprintf('Workspace saved ...\n');
        end
        
        scoresRefit = exp(modelRefit.logScores);
        
        labVals = 1:3;
        for lab = labVals
            [~,score_svm] = predict(model.SVMModel{lab},scoresRefit');
            [Xsvm,Ysvm,Tsvm,AUCsvm,optPTsvm] = ...
                perfcurve(labTest==lab,score_svm(:,2),'true');
            [~,inds] = unique(Xsvm);
            YVals(:,l,s,lab) = interp1(Xsvm(inds),Ysvm(inds),XVals);
            AUCVals(l,s,lab) = AUCsvm;
        end
        
    end
end

%% Show results
colors = get(gca,'colororder');
alpha = .2;
labVals = 1:3;

for l = 1:numel(Lvals)
    figure,
    for lab = labVals
        plot(XVals,sum(YVals(:,:,l,lab),2)/numel(Svals),'Color',colors(lab,:),'LineWidth',2)
        hold on
    end
    % for lab = labVals
    %     for file = 1:numel(files)
    %         plot(XVals,YVals(:,file,lab),'Color',min(colors(lab,:)+alpha,1))
    %     end
    % end
    legend({'Homecage' 'FIT' 'FIT+CD1'},'Location','Best')
    hold off
    xlabel('False positive rate','FontSize',12)
    ylabel('True positive rate','FontSize',12)
    title('Average ROC for Heldout Mouse','FontSize',14)
    drawnow
    pause(.5)
    
    figure,
    boxplot(squeeze(AUCVals(l,:,:)),{'Homecage' 'FIT' 'FIT+CD1'})
    title('AUC Box Plot for Heldout Mouse','FontSize',14)
    
    fprintf('Average AUC: %d\n',sum(AUCVals(l,:,:))/numel(AUCVals(l,:,:)))
end