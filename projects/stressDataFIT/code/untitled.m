clear; close all;

N=25;
C=3;
L=5;
sig = 1e-1;

for l=1:L
    b1 = rand(C,1);
    B1 = b1*b1';
    k1 = rand(N);
    K1 = k1*k1';
    
    b2 = rand(C,1);
    B2 = b2*b2';
    k2 = rand(N);
    K2 = k2*k2';

    Kl{l} = kron(B1,diag(k1(:,1))) + kron(B2,diag(k2(:,1)));
    
    [Ql{l},Lambdal{l}] = eig(Kl{l});
end
K = blkdiag(Kl{:});
Q = blkdiag(Ql{:});
Lambda = blkdiag(Lambdal{:});
figure, imagesc(K);
figure, imagesc(Q);

s = rand(L,1);
sNorm = norm(s);
%s = s/norm(s)^2;
sI = kron(s'/sNorm,eye(N*C));
sIQ = sI*Q;

A1 = sNorm^2*sI*K*sI' + sig*eye(N*C);
A2 = sIQ*(sNorm^2*Lambda + sig*eye(N*C*L))*sIQ';
sum(abs(A1(:)-A2(:)))

figure, imagesc(A1), colorbar
figure, imagesc(A2), colorbar

invTemp = Q/(sNorm^2*Lambda + sig*eye(N*C*L))*Q';
%figure, imagesc(inv(Q*(sNorm^2*Lambda + sig*eye(N*C*L))*Q')), colorbar
figure, imagesc(invTemp), colorbar % check

sol3 = pinv(sI)'*invTemp*pinv(sI);
figure, imagesc(sol3), colorbar

sol1 = inv(A1);
figure, imagesc(sol1), colorbar

sol2 = sIQ/(sNorm^2*Lambda + sig*eye(N*C*L))*sIQ';
figure, imagesc(sol2), colorbar

sum(abs(sol1(:)-sol2(:)))

[Qs1,Rs1] = qr(sI');
[Qs2,Rs2] = qr(sI',0);

sol4 = Rs2\Qs2'*Q/(sNorm^2*Lambda + sig*eye(N*C*L))*Q'*Qs1*Rs1;

figure, imagesc(Q*(sNorm^2*Lambda + sig*eye(N*C*L))*Q'*sI'*sol2), colorbar

temp=rand(75,1);
sol4 = sI*Q/(sNorm^2*Lambda + sig*eye(N*C*L))*Q'*Qs1*Rs1*temp;
figure, imagesc(sol4), colorbar

sol5 = Qs1*Rs1*[1;zeros(74,1)];

% sol2 = pinv(sIQ)'*((Lambda + sig/norm(s)^2*eye(N*C*L))\pinv(sIQ')');
% figure, imagesc(sol2), colorbar


