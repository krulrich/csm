clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Load saved model
fileName = 'stressOutputAll_20160330_groups_r11_testAll';
load(strcat('../savedWorkspace/',fileName));
fileName = 'stressOutputAll_20160330_groups_r11_testAll';
dataAll = matfile(strcat('../dataMatfile/dataStoreAll_',fileName));
dataAll2 = matfile(strcat('../dataMatfile/dataStoreAll2_',fileName));

%% Align all scores
nSessions = dataOptsAll.sessions+dataOptsAll2.sessions;
scoresAllAlign = zeros(nSessions,240,modelOptsAll.L);
taskLabels = cat(2,ones(1,120),2*ones(1,60),3*ones(1,60));
tasks = {'Homecage','FIT','FIT+CD1'};
sessionNames = cat(1,dataOptsAll.sessionNames',dataOptsAll2.sessionNames');
sessionLabsWindow = cat(1,dataAll.sessionLab,dataOptsAll.sessions+dataAll2.sessionLab);
taskLabsWindow = cat(1,dataAll.lab,dataAll2.lab);

scoresAll = exp(modelAll.logScores);
for sess = 1:nSessions
    inds = find(sessionLabsWindow==sess & taskLabsWindow==1);
    scoresAllAlign(sess,1:120,:) = scoresAll(:,inds(1:120))';
    
    inds = find(sessionLabsWindow==sess & taskLabsWindow==2);
    scoresAllAlign(sess,121:180,:) = scoresAll(:,inds(1:60))';
    
    inds = find(sessionLabsWindow==sess & taskLabsWindow==3);
    scoresAllAlign(sess,181:240,:) = scoresAll(:,inds(1:60))';
end

scoresAllAlign = bsxfun(@rdivide,scoresAllAlign,sum(scoresAllAlign,3));

%save('scoresAlign.mat','scoresAllAlign','taskLabels','tasks','sessionNames')

%% Match pre and post stress data

scoresAllAlignMatch = zeros(dataOptsAll2.sessions,240,modelOptsAll.L,2);
scoresAllAlignMatch(:,:,:,2) = scoresAllAlign((dataOptsAll.sessions+1):end,:,:);

for sess = 1:dataOptsAll2.sessions
    mouseName = strsplit(dataOptsAll2.sessionNames{sess},'_');
    mouseName = mouseName{1};
    miceOrigNames = cellfun(@(y)y{1}, ...
        cellfun(@(x)strsplit(x,'_'),dataOptsAll.sessionNames,'un',0),'un',0);
    
    indsC = strfind(miceOrigNames,mouseName);
    inds = find(not(cellfun('isempty', indsC)));
    
    scoresAllAlignMatch(sess,:,:,1) = scoresAllAlign(inds(1),:,:);
end

%% Show average pre and post stress scores over time
l = 4;
ws = 4;
scoresAllAlignMatchMA = filter(1/ws*ones(ws,1),1,scoresAllAlignMatch,[],2);

intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;
intRatio(11) = -1;

indsControl = intRatio == 0;
indsResil = intRatio > 1;
indsNoResil = (intRatio < 1 & intRatio > 0);

scoresControl = cat(2,scoresAllAlignMatchMA(indsControl,:,l,1),scoresAllAlignMatchMA(indsControl,:,l,2));
scoresResil = cat(2,scoresAllAlignMatchMA(indsResil,:,l,1),scoresAllAlignMatchMA(indsResil,:,l,2));
scoresNoResil = cat(2,scoresAllAlignMatchMA(indsNoResil,:,l,1),scoresAllAlignMatchMA(indsNoResil,:,l,2));

xvals = [linspace(0,1,120) linspace(1.01,2,60) linspace(2.01,3,60)];
xvals = [xvals 3+xvals];

figure
hold on
plot(xvals,mean(scoresControl,1),'-','Color',[.1 .1 1],'LineWidth',2)
plot(xvals,mean(scoresResil,1),'-','Color',[.1 1 .1],'LineWidth',2)
plot(xvals,mean(scoresNoResil,1),'-','Color',[1 .1 .1],'LineWidth',2)
plot(xvals,scoresNoResil,'-','Color',[1 .7 .7])
plot(xvals,scoresResil,'-','Color',[.7 1 .7])

plot(xvals,scoresControl,'-','Color',[.7 .7 1])

plot(xvals,mean(scoresResil,1),'-','Color',[.1 1 .1],'LineWidth',2)
plot(xvals,mean(scoresControl,1),'-','Color',[.1 .1 1],'LineWidth',2)

plot(xvals,mean(scoresNoResil,1),'-','Color',[1 .1 .1],'LineWidth',2)

plot([1,1],get(gca,'ylim'),'k-','LineWidth',2)
plot([2,2],get(gca,'ylim'),'k-','LineWidth',2)
plot([3,3],get(gca,'ylim'),'k-','LineWidth',4)
plot([4,4],get(gca,'ylim'),'k-','LineWidth',2)
plot([5,5],get(gca,'ylim'),'k-','LineWidth',2)
hold off
set(gca,'ylim',[0,.25]);%1.5*max([mean(scoresControl,1) mean(scoresResil,1) mean(scoresNoResil,1)])])
legend({'Control Group','Resilient Group','Non-resilient Group'},'location','best')
set(gca,'xtick',.5:1:5.5,'xticklabel', ...
    {'HC-pre','FIT-pre','CD1-pre','HC-post','FIT-post','CD1-post'})
ylabel('Factor score','FontSize',12)
title(sprintf('Factor %d Scores by Group',l),'FontSize',14)

%% Create boxplot of pre-stress scores
l = 4;

intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;
intRatio(11) = -1;

indsControl = intRatio == 0;
indsResil = intRatio > 1;
indsNoResil = (intRatio < 1 & intRatio > 0);

scoresControlPreFIT = mean(scoresAllAlignMatch(indsControl,taskLabels==2,l,2),2);
scoresResilPreFIT = mean(scoresAllAlignMatch(indsResil,taskLabels==2,l,2),2);
scoresNoResilPreFIT = mean(scoresAllAlignMatch(indsNoResil,taskLabels==2,l,2),2);

figure,
boxplot([scoresControlPreFIT;scoresResilPreFIT;scoresNoResilPreFIT], ...
    [ones(size(scoresControlPreFIT));2*ones(size(scoresResilPreFIT));3*ones(size(scoresNoResilPreFIT))])
ylabel('Average Factor Score')
set(gca,'xtick',[1,2,3],'xticklabel', ...
    {'Control Group','Resilient Group','Non-resilient Group'})
title('Factor 4 Usage for Post-Stress Mice under FIT','FontSize',14)


%% Create boxplot of task conditions
l = 3;

intRatio = zeros(dataOptsPost.sessions,1);
intRatio(5) = 0.3552136842;intRatio(9) = 1.0373584754;intRatio(11) = 0.9417476293;intRatio(12) = 0.4371482438;intRatio(15) = 0.8247298954;intRatio(18) = 1.3122021776;intRatio(19) = 1.4690909373;intRatio(20) = 0.8490863281;intRatio(21) = 0.3382716125;intRatio(23) = 1.2494352287;intRatio(24) = 0.7626183007;intRatio(26) = 1.0206631271;intRatio(27) = 0.48757764;intRatio(28) = 1.4168032554;intRatio(31) = 1.6445454191;intRatio(30) = 0.7116962948;intRatio(33) = 0.8461808112;intRatio(34) = 1.1955609054;intRatio(35) = 1.2512777138;
intRatio(11) = -1;

scoresPostHC = mean(scoresAllAlignMatch(:,taskLabels==1,l,:),2);
scoresPostFIT = mean(scoresAllAlignMatch(:,taskLabels==2,l,:),2);
scoresPostCD1 = mean(scoresAllAlignMatch(:,taskLabels==3,l,:),2);
scoresPostHC = scoresPostHC(:);
scoresPostFIT = scoresPostFIT(:);
scoresPostCD1 = scoresPostCD1(:);

figure,
boxplot([scoresPostHC;scoresPostFIT;scoresPostCD1], ...
    [ones(size(scoresPostHC));2*ones(size(scoresPostFIT));3*ones(size(scoresPostCD1))])
ylabel('Average Factor Score')
set(gca,'xtick',[1,2,3],'xticklabel', ...
    {'Homecage','FIT','FIT+CD1'})
title('Factor 3 Usage for All Mice','FontSize',14)

