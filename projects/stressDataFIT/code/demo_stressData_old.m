clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))

%% Set Parameters
dataOpts.numSeconds = 5;
dataOpts.highFreq = 50; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5; %10
dataOpts.mouseName = 'MouseGS007_090914';

C = 7;

%% Load and preprocess Data
[X,regionNames] = loadStressData(dataOpts,'_PredictionIso_FIT_LFP');
N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

% take fourier transform
Ns = ceil(N/2);
Xfft = 1/sqrt(N)*fft(X);
Xfft = 2*(Xfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

%% Visualize Data
% figure(1), clf, plot(tau,X(:,:,1));%plot(tau(1:min(1*sampRate,N)),x(1:min(1*sampRate,N),:,1));
% title('Example LFP Data','FontSize',16)
% xlabel('time (s)','FontSize',14)
% legend(regionNames(1:C))

%% Create model
nW = 131;
Q = 10;
R = 2*ones(Q,1);
gamma = 100;
L = 15;

% create Q Gaussian basis functions
%k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);
LMCmodels = cell(L,1);
for l = 1:L
    %kernels = GP.kernels.gaborKernels(Q,1.5,40);
    %means = num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq,Q));
    means = num2cell(dataOpts.lowFreq + (dataOpts.highFreq-dataOpts.lowFreq)*rand(1,Q));
    k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,dataOpts.highFreq],[eps,1e2]), ...
        means,num2cell(rand(1,Q)), 'un', 0);
    kernels = GP.kernels.kernels(Q, k);
    
    % generate kernels (weights and phase of basis functions) for each reagion
    B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
    %B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
    coregs = GP.coregs.mats(C,Q,R,B);
    
    % form GP with CSM kernel
    LMCmodels{l} = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);
end

kernel = GP.LMC_FA(LMCmodels,nW,C,Q);

model = GP.LMC_FA_SVM(kernel,nW,C,Q);
opts.labels = -1*ones(nW,1);
opts.labels(1:end/2) = -1*opts.labels(1:end/2);

%% Fit model
opts.iters = 100;
opts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
figure(2); clf;

evals = algorithms.rprop(s,Xfft(:,1:C,1:nW),model,opts,plotOpts);


%% Results
%for l = 1:L
%figure, model.LMCkernels{l}.plotpsd(0,20)
%end

figure, imagesc(min(model.kernel.scores,5)), colorbar

%%
nWR = nW; %10
modelRefit = GP.LMC_FA(model.kernel.LMCkernels,nWR,C,Q);
modelRefit.updateKernels = false;
evals2 = algorithms.rprop(s,Xfft(:,1:C,1:nWR),modelRefit,opts,plotOpts);

figure, imagesc(min(modelRefit.scores,5)), colorbar

[modelRefit.scores'*model.SVMModel.Beta + model.SVMModel.Bias ...
model.kernel.scores(:,1:nWR)'*model.SVMModel.Beta + model.SVMModel.Bias]

%%
%figure, model.LMCkernels{2}.kernels.plotSpecDens(0,50)

%%
F(nW) = struct('cdata',[],'colormap',[]);
figure, clf;
for w = 1:nW
    clf
    model.kernel.plotpsd(w,0,30,regionNames);
    drawnow
    F(w) = getframe(gcf);
end

vidObj = VideoWriter('testvid.avi');
vidObj.FrameRate = 5;
open(vidObj);
for j = 1:nW
    writeVideo(vidObj,F(j));
end
close(vidObj)



%%
% params2=params(2:end,:);
% qq=std(params2')';
% params3=bsxfun(@rdivide,params2,qq);
% [u,v] = nnmf(params3,10);
% H=u(:,4).*qq;
% figure(6), imagesc(reshape(H,7,[]),[0 1]*max(abs(H)));colorbar
% %%
% model2 = model;
% model2.setParams([100;u(:,3).*qq]);
% figure, model2.plotpsd(0,30);
% 

%% 
% Qnew = Q;
% modelNew = model.LMCkernels{1};
% %modelNew.Q = Qnew;
% %modelNew.kernels.k = modelNew.kernels.k(1:Qnew);
% %modelNew.kernels.Q = Qnew;
% %modelNew.coregs.B = modelNew.coregs.B(1:Qnew);
% %modelNew.coregs.Q = Qnew;
% 
% nW = 30;%size(X,3);
% modelNew.updateKernels = 0;
% paramsNew2 = zeros(nParams-nkParams,nW);
% 
% tic
% for w = 1:nW
%     xt = X(:,1:C,w);
%     xfft = Xfft(:,1:C,w);
%     modelNew.gamma = 100;
% 
%     opts.iters = 300;
%     algorithms.rprop(s,xfft,modelNew,opts,plotOpts);
%     paramsNew2(:,w) = modelNew.getParams;
%     fprintf('Window %d/%d, time %0.2f\n',w,nW,toc)
% end
% 
% figure, modelNew.plotpsd(0,30)
% 
% figure, imagesc(paramsNew2(2:end,:)), colorbar
% %save output4
% 
% %%
% paramsAll = paramsNew2(2:end,:);%[paramsNew(2:end,:) paramsNew2(2:end,:)];
% qq=std(paramsAll,0,2);
% params3=bsxfun(@rdivide,paramsAll,qq);
% [u,v] = nnmf(params3,4,'rep',20);
% H=u(:,3).*qq;
% %figure(6), imagesc(reshape(H(2:end),7,[]),[0 1]*max(abs(H(2:end))));colorbar
% %%
% modelNew.setParams([1000; u(:,3).*qq]);
% figure, modelNew.plotpsd(0,30);
% 
% %% Figures to Show
% figure, model.LMCkernels{2}.kernels.plotSpecDens(0,50)
% title('Kernel Bases','FontSize',12)
% 
% modelNew.setParams(paramsNew2(:,20))
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Window 20','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams(paramsNew2(:,15))
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Window 81','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams(paramsNew2(:,20))
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Window 110','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% %paramsAll = [paramsNew(2:end,:) paramsNew2(2:end,:)];
% figure, imagesc(paramsAll), colorbar
% title('Parameters, MouseGS007 090914','FontSize',12)
% xlabel('Window number (3 sec)')
% ylabel('Parameter number')
% 
% %qq=ones(size(paramsAll,1),1);
% qq=std(paramsAll,0,2);
% params3=bsxfun(@rdivide,paramsAll,qq);
% [u,v] = nnmf(params3,10,'rep',20);
% figure, scatter3(v(1,:),v(2,:),v(3,:),10,1:size(v,2),'filled'), colorbar
% xlabel('Component 1')
% ylabel('Component 2')
% zlabel('Component 3')
% 
% figure, imagesc(v), colorbar
% xlabel('Window')
% ylabel('Component')
% title('NNMF Component Usage','FontSize',14)
% 
% modelNew.setParams([1000; u(:,1).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 1','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams([1000; u(:,2).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 2','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams([1000; u(:,3).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 3','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams([1000; u(:,4).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% modelNew.setParams([1000; u(:,6).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 6','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% axes;
% h = title('Component 4','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% modelNew.setParams([1000; u(:,6).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% modelNew.setParams([1000; u(:,6).*qq])
% figure, modelNew.plotpsd(0,30,regionNames)
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 6','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% set(gcf,'NextPlot','add');
% axes;
% h = title('Component 6','FontSize',14);
% set(gca,'Visible','off');
% set(h,'Visible','on'); 
% 
% paramsHat = diag(qq)*u*v;
% figure, imagesc(paramsHat), colorbar
% title('Reconstruction','FontSize',12)
% xlabel('Window number (3 sec)')
% ylabel('Parameter number')