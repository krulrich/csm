function [x,regionNames] = loadStressData(opts,LFPname)
    if ~exist('LFPname','var'), LFPname = '_PredictionIso_Homecage_LFP'; end
    load([opts.mouseName '_CHANS']);
    
    %% Extract indices of channels from the same regions
    % this is a roundabout method, but remains consistent with different
    % methods of labelling channels within the stress datasets (for now!)
    
    acn = CHANNAMES(CHANACTIVE==1); % active channel names
    
    acn2 = cellfun(@(x)['*' x],acn,'un',0); % add indicator to start of string
    acn2 = cellfun(@(x)strrep(x,'*L_',''),acn2,'un',0); % remove '*L_' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*R_',''),acn2,'un',0); % remove '*R_' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); % remove '*' from start of strings
    
    acn2 = cellfun(@(x)x(1:numel(x)-3),acn2,'un',0); % remove the last 3 characters
    
    acn2 = cellfun(@(x)[x '*'],acn2,'un',0); % add indicator to end of string
    acn2 = cellfun(@(x)strrep(x,'_L*',''),acn2,'un',0); % remove '_L*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'_R*',''),acn2,'un',0); % remove '_R*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); % remove '*' from start of strings
    
    [regionNames,~,acn_inds] = unique(acn2); % automatically sorts names alphabetically
    R = numel(regionNames); % number of channels
    
    %% Load data
    load([opts.mouseName LFPname])
    
    %% Organize data into matrix
    x = zeros(eval(['numel(' acn{1} ')']),R);
    for r = 1:R
        % average all channels from the same region
        acn_inds_r = find(acn_inds==r)';
        for ac_ind = acn_inds_r;
            chan = double(eval(acn{ac_ind}))';
            
            % bandpass filter
            sampRateOrig = 1000; highFreq = 400; lowFreq = 1;
            highpass = highFreq/sampRateOrig*2;
            lowpass = lowFreq/sampRateOrig*2;
            [butterb,buttera] = butter(2,[lowpass,highpass]);
            chan = filtfilt(butterb,buttera,chan); % bandpass filter
            
            x(:,r) = x(:,r) + chan;
        end
        x(:,r) = x(:,r) / numel(find(acn_inds==r));
    end
    
    %% Filter and subsample this data
    numSeconds = opts.numSeconds;
    highFreq = opts.highFreq;
    lowFreq = opts.lowFreq;
    sampFact = opts.sampFact;

    % only process a fixed amount of time
    sampRate = sampRateOrig;%/sampFact;
    N = sampRate*numSeconds;

    % notch filter 60Hz signal
    filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
    filtbw=filtfreq/60; % 1Hz Bandwidth
    [filtb,filta]=iirnotch(filtfreq,filtbw);
    x = filtfilt(filtb,filta,x);

%     % bandpass filter
%     highpass = highFreq/sampRateOrig*2;
%     lowpass = lowFreq/sampRateOrig*2;
%     [butterb,buttera] = butter(2,[lowpass,highpass]);
%     x = filtfilt(butterb,buttera,x); % bandpass filter

%     % subsample by sample factor
%     x = x(1:sampFact:end,:);

    % separate into windows
    W = floor(size(x,1)/(numSeconds*sampRate));
    x = x(1:W*numSeconds*sampRate,:);
    x = permute(reshape(x',[R,N,W]),[2,1,3]);
    
    % bandpass filter
    highpass = highFreq/sampRateOrig*2;
    lowpass = lowFreq/sampRateOrig*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    x = filtfilt(butterb,buttera,x); % bandpass filter
    
    % normalize data
    x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x)),std(x));
        
    % subsample by sample factor
    x = x(1:sampFact:end,:,:);
end