function [XTs,XOf,XHc,regionNames] = loadTstData(opts)
% loadTstData
%   Loads LFP recordings from tail suspension experiments. Data are separated
%   into homecage, open field, and tail supension sections, then preprocessed.
%   INPUTS
%   opts: Structure containing options for loading data.
%       FIELDS
%       mouseName: string containing mouse name as written in data
%           filename (e.g. 'MouseCKA1')
%       data: string containing date as found in data filename
%           (e.g. '030115')
%       highfreq: high cutoff frequency of second bandpass filter for data
%       lowfreq: low cutoff frequency of second bandpass filter for data
%       sampFact: subsampling factor
%       numSeconds: number of seconds per window
%   OUTPUTS
%   XTs: NxRxWt matrix of processed tail suspension data. N = # points/window.
%           R = # regions recorded. Wt = # windows in tail suspension segment.
%   XOf: NxRxWo matrix of processed open field data. Wo = # windows in open
%       field segment.
%   XHc: NxRxWh matrix of processed homecage data. Wh = # windows in homecage.
%   regionNames: cell array containing names of the R regions recorded.

% Initial bandpass filter parameters (Hz)
sampRateOrig = 1000; highFreq1 = 400; lowFreq1 = 1;

% homecage recording duration (s)
hcDuration = 300;

%% Load data
load([opts.mouseName '_' opts.date '_HCOFTS_TIME'],'INT_TIME')
lfpData = load([opts.mouseName '_' opts.date '_HCOFTS_LFP']);

%% Extract indices of channels from the same regions
% this is a roundabout method, but remains consistent with different
% methods of labelling channels within the stress datasets (for now!)

% get channel names (don't include lateral habenula)
vars = fieldnames(lfpData);
isChannel = ~cellfun(@isempty,regexp(vars,'_\d\d'));
isChannel = isChannel & cellfun(@isempty, regexp(vars,'LH_Hab_\d\d'));
acn = vars(isChannel);

acn2 = cellfun(@(x)['*' x],acn,'un',0); % add indicator to start of string
acn2 = cellfun(@(x)strrep(x,'*L_',''),acn2,'un',0); % remove '*L_' from start of strings
acn2 = cellfun(@(x)strrep(x,'*R_',''),acn2,'un',0); % remove '*R_' from start of strings
acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); % remove '*' from start of strings

acn2 = cellfun(@(x)x(1:numel(x)-3),acn2,'un',0); % remove the last 3 characters

acn2 = cellfun(@(x)[x '*'],acn2,'un',0); % add indicator to end of string
acn2 = cellfun(@(x)strrep(x,'_L*',''),acn2,'un',0); % remove '_L*' from start of strings
acn2 = cellfun(@(x)strrep(x,'_R*',''),acn2,'un',0); % remove '_R*' from start of strings
acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); % remove '*' from start of strings

[regionNames,~,acn_inds] = unique(acn2); % automatically sorts names alphabetically
R = numel(regionNames); % number of channels

%% Organize data into matrix
x = zeros(eval(['numel(lfpData.' acn{1} ')']),R);
for r = 1:R
    % average all channels from the same region
    acn_inds_r = find(acn_inds==r)';
    for ac_ind = acn_inds_r;
        chan = double(eval(['lfpData.' acn{ac_ind}]))';
        
        % bandpass filter
        highpass1 = highFreq1/sampRateOrig*2;
        lowpass1 = lowFreq1/sampRateOrig*2;
        [butterb1,buttera1] = butter(2,[lowpass1,highpass1]);
        chan = filtfilt(butterb1,buttera1,chan); % bandpass filter
        
        x(:,r) = x(:,r) + chan;
    end
    x(:,r) = x(:,r) / numel(find(acn_inds==r));
end

%% Get relevant portions of recordings

intervals = INT_TIME*sampRateOrig;

% homecage
hcStart = 1;
hcEnd = hcDuration*sampRateOrig;
XHc = x(hcStart:hcEnd,:);

% open field
ofStart = intervals(1);
ofEnd = intervals(1)+intervals(2)-1;
XOf = x(ofStart:ofEnd,:);

% tail suspension
tsStart = intervals(3);
tsEnd = intervals(3)+intervals(4)-1;
XTs = x(tsStart:tsEnd,:);

%% Finish preprocessing individual data sections

XTs = processSection(XTs);
XOf = processSection(XOf);
XHc = processSection(XHc);

%{
processSection
    Performs preprocessing for a section of LFP data. Pre-processing includes
    60Hz notch filtering, separating data into windows, bandpass filtering,
    normalization, and subsampling.
%}
    function data = processSection(data)
        highFreq2 = opts.highFreq;
        lowFreq2 = opts.lowFreq;
        sampFact = opts.sampFact;
        N = opts.numSeconds*sampRateOrig;
        
        % notch filter 60Hz signal
        filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
        filtbw=filtfreq/60; % 1Hz Bandwidth
        [filtb,filta]=iirnotch(filtfreq,filtbw);
        data = filtfilt(filtb,filta,data);
        
        % separate into windows
        W = floor(size(data,1)/(N));
        data = data(1:W*N,:);
        data = permute(reshape(data',[R,N,W]),[2,1,3]);
        
        % bandpass filter
        highpass2 = highFreq2/sampRateOrig*2;
        lowpass2 = lowFreq2/sampRateOrig*2;
        [butterb2,buttera2] = butter(2,[lowpass2,highpass2]);
        data = filtfilt(butterb2,buttera2,data); % bandpass filter
        
        % normalize data
        data = bsxfun(@rdivide,bsxfun(@minus,data,mean(data)),std(data));
        
        % subsample by sample factor
        data = data(1:sampFact:end,:,:);
    end
end