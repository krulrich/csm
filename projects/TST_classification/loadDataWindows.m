function data = loadDataWindows(dataOpts,loadOpts)
% loadDataWindows
%   loads TST data from multiple files.
%   INPUTS
%   dataOpts: structure containing information about the stored data and
%       preprocessing options
%       FIELDS
%       path: string containing path to the folder containing all data files
%       chans: vector indicating which channels to use
%       mouseName: string containing mouse name as written in data
%           filename (e.g. 'MouseCKA1')
%       data: string containing date as found in data filename
%           (e.g. '030115')
%       highfreq: high cutoff frequency of second bandpass filter for data
%       lowfreq: low cutoff frequency of second bandpass filter for data
%       sampFact: subsampling factor
%       numSeconds: number of seconds per window
%   loadOpts: structure containing options for loading the data into a single
%       structure or file
%       useMatFile: logical indicating whether the output of the function will
%           be a matfile or structure
%       dataStoreName: name of matfile for data storage. Used only if useMatFile
%           is true
%       sampleData: logical indicating whether to sample a subset of the data
%       testSet: logical indicating whether to hold out sets of data for
%           cross-validation and testing
%       trainRatio,valRatio,testRatio: indicates the proportion of data to be
%           included in the training, validation, and test sets, respectively.
%   OUTPUTS
%   data: structure containing data
%   LOADED VARIABLES
%   nSessions
%   sessionNames
%   sessionDates
%   gtypeLabels
if loadOpts.useMatFile
    if exist(loadOpts.dataStoreName,'file')
        delete(loadOpts.dataStoreName); % remove previous data storage on hd
    end
    data = matfile(loadOpts.dataStoreName,'Writable',true);
end

addpath(genpath(dataOpts.path))

load('sessions.mat')

idxE = 0;
idxValE = 0;
idxTestE = 0;

for sess = 1:nSessions
    % get mouse name & date (session name)
    dataOpts.mouseName = sessionNames{sess};
    dataOpts.date = sessionDates{sess};
    fprintf('Loading data for %s from %s (%d/%d) ...\n', ...
        dataOpts.mouseName,dataOpts.date,sess,nSessions)
    
    % Load all data for session sess. Skip this session if not all regions were
    %   recorded
    [XTs,XOf,XHc,regionNames] = loadTstData(dataOpts);
    if numel(regionNames) ~= numel(dataOpts.chans)
        warning('Skipping session: incorrect number of regions in data.')
        continue
    end
    data.regionNames = regionNames(dataOpts.chans);
    
    % concatenate data and labels for session sess
    X = cat(3,XHc,XOf,XTs);
    labels = [ones(size(XHc,3),1); 2*ones(size(XOf,3),1);  ...
        3*ones(size(XTs,3),1)];
    
    % randomly sample a subset of data from session sess
    if loadOpts.sampleData
        W = size(X,3);
        weights = W*((labels==1)/sum(labels==1) + ...
            (labels==2)/sum(labels==2) + (labels==3)/sum(labels==3));
        inds = sort(datasample(1:W,ceil(loadOpts.sampleFact*W), ...
            'Replace',false,'Weights',weights));
        
        %inds = dividerand(size(X,3),loadOpts.sampleFact,0,1-loadOpts.sampleFact);
        X = X(:,:,inds);
        labels = labels(inds);
    end
    
    % take Fourier transform
    data.N = size(X,1);
    data.Ns = ceil(data.N/2);
    Xfft = 1/sqrt(data.N)*fft(X);
    Xfft = 2*(Xfft(2:data.Ns+1,:,:));
    
    % split data into training and testing sets
    if loadOpts.testSet
        [trainInd,valInd,testInd] = dividerand(size(X,3), ...
            loadOpts.trainRatio,loadOpts.valRatio,loadOpts.testRatio);
        data.nTrain(sess,1) = numel(trainInd);
        data.nVal(sess,1) = numel(valInd);
        data.nTest(sess,1) = numel(testInd);
        idxB = idxE + 1;
        idxValB = idxValE + 1;
        idxTestB = idxTestE + 1;
        idxE = idxB + data.nTrain(sess,1) - 1;
        idxValE = idxValB + data.nVal(sess,1) - 1;
        idxTestE = idxTestB + data.nTest(sess,1) - 1;
        if sess == 1
            data.XTrain = X(:,dataOpts.chans,trainInd);
            data.XVal = X(:,dataOpts.chans,valInd);
            data.XTest = X(:,dataOpts.chans,testInd);
            data.XfftTrain = Xfft(:,dataOpts.chans,trainInd);
            data.XfftVal = Xfft(:,dataOpts.chans,valInd);
            data.XfftTest = Xfft(:,dataOpts.chans,testInd);
        else
            data.XTrain(:,:,idxB:idxE) = X(:,dataOpts.chans,trainInd);
            data.XVal(:,:,idxValB:idxValE) = X(:,dataOpts.chans,valInd);
            data.XTest(:,:,idxTestB:idxTestE) = X(:,dataOpts.chans,testInd);
            data.XfftTrain(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,trainInd);
            data.XfftVal(:,:,idxValB:idxValE) = Xfft(:,dataOpts.chans,valInd);
            data.XfftTest(:,:,idxTestB:idxTestE) = Xfft(:,dataOpts.chans,testInd);
        end
        data.labTrain(idxB:idxE,1) = labels(trainInd);
        data.labVal(idxValB:idxValE,1) = labels(valInd);
        data.labTest(idxTestB:idxTestE,1) = labels(testInd);
        data.sessionLabTrain(idxB:idxE,1) = sess;
        data.sessionLabVal(idxValB:idxValE,1) = sess;
        data.sessionLabTest(idxTestB:idxTestE,1) = sess;
        data.gtypeLabTrain(idxB:idxE,1) = gtypeLabels(sess);
        data.gtypeLabVal(idxValB:idxValE,1) = gtypeLabels(sess);
        data.gtypeLabTest(idxTestB:idxTestE,1) = gtypeLabels(sess);
    else
        data.Wm(sess,1) = size(X,3);
        idxB = idxE + 1;
        idxE = idxB + data.Wm(sess,1) - 1;
        if sess == 1
            data.X = X(:,dataOpts.chans,:);
            data.Xfft = Xfft(:,dataOpts.chans,:);
        else
            data.X(:,:,idxB:idxE) = X(:,dataOpts.chans,:);
            data.Xfft(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,:);
        end
        data.lab(idxB:idxE,1) = labels;
        data.sessionLab(idxB:idxE,1) = sess;
        data.gtypeLab(idxB:idxE,1) = gtypeLabels(sess);
    end
    
    clear X labels Xfft xHc
end

% store DFT frequency locations
data.sampRate = data.N/dataOpts.numSeconds;
data.tau = 0:1/data.sampRate:(dataOpts.numSeconds-1/data.sampRate);
data.s = (data.sampRate/data.N):(data.sampRate/data.N):(data.sampRate/2);

end