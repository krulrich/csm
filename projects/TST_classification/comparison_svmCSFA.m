clear; format short e; close all;
set(0,'defaulttextinterpreter','none')

%% Set save options
saveOpts.saveOutput = true;
saveOpts.fileName = 'csfaRbf_xVal';
saveOpts.plot = true;

%% Set data options
dataOpts.numSeconds = 5;
dataOpts.highFreq = 50;
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5;
dataOpts.chans = 1:11; % index of channels to consider
dataOpts.path = '~/Box Sync/Home Folder nmg14/Box/NIPS2016/OpenField/';

%% Set load options
loadOpts.sampleData = true; % sample a subset of data windows
loadOpts.sampleFact = .5; % fraction of all data windows to sample
loadOpts.testSet = true; % split data into testing and training set
loadOpts.trainRatio = .7; % fraction of data used in training set
loadOpts.valRatio = .1; % fraction of data used in validation set
loadOpts.testRatio = .2; % fraction of data used in testing set
loadOpts.useMatFile = true; % use matfile to store large data
loadOpts.dataStoreName = 'dataStore.mat';
loadOpts.useStoredData = true; % use previously generated matfile

%% Set training options
modelOpts.Q = 20; % number of Gaussian components in SM kernel
modelOpts.R = 2; % rank of each coregionalization matrix
modelOpts.L = 25; % number of factors
modelOpts.maxW = 500; % maximum number of windows in a partition
modelOpts.kernel = 'rbf';
modelOpts.kernelScale = 'auto';
modelOpts.xVal = true;

algOpts.iters = 250;
algOpts.evalInterval = 10;

plotOpts.plot = false;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};

%% Load/preprocess Data
if loadOpts.useStoredData && exist(loadOpts.dataStoreName,'file')
    data = matfile(loadOpts.dataStoreName,'Writable',true);
    fprintf('Loading data from previously saved file.\n')
else
    data = loadDataWindows(dataOpts,loadOpts);
    fprintf('Loading complete ... \n')
end

%% Train CSFA model on training set

% create a model
modelOpts.C = numel(dataOpts.chans); % number of channels
modelOpts.W = sum(data.nTrain); % total number of windows
model = GP.LMC_FA(modelOpts,dataOpts);

% update model via resilient backpropagation
data.y = data.XfftTrain; % model requires data.y as input (use training data)
if plotOpts.plot
    figure
    plotOpts.ax = gca;
end
evals = algorithms.rprop(data.s,data,model,algOpts,plotOpts);
scores = exp(model.logScores)';

%% Train model for x-validation if necessary
if modelOpts.xVal
    % create a new model for validation (since model form is data dependent)
    % then train model to get scores
    modelValOpts = modelOpts;
    modelValOpts.W = sum(data.nVal);
    modelVal = GP.LMC_FA(modelValOpts,dataOpts,model.LMCkernels);
    modelVal.updateKernels = false;
    data.y = data.XfftVal;
    valPlotOpts = plotOpts;
    valPlotOpts.plot = false;
    algorithms.rprop(data.s,data,modelVal,algOpts,valPlotOpts);
    valScores = exp(modelVal.logScores)';
end

%% Train SVM on CSFA scores
% create a model for all classes in the classification problem
nClasses = numel(unique(data.labTrain));
for l = 1:(nClasses + 1)
    
    % get target label vector
    if l <= nClasses
        % initialize all class labels to -1 &set positive labels to 1
        % (1-vs-all classification)
        labels = -1*ones(size(data.labTrain));
        labels(data.labTrain == l) = 1;
    else % labels for genotype data
        labels = data.gtypeLabTrain;
    end
    
    % sample example data to train SVM
    [~,idx] = datasample(scores,sum(labels==1),'Weights',double(labels==-1));
    
    labelsSVM = [labels(labels==1); labels(idx)];
    scoresSVM = [scores(labels==1,:); scores(idx,:)];
    
    if modelOpts.xVal
        % get target label vector
        if l <= nClasses
            labVal = -1*ones(size(data.labVal));
            labVal(data.labVal == l) = 1;
        else
            labVal = data.gtypeLabVal;
        end
        
        % choose SVM model parameters via cross validation
        iCv = 1;
        for bc = logspace(5,0,11)
            valSvm{iCv} = fitcsvm(scoresSVM,labelsSVM,'Standardize',true,...
                'KernelFunction',modelOpts.kernel,'BoxConstraint',bc,...
                'KernelScale',modelOpts.kernelScale);
            valEst = predict(valSvm{iCv},valScores);
            vErr = (valEst~=labVal);
            L(iCv) = sum(vErr)/numel(vErr);
            iCv = iCv+1;
        end
        [ls,iCv] = min(L)
        svmModels{l} = valSvm{iCv};
    else
        % obtain the SVM weights on classifying factor scores
        svmModels{l} = fitcsvm(scoresSVM,labelsSVM,'Standardize',true,...
            'KernelFunction',modelOpts.kernel, 'KernelScale',modelOpts.kernelScale);
    end

    % measure performance on training data
    [trEst,s] = predict(svmModels{l},scores);
    trErr = (trEst~=labels);
    trainLoss(l) = sum(trErr)/numel(trErr)
    
    % save scores for multiclass classification
    if l <= nClasses      
        classScoresTrain(:,l) = s(:,2);
    end    
end

[~,estClassTrain] = max(classScoresTrain,[],2);
trErrMulti = (estClassTrain ~= data.labTrain);
trainLossMulti = sum(trErrMulti)/numel(trErrMulti)

targetMat = zeros(nClasses,numel(data.labTrain));
outputMat = targetMat;
for j = 1:nClasses
    targetMat(j,data.labTrain==j) = 1;
    outputMat(j,estClassTrain==j) = 1;
end

if saveOpts.plot
    figure; plotconfusion(targetMat, outputMat)
end

if saveOpts.saveOutput
    save(saveOpts.fileName);
    fprintf('Workspace saved ...\n');
end

%% Retrain model for testing

% create a new model for testing (since model form is data dependent)
modelRefitOpts = modelOpts;
modelRefitOpts.W = sum(data.nTest);
modelRefit = GP.LMC_FA(modelRefitOpts,dataOpts,model.LMCkernels);
modelRefit.updateKernels = false;

% update model via resilient backpropagation (requires data.y as input)
data.y = data.XfftTest;
evals2 = algorithms.rprop(data.s,data,modelRefit,algOpts,plotOpts);
testScores = exp(modelRefit.logScores)';

%% Test model on heldout testing set
for j = 1:(nClasses+1)
    % get target label vector
    if j <= nClasses
        labTest = -1*ones(size(data.labTest));
        labTest(data.labTest == j) = 1;
    else
        labTest = data.gtypeLabTest;
    end
    
    % measure performance on training data
    [tsEst,s] = predict(svmModels{j},testScores);
    tsErr = (tsEst~=labTest);
    testLoss(j) = sum(tsErr)/numel(tsErr)
    
    % save scores for multiclass classification
    if j <= nClasses      
        classScoresTest(:,j) = s(:,2);
    end
end

[~,estClassTest] = max(classScoresTest,[],2);
tsErrMulti = (estClassTest ~= data.labTest);
testLossMulti = sum(tsErrMulti)/numel(tsErrMulti)

targetMat = zeros(nClasses,numel(data.labTest));
outputMat = targetMat;
for j = 1:nClasses
    targetMat(j,data.labTest==j) = 1;
    outputMat(j,estClassTest==j) = 1;
end

if saveOpts.plot
    figure; plotconfusion(targetMat, outputMat)
end

if saveOpts.saveOutput
    save(saveOpts.fileName);
    fprintf('Workspace saved ...\n');
end


