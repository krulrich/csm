function [x,regionNames] = loadPrideData(opts)
   
    %% Load data
    load([opts.sessionName '.dat.mat'])
    x = raw';
    sampRateOrig = 1/(t(2) - t(1));
    regionNames = {'1','2','3'};
    R = 3;
    
    %% Filter and subsample this data
    numSeconds = opts.numSeconds;
    highFreq = opts.highFreq;
    lowFreq = opts.lowFreq;
    sampFact = opts.sampFact;

    % only process a fixed amount of time
    sampRate = sampRateOrig/sampFact;
    N = sampRate*numSeconds;

    % notch filter 60Hz signal
    filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
    filtbw=filtfreq/60; % 1Hz Bandwidth
    [filtb,filta]=iirnotch(filtfreq,filtbw);
    x = filtfilt(filtb,filta,x);

    % bandpass filter
    highpass = highFreq/sampRateOrig*2;
    lowpass = lowFreq/sampRateOrig*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    x = filtfilt(butterb,buttera,x); % bandpass filter

    % subsample by sample factor
    x = x(1:sampFact:end,:);

    % normalize data
    x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x)),std(x));

    % separate into windows
    W = floor(size(x,1)/(numSeconds*sampRate));
    x = x(1:W*numSeconds*sampRate,:);
    x = permute(reshape(x',[R,N,W]),[2,1,3]);
    
    % normalize again for fun
    x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x,1)),std(x,1));
end