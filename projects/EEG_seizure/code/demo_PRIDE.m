clear; format short e; close all;
set(0,'defaulttextinterpreter','none')
addpath('../../../code')

%% Load and preprocess Data
dataOpts.numSeconds = 3;
dataOpts.highFreq = 30;
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 1;
dataOpts.chans = 1:3; % index of channels to consider
dataOpts.dataSet = 'seizurePRIDE';
dataOpts.path = '/home2/kru2/data/neuroscience/eeg/seizure/PRIDE_test';
dataOpts.sessionNames = {'1417_Tx01','1417_Tx02','1417_Tx03','1417_Tx04', ...
    '1417_Tx05','1417_Tx06','1417_Tx07','1417_Tx08', ...
    '1439_Tx01','1439_Tx02','1439_Tx03','1439_Tx04','1439_Tx05', ...
    '1439_Tx07','1439_Tx08','1439_Tx09','1439_Tx10', ... % 1439_Tx06 missing
    '1440_Tx01','1440_Tx02','1440_Tx03','1440_Tx04','1440_Tx05', ...
    '1440_Tx06','1440_Tx07','1440_Tx08'};
dataOpts.sessions = numel(dataOpts.sessionNames); % number of sessions/animals/trials

loadOpts.testSet = false; % split data into testing and training set
loadOpts.testFrac = 0.3; % fraction of data in testing set
loadOpts.useMatFile = false; % use matfile to store large data

data = loadDataWindows(dataOpts,loadOpts);

%% Train model on training set
% create a model
modelOpts.C = numel(dataOpts.chans); % number of channels
modelOpts.Q = 20; % number of Gaussian components in SM kernel
modelOpts.R = 2; % rank of each coregionalization matrix
modelOpts.L = 20; % number of factors
modelOpts.W = sum(data.Wm); % total number of windows
modelOpts.maxW = 400; % maximum number of windows in a partition
model = GP.LMC_FA(modelOpts,dataOpts); % create model

% update model via resilient backpropagation
figure(2);
data.y = data.Xfft; % model requires data.y as input (use all data)
algOpts.iters = 20;
algOpts.evalInterval = 221;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
plotOpts.ax = gca;
evals = algorithms.rprop(data.s,data,model,algOpts,plotOpts);

%% Show some model details on training data
% plot factor scores on training set
scores = exp(model.logScores);
figure, imagesc(min(scores,2)), colorbar
xlabel('Window number (5 sec)')
ylabel('Factor number')
title('Factor Scores over Time','FontSize',14)

% plot inferred cross-spectrum for single window
figure, model.plotCsd(19,'minFreq',0,'maxFreq',30,'names',data.regionNames)

%% Display all kernel factors
lVals = 1:modelOpts.L;
for l = lVals
    %figure('name',num2str(l),'units','normalized','outerposition',[0 0 1 1])
    figure
    model.LMCkernels{l}.plotCsd('minFreq',0,'maxFreq',30, ...
        'names',data.regionNames,'showAmplitude',false)
    %ax = subplot(7,7,(C-3)*C+2);
    %text(0.5,0.5,['Factor ' num2str(l)],'FontSize',16)
    %set( ax, 'visible', 'off')
    drawnow
    pause(.1)
    saveas(gcf,['factor' sprintf('%02d',l)],'pdf')
    %eval(sprintf('!pdfcrop factor%02d.pdf factor%02d.pdf',l,l))
    close
end

%% Display covariance function
lvals = 1:modelOpts.L;
for l = lvals
    figure
    plot(data.tau,model.LMCkernels{l}.covFuns(data.tau),'LineWidth',2)
    xlabel('Time (seconds)')
    title(['Correlation Function for Factor ' num2str(l)],'FontSize',14)
    legend(data.regionNames,0)
    drawnow
    pause(.1)
    saveas(gcf,['corrfun_factor' sprintf('%02d',l)],'pdf')
    close
end

%% Plot factor scores for each session
scores = exp(model.logScores);
inds = [0; cumsum(data.Wm)];

for i = 2:numel(inds)
    figure;
    imagesc(dataOpts.numSeconds/60*(0:(data.Wm(i-1)-1)),1:modelOpts.L, ...
        min(scores(:,(inds(i-1)+1):inds(i)),1.2))
    colorbar
    xlabel('Time (minutes)')
    ylabel('Factor number')
    title(['Factor Scores over Time for ' dataOpts.sessionNames{i-1}],'FontSize',14)
    drawnow
    saveas(gcf,['scores_' dataOpts.sessionNames{i-1}],'pdf')
    close
end
