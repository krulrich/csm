function data = loadDataWindows(dataOpts,loadOpts)

if loadOpts.useMatFile
    delete('dataStore.mat'); % remove previous data storage on hd
    data = matfile('dataStore.mat','Writable',true);
end

addpath(genpath(dataOpts.path))

idxE = 0;
idxTestE = 0;

for sess = 1:dataOpts.sessions
    % load data for session
    dataOpts.sessionName = dataOpts.sessionNames{sess};
    [X,data.regionNames] = loadPrideData(dataOpts);
    labels = ones(size(X,3),1);
    
    % take Fourier transform
    data.N = size(X,1);
    data.Ns = ceil(data.N/2);
    Xfft = 1/sqrt(data.N)*fft(X);
    Xfft = 2*(Xfft(2:data.Ns+1,:,:));
    
    % split data into training and testing sets
    if loadOpts.testSet
        [trainInd,~,testInd] = dividerand(size(X,3),.7,0,.3);
        data.nTrain(sess,1) = numel(trainInd);
        data.nTest(sess,1) = numel(testInd);
        idxB = idxE + 1;
        idxTestB = idxTestE + 1;
        idxE = idxB + data.nTrain(sess,1) - 1;
        idxTestE = idxTestB + data.nTest(sess,1) - 1;
        if sess == 1
            data.XTrain = X(:,dataOpts.chans,trainInd);
            data.XTest = X(:,dataOpts.chans,testInd);
            data.XfftTrain = Xfft(:,dataOpts.chans,trainInd);
            data.XfftTest = Xfft(:,dataOpts.chans,testInd);
        else
            data.XTrain(:,:,idxB:idxE) = X(:,dataOpts.chans,trainInd);
            data.XTest(:,:,idxTestB:idxTestE) = X(:,dataOpts.chans,testInd);
            data.XfftTrain(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,trainInd);
            data.XfftTest(:,:,idxTestB:idxTestE) = Xfft(:,dataOpts.chans,testInd);
        end
        data.labTrain(idxB:idxE,1) = labels(trainInd);
        data.labTest(idxTestB:idxTestE,1) = labels(testInd);
        data.sessionLabTrain(idxB:idxE,1) = sess;
        data.sessionLabTest(idxTestB:idxTestE,1) = sess;
    else
        data.Wm(sess,1) = size(X,3);
        idxB = idxE + 1;
        idxE = idxB + data.Wm(sess,1) - 1;
        if sess == 1
            data.X = X(:,dataOpts.chans,:);
            data.Xfft = Xfft(:,dataOpts.chans,:);
        else
            data.X(:,:,idxB:idxE) = X(:,dataOpts.chans,:);
            data.Xfft(:,:,idxB:idxE) = Xfft(:,dataOpts.chans,:);
        end
        data.lab(idxB:idxE,1) = labels;
        data.sessionLab(idxB:idxE,1) = sess;
    end
    
    clear X labels Xfft X_home X_fit time_fit
end

% store DFT frequency locations
data.sampRate = data.N/dataOpts.numSeconds;
data.tau = 0:1/data.sampRate:(dataOpts.numSeconds-1/data.sampRate);
data.s = (data.sampRate/data.N):(data.sampRate/data.N):(data.sampRate/2);

end