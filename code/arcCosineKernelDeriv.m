function dG = arcCosineKernelDeriv(U,V,n)
% U is an 1-by-p matrix.
% V is an n-by-p matrix.
% n is the family of activation function.
%     n=0 -- step
%     n=1 -- ramp
%     n=2 -- quarter-pipe
% G is an p-by-n Gram matrix of the rows of U and V.

if nargin < 2, V = U; end
if nargin < 3, n = 0; end

Unorm = sqrt(sum(U.^2,2));
Vnorm = sqrt(sum(V.^2,2));

zeta = bsxfun(@rdivide,U,Unorm)*bsxfun(@rdivide,V,Vnorm)';

dzeta = bsxfun(@rdivide,V,Vnorm)'/Unorm - ...
    bsxfun(@times,U'/Unorm,(U*V')./Vnorm');

dtheta = -1*dzeta*diag(max((1-zeta.^2),eps).^(-1/2));

dG = -1/pi*dtheta;

end