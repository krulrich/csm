function oneHotLabels = oneHot(labels,valueLabels)

% Takes a vector of size n by 1 as input and creates a one-hot encoding of its
% elements.
if nargin < 2
    valueLabels = unique(labels);
end
nLabels = length(valueLabels);
nSamples = numel(labels);

oneHotLabels = zeros(nLabels, nSamples);

for i = 1:nLabels
	oneHotLabels(i,:) = (labels == valueLabels(i));
end