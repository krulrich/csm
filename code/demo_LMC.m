clear; format short e; close all;

%% Set Parameters
C = 3; % number of channels
Q = 3; % number of Gaussian components in SM kernel
R = 2; % rank of coregionalization matrices

rng(999) % set random number seed

%% Generate data from CSM kernel
% randomly generate a GP model with a CSM kernel
% the LMC class defaults to the use of SG kernels with complex 
% coregionalization matrices
model = GP.LMC(C,Q,R);
model.normalizeCovariance();

% show cross-spectrum
figure(1), model.plotCsd;

% sample W windows of data from model, each window has N data points
W = 20; N = 100; sampRate = 100; delta = 1/sampRate;
y = zeros(N,C,W);
for w = 1:W
    y(:,:,w) = model.sample(N,delta);
end

% plot an example draw from the C-channel GP
tau = 0:delta:(N-1)*delta;
figure(2), plot(tau,y(:,:,1))

%% Fit new CSM kernel to data
fprintf('Fit new CSM kernel to data ...\n')
% create a new GP model
Q = 10; % allow the number of components to be large
modelFit = GP.LMC(C,Q,R);

% fit the model through the resilient backpropagation algorithm
figure(3);
opts.iters = 200;
opts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
plotOpts.ax = gca;
algorithms.rprop(tau,y,modelFit,opts,plotOpts);

% show the fitted cross-spectrum
figure(4), modelFit.plotCsd;

%% Fit new CSM kernel to data using the DFT approximation
fprintf('Fit new CSM kernel to data using DFT approximation ...\n')
pause(1)

% create a new GP model, now using the LMC_DFT class
modelFitDFT = GP.LMC_DFT(C,Q,R);

% obtain the DFT coefficients of the observations
s = (sampRate/N):(sampRate/N):(sampRate/2);
Ns = ceil(N/2);
yfft = 1/sqrt(N)*fft(y);
yfft = 2*(yfft(2:Ns+1,:,:));

% fit the DFT model through the resilient backpropagation algorithm
figure(3);
plotOpts.color = 'r';
plotOpts.legendNames{2} = 'DFT Approx';
algorithms.rprop(s,yfft,modelFitDFT,opts,plotOpts);

% show the fitted cross-spectrum
figure(5), modelFitDFT.plotCsd;
