classdef dCSFA_logistic < handle
  properties
    W % number of time windows (observations) per animal (A-dim vector)
    C % number of channels
    Q % number of components in SM kernel
    L % number of factors in mixture
    dL % number of discriminitive factors
    lambda % lagrange multiplier (tuning parameter)
    
    kernel % LMC_FA kernel
    
    model % classifier
  end
  
  methods
    function self = dCSFA_logistic(modelOpts,dataOpts,kernels)
      warning('error','stats:glmfit:IterationLimit');
      if nargin > 0
        self.W = modelOpts.W;
        self.C = modelOpts.C;
        self.Q = modelOpts.Q;
        self.L = modelOpts.L;
        if modelOpts.dL > modelOpts.L
          error(['Specified number of discriminitive factors is greater than'...
            'total number of factors'])
        end
        self.dL = modelOpts.dL;
        self.lambda = modelOpts.lambda;
        
        if nargin < 3
          self.kernel = GP.LMC_FA(modelOpts,dataOpts);
        else
          self.kernel = GP.LMC_FA(modelOpts,dataOpts,kernels);
        end
      end
    end
    
    function res = evaluate(self,x,y)
      res = self.kernel.evaluate(x,y);
    end
    
    function res = getParams(self)
      res = self.kernel.getParams;
    end
    
    function setParams(self,params)
      self.kernel.setParams(params);
    end
    
    function [lb,ub] = getBounds(self)
      [lb,ub] = self.kernel.getBounds;
    end
    
    function grad = gradient(self,s,data,opts)
      % 1) obtain gradient and current kernel parameter values
      grad = self.kernel.gradient(s,data,opts); % obtain gradient vector
      params = self.kernel.getParams; % obtain current parameter value vector
      logScores = reshape(params(end-self.L*self.W+1:end),[self.L,self.W]); % extract the log of factor scores from parameter values
      
      lossTot = 0; % initialize a loss measure
      
      for l = 1:size(opts.labels,2) % loop over all classes in the classification problem
        lInds = 1:self.dL; %(nL*(l-1)+1):(nL*l);
        scores = exp(logScores(lInds,:)); % convert logScores to scores (gradients taken wrt log scores)
        
        % 2)
        labels = opts.labels;
        if ~isfield(opts,'groups')
          classModel = fitglm(scores',labels,'Distribution','binomial');
          b = classModel.Coefficients.Estimate;
          coeffs = b(2:end);
        else % want to model group effects by adding mouse as one hot regressors
          features = [scores' util.oneHot(opts.groups)'];
          classModel = fitglm(features,labels,'Distribution','binomial',...
            'Intercept',false);
          b = classModel.Coefficients.Estimate;
          n = numel(unique(opts.groups));
          coeffs = b(1:end-n);
        end
        
        % 3) gradient of cross-entropy loss
        yHat = classModel.Fitted.Probability;
        gradCE = -coeffs*(labels - yHat');
        
        %                 gradientCheck
        
        % 4) inject gradient of hinge loss into kernel gradient
        gradCE2 = zeros(self.L,self.W);
        gradCE2(lInds,:) = gradCE;
        grad(end-self.L*self.W+1:end) = grad(end-self.L*self.W+1:end) - ...
          self.lambda*reshape(gradCE2,[self.L*self.W,1]);
        
        lossTot = lossTot+...
          mean(-labels'.*log(yHat)-(1-labels').*log(1-yHat));
      end
      %fprintf('%d\n',lossTot)
    end
    
    % Make a copy of a handle object.
    function new = copy(self)
      % Instantiate new object of the same class.
      new = feval(class(self));
      
      % Copy all non-hidden properties.
      p = properties(self);
      for i = 1:length(p)
        if ismethod(self.(p{i}),'copy')
          new.(p{i}) = self.(p{i}).copy;
        else
          new.(p{i}) = self.(p{i});
        end
      end
    end
  end
end
