classdef LMC_FA < handle & GP.spectrumPlots
    properties
        % model parameters
        P % number of partitions of data
        W % number of time windows (observations) per partition
        maxW % maximum number of time windows in a partition
        C % number of channels
        Q % number of components in SM kernel
        L % rank of FA
        logScores % log of factor scores
        
        LMCkernels % cell array of L LMC kernels
        
        gamma % additive Gaussian noise
        
        updateKernels % biniary indicator for kernel updates
    end
    
    methods
        function self = LMC_FA(modelOpts,dataOpts,kernels)
            if nargin > 0
                % set model parameters
                self.L = modelOpts.L;
                self.C = modelOpts.C;
                self.Q = modelOpts.Q;
                self.gamma = 200;
                
                % divide windows into partitions for computations
                self.maxW = modelOpts.maxW;
                self.P = ceil(modelOpts.W/self.maxW);
                self.W(1:(self.P-1)) = self.maxW;
                self.W(self.P) = modelOpts.W - self.maxW*(self.P-1);
                
                % initialize factor scores
                self.logScores = log(rand(self.L,modelOpts.W));
                
                % create and initialize L LMC kernels
                if nargin < 3
                    self.LMCkernels = cell(self.L,1);
                    for l = 1:self.L
                        % initialize set of SG kernels
                        means = num2cell(dataOpts.lowFreq + ...
                            (dataOpts.highFreq-dataOpts.lowFreq)* ...
                            rand(1,self.Q));
                        k = cellfun(@(mu,var) GP.kernels.SG(mu,var, ...
                            [dataOpts.lowFreq,dataOpts.highFreq],[1e-2,1.5e3]), ...
                            means,num2cell(rand(1,self.Q)), 'un', 0);
                        kernels = GP.kernels.kernels(self.Q, k);
                        
                        % initialize coregionalization matrices
                        coregs = GP.coregs.mats(self.C,self.Q,modelOpts.R);
                        
                        % form GP with CSM kernel
                        self.LMCkernels{l} = GP.LMC_DFT(self.C,self.Q, ...
                            modelOpts.R,coregs,kernels,inf);
                        self.LMCkernels{l}.updateNoise = false;
                    end
                else
                    self.LMCkernels = kernels;
                end
                
                self.updateKernels = true;
            end
        end
        
        function res = evaluate(self,x,data)
            Ns = numel(x);
            Np = self.P;
            Nc = self.C;
            Nl = self.L;
            
            UKUlStore = zeros(Nc^2*Ns,Nl);
            [rows,cols] = find(logical(kron(speye(Ns),ones(Nc))));
            
            for l = 1:Nl
                opts.block = true;
                opts.smallFlag = false;
                [~,UKUl] = self.LMCkernels{l}.UKU(x,opts);
                vals = self.LMCkernels{l}.extractBlocks(UKUl);
                UKUlStore(:,l) = vals(:);
            end
            
            res = 0;
            for p = 1:Np % loop through partitions
                inds = ((p-1)*self.maxW+1):((p-1)*self.maxW+self.W(p)); % get the indices associated with partition
                scoresAllMem = exp(self.logScores(:,inds)); % extract scores for partition p
                if isobject(data)||isstruct(data), yP = conj(data.y(:,:,inds)); % extract data for partition p if structure is provided
                else yP = data(:,:,inds); end % extract data for partition p if array is provided
                
                for w = 1:self.W(p) % loop through windows for partition p
                    vals = sum(bsxfun(@times,UKUlStore, ...
                        permute(scoresAllMem(:,w),[2,1])),2); % get nonzero values
                    UKU = sparse(rows,cols,vals,Nc*Ns,Nc*Ns); % put into sparse matrix
                    UKU = UKU + 1/self.gamma*speye(Nc*Ns); % add white noise
                    
                    UKU = 2*UKU; % conversion to complex covariance matrix
                    
                    y = yP(:,:,w).'; % data for window w
                    y = y(:); % vectorization of window w data
                    
                    logDetUKU = full(2*sum(log(diag(chol(UKU))))); % log determinant of covariance matrix
                    
                    res = res - Nc*Ns*log(pi) - logDetUKU - y'*(UKU\y); % complex normal pdf
                end
            end
            res = real(res); % remove machine precision complex values
        end
        
        function res = getParams(self)
            if self.updateKernels
                params = cellfun(@(x)x.getParams,self.LMCkernels,'un',0);
                res = vertcat(params{:},self.logScores(:));
            else
                res = self.logScores(:);
            end
        end
        
        function setParams(self,params)
            indB = 1;
            if self.updateKernels
                for l = 1:self.L
                    indE = indB + self.LMCkernels{l}.nParams - 1; % get param indices
                    self.LMCkernels{l}.setParams(params(indB:indE)); % update parameters
                    self.LMCkernels{l}.normalizeCovariance(); % normalize to correlation function for identifiability
                    indB = indE + 1;
                end
            end
            self.logScores = reshape(params(indB:end),[self.L,sum(self.W)]);
        end
        
        function [lb,ub] = getBounds(self)
            lb = -10*ones(self.L*sum(self.W),1);
            ub = 10*ones(self.L*sum(self.W),1);
            if self.updateKernels
                [lball,uball] = cellfun(@(x)x.getBounds,self.LMCkernels,'un',0);
                lb = vertcat(lball{:},lb);
                ub = vertcat(uball{:},ub);
            end
        end
        
        function grad = gradient(self,s,data,opts)
            Ns = numel(s); % number of frequency bins
            Np = self.P; % number of partitions
            Nc = self.C; % number of channels
            Nl = self.L; % number of latent factors
            
            kderiv = cell(self.L,1); % initialize derivative storage for covariance functions
            Bderiv = cell(self.L,1); % initialize derivative storage for coregionalization matrices
            UKUlStore = zeros(Nc^2*Ns,Nl); % initialize storage of vectorized covariance matrices for each factor
            [rows,cols] = find(logical(kron(speye(Ns),ones(Nc)))); % get indices of non-zero values in covariance matrices
            
            for l = 1:Nl
                [kderiv{l},Bderiv{l}] = self.LMCkernels{l}.deriv(s); % compute derivatives of parameters for each factor
                
                % store Gram matrix for each factor (for speedup)
                opts.block = true;
                opts.smallFlag = false;
                [~,UKUl] = self.LMCkernels{l}.UKU(s,opts); % get factor l Gram matrix
                vals = self.LMCkernels{l}.extractBlocks(UKUl); % extract non-zero locations of factor l Gram matrix
                UKUlStore(:,l) = vals(:); % store factor l Gram matrix
            end
            
            sdAll = UKUlStore; % spectral density all (sdAll will go on gpu if available)
            kdAll = single(horzcat(kderiv{:})); % size Ns x L*nParams; convert to single for gpu efficiency
            BdAll = single(horzcat(Bderiv{:})); % size C^2 x L*nParams; convert to single for gpu efficiency
            
            nParams = self.LMCkernels{1}.nParams; % number of parameters
            
            
            LMCgrad = zeros(nParams,self.L); % initialize gradient storage for factor parameters
            sgradAll = cell(Np,1); % initialize gradient storage for factor score parameters
            
            % put derivatives onto GPU (if available) for computations
            if gpuDeviceCount > 0
                kdAll = gpuArray(kdAll);
                BdAll = gpuArray(BdAll);
                LMCgrad = gpuArray(LMCgrad);
                sdAll = gpuArray(sdAll);
            end
            
            for p = 1:Np % loop through all animals
                inds = ((p-1)*self.maxW+1):((p-1)*self.maxW+self.W(p)); % obtain indices for partition p
                
                if isobject(data)||isstruct(data), y = data.y(:,:,inds); % extract data for partition p if structure is provided
                else y = data(:,:,inds); end % extract data for partition p if array is provided
                
                y = conj(y)/2; % circular symmetry correction
                
                sgrad = zeros(self.L,self.W(p)); % initialize gradient storage for factor scores in partition p
                scoresAllMem = exp(self.logScores(:,inds)); % extract factor scores for partition p
                if gpuDeviceCount > 0 % place arrays on gpu if available
                    sgrad = gpuArray(sgrad);
                    scoresAll = gpuArray(scoresAllMem);
                else scoresAll = scoresAllMem;
                end
                
                % obtain inverse of Gram matrix for each window in the
                % partition.  Use parfor for minor speedup
                UKUinvP = zeros(Nc,Nc,Ns,self.W(p)); % initialize storage for Gram matrix inverse
                gam = self.gamma;
                parfor w = 1:self.W(p)
                    % get UKU for window w
                    vals = sum(bsxfun(@times,UKUlStore, ...
                        permute(scoresAllMem(:,w),[2,1])),2); % get nonzero values
                    UKU = sparse(rows,cols,vals,Nc*Ns,Nc*Ns); % put into sparse matrix
                    UKU = UKU + 1/gam*speye(Nc*Ns); % add white noise
                    
                    % get inverse of UKU
                    [Qd,Rd] = qr(UKU); % qr decomposition
                    UKUinv = Qd/(Rd'); % inverse using qr
                    UKUinvP(:,:,:,w) = reshape( ...
                        self.LMCkernels{1}.extractBlocks(UKUinv),[Nc,Nc,Ns]); % reshape for Ag
                end
                
                for w = 1:self.W(p) % loop through all windows for animal a
                    UKUinv = UKUinvP(:,:,:,w); % extract Gram matrix inverse for window w
                    
                    Ag = self.getA(y(:,:,w),UKUinv); % get A for gradient computations
                    Ag = single(reshape(Ag,[Nc^2,Ns])); % vectorize, convert to single for gpu efficiency
                    Agt = Ag'; % get transpose of A (used for inner product)
                    
                    % put derivatives onto GPU for computations
                    if gpuDeviceCount > 0
                        Agt = gpuArray(Agt);
                        Ag = gpuArray(Ag);
                    end
                    
                    if self.updateKernels
                        if gpuDeviceCount > 0 % gradient for window w on gpu
                            gradW = Agt*BdAll;
                            gradW = arrayfun(@times,gradW,kdAll);
                        else % gradient for window w on cpu
                            gradW = (Agt*BdAll).*kdAll;
                        end
                        gradW = sum(real(gradW),1).'; % gradient requires trace (sum operation)
                        LMCgrad = LMCgrad + bsxfun(@times, scoresAll(:,w)', ...
                            reshape(gradW,[nParams,Nl]) ); % add window w contribution to gradient
                    end
                    
                    sgrad(:,w) = scoresAll(:,w).*real(Ag(:)'*sdAll)'; % gradient for window w factor scores
                end
                if gpuDeviceCount > 0 % gather factor score gradients from gpu
                    sgradAll{p} = gather(sgrad);
                else sgradAll{p} = sgrad;
                end
            end
            
            sgrad = horzcat(sgradAll{:}); % convert sgrad into a vector
            
            if gpuDeviceCount > 0 % gather factor gradients from gpu
                LMCgrad = gather(LMCgrad);
            end
            
            if self.updateKernels % concatenate gradients if factors (kernels) are being updated
                grad = vertcat(LMCgrad(:),sgrad(:));
            else % otherwise the gradient vector is just the factor score gradient vector
                grad = sgrad(:);
            end
            grad = double(grad); % cast back to double
        end
        
        function res = UKU(self,s,n,UKUlstore)
            Ns = numel(s);
            
            res = bsxfun(@times,1/self.gamma*eye(self.C), ones([1,1,Ns]));
            if ~exist('UKUlstore','var')
                for l = 1:self.L
                    res = res + exp(self.logScores(l,n)) * ...
                        self.LMCkernels{l}.extractBlocks(self.UKUl(s,l));
                end
            else
                res = res + sum(bsxfun(@times,UKUlstore, ...
                    permute(exp(self.logScores(:,n)),[2,3,4,1])),4);
            end
        end
        
        function res = UKUl(self,s,l)
            Nc = self.C;
            Ns = numel(s);
            d = 1/(2*Ns*(s(2)-s(1)));
            SD = self.LMCkernels{l}.kernels.specDens(s);
            
            res = spalloc(Ns*Nc,Ns*Nc,Nc^2*Ns);
            for q = 1:self.Q
                B = self.LMCkernels{l}.coregs.getMat(q);
                res = res + 1/d/2*kron(spdiags(SD(:,q),0,Ns,Ns),B);
            end
        end
        
        function plotCsd(self,n,varargin)
            % plot the cross-spectral density for window n
            p = inputParser;
            p.KeepUnmatched = true;
            addOptional(p,'minFreq',0,@isnumeric);
            addOptional(p,'maxFreq',30,@isnumeric);
            parse(p,varargin{:});
            opts = p.Results;
            
            s = linspace(opts.minFreq,opts.maxFreq,1e3);
            UKU = self.UKU(s,n);
            
            plotCsd@GP.spectrumPlots(s,UKU,varargin{:})
        end
        
        function keepChannels(self,chans)
            % replace all properties with only the channels in chans
            % NOTE: this will remove data!
            self.C = numel(chans);
            for l = 1:self.L
                self.LMCkernels{l}.keepChannels(chans);
            end
        end
        
        function plotRelativeCsd(self,l,varargin)
            % plot the spectral density normalized accross all factors.
            % This gives the *relative* impact of each factor as a function
            % of frequency.
            p = inputParser;
            p.KeepUnmatched = true;
            addOptional(p,'minFreq',0,@isnumeric);
            addOptional(p,'maxFreq',30,@isnumeric);
            parse(p,varargin{:});
            opts = p.Results;
            
            s = linspace(opts.minFreq,opts.maxFreq,1e3);
            
            UKU = zeros(self.C,self.C,1e3,self.L);
            optsUKU.smallFlag = true;
            optsUKU.block = true;
            for j = 1:self.L
                UKU(:,:,:,j) = self.LMCkernels{j}.UKU(s,optsUKU);
            end
            UKUnorm = bsxfun(@rdivide,UKU,sum(abs(UKU),4));
            
            GP.spectrumPlots.plotCsd(s,UKUnorm(:,:,:,l),varargin{:})
        end
        
        % Make a copy of a handle object.
        function new = copy(self)
            % Instantiate new object of the same class.
            new = feval(class(self));
            
            % Copy all non-hidden properties.
            p = properties(self);
            for i = 1:length(p)
                % check if property is actually a cell array of properties
                if  iscell(self.(p{i}))
                    % instantiate new cell array
                    new.(p{i}) = cell(size(self.(p{i})));
                    for j = 1:numel(self.(p{i}))
                        % if property is a handle, deep copy
                        if ismethod(self.(p{i}){j},'copy')
                            new.(p{i}){j} = self.(p{i}){j}.copy;
                        else
                            new.(p{i}){j} = self.(p{i}){j};
                        end
                    end
                else
                    % if property is a handle, deep copy
                    if ismethod(self.(p{i}),'copy')
                        new.(p{i}) = self.(p{i}).copy;
                    else
                        new.(p{i}) = self.(p{i});
                    end
                end
            end
        end
        
    end
    
    methods(Static)
        function Akeep = getA(data,UKUinv)
            % get matrix A = a*a' - W*K^{-1} for GP gradient steps
            alpha1 = sum(bsxfun(@times,UKUinv,permute((data),[3,2,1])),2);
            alpha2 = sum(bsxfun(@times,UKUinv,permute(conj(data),[2,3,1])),1);
            Akeep = bsxfun(@times,alpha1,alpha2) - UKUinv;
        end
    end
end
