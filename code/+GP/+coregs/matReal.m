classdef matReal < handle
    % A complex coregionalization matrix
    
    properties
        % model parameters
        C % number of channels
        R % rank of coregionalization matrix
        
        weights % real component of coregionalization matrix    
        weightsB % upper/lower bounds on weights
    end
    
    methods
        function self = matReal(C,R,weights,weightsB)
            self.C = C;
            self.R = R;
            
            if ~exist('weights','var')
                self.weights = 1/100*rand(C,R);
                self.weightsB = cat(3,eps*ones(C,R),1e2*ones(C,R));
            else
                self.weights = weights;
                self.weightsB = weightsB;
            end
        end
        
        function res = getMat(self)
            w = self.weights;
            res = w*w';
        end
        
        function res = getParams(self)
            res = self.weights(:);
        end
        
        function setParams(self,params)
            %warning('off','all')
            %w = reshape(params,[self.C,self.R]);
            %[u,v] = nnmf(w*w',self.R);
            %self.weights = sqrt(u.*v');
            self.weights = reshape(params,[self.C,self.R]);
        end
        
        function [lb,ub] = getBounds(self)
            lb = self.weightsB(:,:,1);
            ub = self.weightsB(:,:,2);
            lb = lb(:); ub = ub(:);
        end
        
        function res = deriv(self)
%             tic;
%             res = zeros(self.C,self.C,self.R*self.C);
%             for r = 1:self.R
%                 w = self.weights(:,r);
%                 res(sub2ind(size(res),1:self.C,
%                 for c = 1:self.C
%                     res(:,c,(r-1)*self.C+c) = w;
%                     res(c,:,(r-1)*self.C+c) = w;
%                     res(c,c,(r-1)*self.C+c) = 2*w(c);
%                 end
%             end
%             toc
%             tic
%             res = mat2cell(res,self.C,self.C,ones(self.C*self.R,1));
%             res = vertcat(res(:));
%             toc
            
            %tic;
            cvals = repmat(1:self.C,[1,self.R])';
            rvals = kron(1:self.R,ones(1,self.C))';
            
            %tic
            
            %res = zeros(self.C,self.C,numel(cvals));
            w = self.weights;
            
            res = cell(size(cvals));
            res(:) = {zeros(self.C)};
            
            for i = 1:numel(cvals)
                c = cvals(i); r = rvals(i);
                %w = self.weights(:,r);  
                
                %tic
                %res(:,c,i) = res(:,c,i) + w(:,r);
                %res(c,:,i) = res(c,:,i) + w(:,r)';
                %toc
                
%                 tic
%                 %res{i} = zeros(self.C);
                 res{i}(:,c) = res{i}(:,c) + w(:,r);
                 res{i}(c,:) = res{i}(c,:) + w(:,r)';
%                 toc
%                 
%                 res{i}(c,:) = res{i}(c,:) + w';
%                 toc
%                 
%                 tic
%                 vc = zeros(self.C,1); vc(c) = 1;
%                 res{i} = w*vc' + vc*w';
%                 toc
            end
            %res = squeeze(mat2cell(res,self.C,self.C,ones(numel(cvals),1)));
            %toc
            
            %tic
            %res = arrayfun(@(c,r)self.derivWeight(c,r),cvals,rvals,'un',0);
            %toc
            %toc
        end
        
        function res = derivWeight(self,c,r)
            w = self.weights(:,r);            
            vc = zeros(self.C,1); vc(c) = 1;
            res = w*vc' + vc*w';
        end
                
        function res = nParams(self)
            res = self.C*self.R;
        end
    end 
end