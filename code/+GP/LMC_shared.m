classdef LMC_shared < handle
    properties
        % model parameters
        N
        nkParams % number of kernel parameters
        
        LMCkernels
    end
    
    methods
        function self = LMC_shared(LMCkernels)
            self.LMCkernels = LMCkernels;
            self.N = numel(LMCkernels);
            self.nkParams = LMCkernels{1}.kernels.nParams;
        end
        
        function res = evaluate(self,x,y)
            C = reshape(mat2cell(y,size(y,1),size(y,2),ones(size(y,3),1)),[self.N,1]);
            res = cellfun(@(z,yn)z.evaluate(x,yn),self.LMCkernels,C,'un',0);
            res = sum(cell2mat(res));
        end
        
        function res = getParams(self)
            params = cellfun(@(x)x.getParams,self.LMCkernels,'un',0);
            Bparams = cellfun(@(x)x(1:end-self.nkParams),params,'un',0);
            kparams = params{1}(end-self.nkParams+1:end);
            res = vertcat(Bparams{:},kparams);
        end
        
        function setParams(self,params)
            kparams = params(end-self.nkParams+1:end);
            nBParams = (numel(params)-self.nkParams)/self.N;
            indB = 1;
            for n = 1:self.N
                indE = indB + nBParams - 1;
                self.LMCkernels{n}.setParams(vertcat(params(indB:indE),kparams));
                indB = indE + 1;
            end
        end
        
        function [lb,ub] = getBounds(self)
            [lball,uball] = cellfun(@(x)x.getBounds,self.LMCkernels,'un',0);
            lbBparams = cellfun(@(x)x(1:end-self.nkParams),lball,'un',0);
            ubBparams = cellfun(@(x)x(1:end-self.nkParams),uball,'un',0);
            lbkparams = lball{1}(end-self.nkParams+1:end);
            ubkparams = uball{1}(end-self.nkParams+1:end);
            lb = vertcat(lbBparams{:},lbkparams);
            ub = vertcat(ubBparams{:},ubkparams);
        end
        
        function grad = gradient(self,x,y,opts)
            nBParams = (numel(self.getParams)-self.nkParams)/self.N;
            kgrad = zeros(self.nkParams,self.N);
            Bgrad = zeros(nBParams,self.N);
            for n = 1:self.N
                ngrad = self.LMCkernels{n}.gradient(x,y(:,:,n),opts);
                Bgrad(:,n) = ngrad(1:nBParams);
                kgrad(:,n) = ngrad(nBParams+1:end);
            end
            grad = [reshape(Bgrad,[numel(Bgrad),1]); sum(kgrad,2)];
        end
    end
end