classdef LMC_FA_SVM_v3 < handle
    properties
        W % number of time windows (observations) per animal (A-dim vector)
        C % number of channels
        Q % number of components in SM kernel
        L % number of factors in mixture
        lambda % lagrange multiplier (SVM tuning parameter)
        
        kernel % LMC_FA kernel
        
        SVMModel % classifier
    end
    
    methods
        function self = LMC_FA_SVM_v3(modelOpts,dataOpts)
            if nargin > 0
                self.W = modelOpts.W;
                self.C = modelOpts.C;
                self.Q = modelOpts.Q;
                self.L = modelOpts.L;
                self.lambda = modelOpts.lambda;
                self.kernel = GP.LMC_FA(modelOpts,dataOpts);
            end
        end
        
        function res = evaluate(self,x,y)
            res = self.kernel.evaluate(x,y);
        end
        
        function res = getParams(self)
            res = self.kernel.getParams;
        end
        
        function setParams(self,params)
            self.kernel.setParams(params);
        end
        
        function [lb,ub] = getBounds(self)
            [lb,ub] = self.kernel.getBounds;
        end
        
        function grad = gradient(self,s,data,opts)
            % 1) obtain gradient and current kernel parameter values
            grad = self.kernel.gradient(s,data,opts); % obtain gradient vector
            params = self.kernel.getParams; % obtain current parameter value vector
            logScores = reshape(params(end-self.L*self.W+1:end),[self.L,self.W]); % extract the log of factor scores from parameter values
                        
            lossTot = 0; % initialize a loss measure
	    nL = 6;
            
            for l = 1:size(opts.labels,2) % loop over all classes in the classification problem
		lInds = 1:nL; %(nL*(l-1)+1):(nL*l);
                scores = exp(logScores(lInds,:)); % convert logScores to scores (gradients taken wrt log scores)
                
                labels = opts.labels(:,l);

		if sum(labels==1) < sum(labels==-1) % if more negative examples than positive examples
		    [~,idx] = datasample(scores,sum(labels==-1),2,'Weights',double(labels==1)); % upsample positive examples
		    labelsSVM = [labels(idx); labels(labels==-1)];
		    scoresSVM = [scores(:,idx), scores(:,labels==-1)];
		else % if more positive examples than negative examples
		    [~,idx] = datasample(scores,sum(labels==1),2,'Weights',double(labels==-1)); % upsample negative examples
		    labelsSVM = [labels(labels==1); labels(idx)];
		    scoresSVM = [scores(:,labels==1), scores(:,idx)];
		end
                                
                % 2) obtain the SVM weights on classifying factor scores
                cmodel = fitcsvm(scoresSVM',labelsSVM);%,'KernelFunction',opts.kernelFunction);
                self.SVMModel{l} = cmodel; % store classification model
                
                % 3) compute gradient of this hinge loss function
                %loss = max(1 - (labels .* (scores'*cmodel.Beta + cmodel.Bias)),0); % compute loss
                lossInd = (1 - (labels .* (scores'*abs(cmodel.Beta) + cmodel.Bias))) > 0; % find support vectors
                gradHL = -1 * bsxfun(@times, (lossInd .* labels)',abs(cmodel.Beta)) .* scores; % take gradient of hinge loss function
                
                % 4) inject gradient of hinge loss into kernel gradient
                gradHL2 = zeros(self.L,self.W);
                gradHL2(lInds,:) = gradHL;
                grad(end-self.L*self.W+1:end) = grad(end-self.L*self.W+1:end) - ...
                    self.lambda*reshape(gradHL2,[self.L*self.W,1]);
                
                %lossTot = lossTot + sum(abs(loss(:)));
                lossTot = lossTot + cmodel.resubLoss / numel(unique(opts.labels)); % compute loss measure
            end
            %fprintf('%d\n',lossTot)
        end
        
        % Make a copy of a handle object.
        function new = copy(self)
            % Instantiate new object of the same class.
            new = feval(class(self));
            
            % Copy all non-hidden properties.
            p = properties(self);
            for i = 1:length(p)
                if ismethod(self.(p{i}),'copy')
                    new.(p{i}) = self.(p{i}).copy;
                else
                    new.(p{i}) = self.(p{i});
                end
            end
        end
    end
end
