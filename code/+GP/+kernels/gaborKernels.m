classdef gaborKernels < handle
    % Stores Q kernels for multi-output Gaussian processes
    
    properties
        % model parameters
        Q % Number of kernels
        nu
        highFreq
        lowFreq
        delta
    end
    
    methods
        function self = gaborKernels(Q,lowFreq,highFreq)
            if nargin > 0
                self.Q = Q;
                if ~exist('lowFreq','var'), lowFreq = 2; end
                if ~exist('highFreq','var'), highFreq = 30; end
                self.highFreq = highFreq;
                self.lowFreq = lowFreq;
                self.nu = highFreq*4;
                self.delta = -1/(Q-1)*log2(lowFreq/highFreq);
            end
        end
        
        function res = getParams(self)
            res = self.nu;
        end
        
        function [lb,ub] = getBounds(self)
            lb = 0;
            ub = inf;
        end
        
        function res = nParams(self)
            res = 1;
        end
        
        function qvals = getKernelNums(self)
            qvals = (1:self.Q)';
        end
        
        function setParams(self,params)
            self.nu = params;
        end
        
        function res = covFun(self,tau,qvals)
            if nargin<3, qvals = 1:self.Q; end
            
            res = zeros(numel(tau),numel(qvals));
            
            for qnum = 1:numel(qvals)
                var = 2.^(-2*self.delta*(qvals(qnum)-1)-1) * self.nu;
                mu = 2.^(-self.delta*(qvals(qnum)-1)) * self.highFreq;
                res(:,qnum) = exp(-2*pi^2*tau.^2*var + 2i*pi*tau*mu);
            end
        end
        
        function res = specDens(self,s,qvals)
            if nargin<3, qvals = 1:self.Q; end
            
            res = zeros(numel(s),numel(qvals));
            
            for qnum = 1:numel(qvals)
                var = 2.^(-2*self.delta*(qvals(qnum)-1)-1) * self.nu;
                mu = 2.^(-self.delta*(qvals(qnum)-1)) * self.highFreq;
                res(:,qnum) = 1/sqrt(2*pi*var)*exp(-1/2/var*(mu-s).^2);
            end
            
            if qnum==1, res = res.'; end
        end
        
        function plotSpecDens(self,minFreq,maxFreq)
            s= linspace(minFreq,maxFreq,1e3);
            plot(s,self.specDens(s),'LineWidth',2);
            xlabel('Frequency','FontSize',14)
        end
        
        function res = derivCovFun(self,tau)
            tau = reshape(tau,[numel(tau),1]);
            res = bsxfun(@times, -2*pi^2*tau.^2, self.covFun(tau) );
            res = mat2cell(res,numel(tau),ones(self.Q,1))';
        end
        
        function res = derivSpecDens(self,s)
            res = cell(self.Q,1);
            s = reshape(s,[1,numel(s)]);
            for q = 1:self.Q
                var = 2.^(-2*self.delta*(q-1)-1) * self.nu;
                mu = 2.^(-self.delta*(q-1)) * self.highFreq;
                res{q} = 1/(2*var^2)*((s-mu).^2-var) .* self.specDens(s,q);
            end
        end
        
        % Make a copy of a handle object.
        function new = copy(self)
            % Instantiate new object of the same class.
            new = feval(class(self));
 
            % Copy all non-hidden properties.
            p = properties(self);
            for i = 1:length(p)
                new.(p{i}) = self.(p{i});
            end
        end
    end
end
