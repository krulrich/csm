classdef SE < handle
    % The squared exponential (SE) covariance kernel
    
    properties
        % model parameters
        l % length scale
        
        % bounds on moddel parameters
        lB % upper/lower bounds on length scale
        
        nParams % number of parameters in this kernel
    end
    
    methods
        function self = SE(l,lB)
            self.l = l;
            self.lB = lB;
            self.nParams = 1;
        end
        
        function res = getParams(self)
            res = self.l;
        end
        
        function setParams(self,params)
            self.l = params;
        end
        
        function [lb,ub] = getBounds(self)
            lb = self.lB(1);
            ub = self.lB(2);
        end
        
        function res = derivCovFun(self,tau)
            res{1,1} = self.derivCovFunL(tau);
        end
        
        function res = derivSpecDens(self,s)
            res{1,1} = self.derivSpecDensL(s);
        end
        
        function res = covFun(self,tau)
            res = exp(-2*pi^2*tau.^2*self.l);
        end
        
        function res = specDens(self,s)
            res = 1/sqrt(2*pi*self.l)*exp(-1/2/self.l*s.^2);
        end
        
        function res = derivCovFunL(self,tau)
            res = -2*pi^2*tau.^2 .* self.covFun(tau);
        end
        
        function res = derivSpecDensL(self,s)
            res = 1/(2*self.l^2)*(s.^2-self.l) .* self.specDens(s);
        end
    end
end
