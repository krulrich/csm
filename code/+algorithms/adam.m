function evals = adam(x,y,model,opts,plotOpts)

evals = zeros(1,opts.iters);
curLine = [];
tic

rmsd = 0.9;
lambda = 1e-1;
beta = 0.5;
stepsize = 1e-2;

history = model.gradient(x,y,opts);
step = zeros(size(history));

for iter=1:opts.iters
    % get parameters, gradients, and bounds
    params = model.getParams();
    g = model.gradient(x,y,opts);
    [lb,ub] = model.getBounds();
    
    % adjust step sizes
    step = beta*step + (1-beta)*g;
    history = rmsd*history + (1-rmsd)*g.^2;
    L = lambda + sqrt(history);
    cat = step./L;
    
    % take step
    params = params + stepsize*cat;
    
    % constrain parameters
    params(params<lb) = lb(params<lb);
    params(params>ub) = ub(params>ub);
     
    % Update parameters
    if sum(~isfinite(params))
        keyboard;
    end
    model.setParams(params);
    
    % Check performance
    evals(iter) = model.evaluate(x,y);
    if sum(~isfinite(evals))
        keyboard;
    end
    
    % Show performance every so often
    if mod(iter,opts.evalInterval)==0
        fprintf('Iteration #%d/%d  Time:%4.1f  Eval=%4.8f\n',...
            iter, opts.iters, toc, full(evals(iter)));
        if plotOpts.plot
            delete(curLine);
            hold on
            iterVals = opts.evalInterval:opts.evalInterval:iter;
            curLine = plot(iterVals,evals(iterVals),...
                'Color',plotOpts.color,'LineWidth',2);
            legend(plotOpts.legendNames,'Location','Best')
            hold off
            drawnow;
        end
    end
    %figure(2), model.plotpsd(0,30); drawnow;
end

end