function evals = rprop(x,y,model,opts,plotOpts)
% RPROP   Resilient backpropagation function for gradient ascent
%
% Inputs
%    x - function inputs
%    y - function outputs
%    model - model of the function between x and y; must contain methods
%            'gradient', 'evaluate', 'getParams', 'setParams', 
%            and 'getBounds'
%    opts - options, including 'iters', 'evalInterval', and any model opts
%    plotOpts - plot options, including 'plot', 'ax', 'color', and
%            'legendNames'
%
% Output
%    evals - function evaluations every 'evalInterval' iterations

evals = zeros(1,opts.iters);
curLine = [];
tic

p = model.getParams();
g_old = zeros(numel(p),1);
step = .01*ones(numel(p),1);

iter = 1;
convCntr = 0;

while (iter < opts.iters) && (convCntr < 10)
    % get parameters, gradients, and bounds
    p = model.getParams();
    g = model.gradient(x,y,opts);
    [lb,ub] = model.getBounds();
    
    %g(lb==-9.9) = g(lb==-9.9) - .1;
    
    % adjust step sizes
    gg = g.*g_old;
    step(gg>0) = 1.2*step(gg>0);
    step(gg<0) = 0.5*step(gg<0);
    
    % take step
    pNew = p + step.*sign(g);
    
    % force no step adjustments on next iteration if direction was changed
    g(gg<0 | pNew<lb | pNew>ub) = 0;
    step(pNew<lb | pNew>ub) = .5*step(pNew<lb | pNew>ub);
    
    % constrain parameters
    pNew(pNew<lb) = p(pNew<lb);
    pNew(pNew>ub) = p(pNew>ub);
    pNew(pNew<lb) = lb(pNew<lb);
    pNew(pNew>ub) = ub(pNew>ub);
    
    % assign gradient as old
    g_old = g;
    
    % Update parameters
    model.setParams(pNew);
    pNew = model.getParams;
    
    % Show performance every so often
    if mod(iter,opts.evalInterval)==0
        % Check performance
        evals(iter) = model.evaluate(x,y);
    
        fprintf('Iteration #%d/%d  Time:%4.1f  Eval=%4.8f  delta=%4.6f\n',...
            iter, opts.iters, toc, full(evals(iter)), sum(abs(p - pNew))/numel(p));
        if plotOpts.plot
            delete(curLine);
            hold on
            iterVals = opts.evalInterval:opts.evalInterval:iter;
            curLine = plot(plotOpts.ax,iterVals,evals(iterVals),...
                'Color',plotOpts.color,'LineWidth',2);
            xlabel(plotOpts.ax,'Iteration');
            title(plotOpts.ax,'Log Evidence','FontSize',14);
            if isfield(plotOpts,'legendNames')
                legend(plotOpts.ax,plotOpts.legendNames,'Location','Best')
            end
            hold off
            drawnow;
        end
    end
    
    % Update convergence checks
    if sum(abs(p - pNew))/numel(p) < 1e-3, convCntr = convCntr + 1;
    else convCntr = 0; end
    iter = iter + 1;
end

end