%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% COSYNE-2007 Abstract Template
%%% Version 1.0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[12pt]{article}
\usepackage{times}

\usepackage{bm}
\usepackage{graphicx}
\usepackage{subcaption}

\oddsidemargin -0.25in		% margin, in addition to 1" standard
\textwidth 7in		% 8.5" - 2*(1+\oddsidemargin)

\topmargin -0.75in		% in addition to 1.5" standard margin
\textheight 9.5in 		% 11 - ( 1.5 + \topmargin + <bottom-margin> )

\columnsep 0.25in

\parindent 0pt
\parskip 6pt

\flushbottom \sloppy
\pagestyle{empty}		% No page numbers

\begin{document}

%%%----------------------------------------------------------------- 
{\LARGE\bf  
Sparse latent factor analysis discovers important modes of network activity from multi-site LFP
data}

{\Large\bf
Neil Gallagher$^1$, Kyle Ulrich$^1$, Kafui Dzirasa$^1$, and Jeff Beck$^1$ 
}

{
$^1$Duke University
}

%%%----------------------------------------------------------------- 

The relationship between complex patterns of neural activity distributed across cortical and subcortical regions and behavior is obscure at best.  Much of the challenge stems from the complexity and high dimensionality of brain activity and its correlates.  As such, a model to determine the functional significance of brain activity (or its correlates) must take into account the distributed nature of neural computation, both between and within brain regions, as well as the importance of oscillations and timing in neural communication.  Recent developments in expressive kernels for Gaussian processes have demonstrated that electrophysiological data may be represented as a generative process with a cross-spectral mixture (CSM) kernel, capturing relevant information about network activity from multiple recording channels. Here, we add a latent factor model to the CSM kernel, termed the CSM-FA kernel, to perform behavior classification in mice during a tail suspension test. Using the CSM-FA kernel, we significantly reduce the dimensionality of the original data while retaining behaviorally relevant information about the state of the network of brain regions being recorded. Indeed, the outputs of this model improve performance on a behavior classification task despite the massive dimensionality reduction performed by the CSM-FA model. Finally, because the CSM-FA model is fully Bayesian it can be used to generate missing data.  For example, we can treat continuous time behavioral output as 'just another neural recording' and then fit the model to simultaneous observations of behavioral and neural data.  This encourages the model to discover latent factors that are sensitive to changes in behavioral state.  We can then use this model to predict behavioral outputs on a test set for which behavioral data has been censored, potentially improving decoding performance.  

%%%----------------------------------------------------------------- 

\textbf{Additional Details}

The electrophysiological data used were local field potentials (LFPs) recorded from multiple brain regions in mice during a tail suspension test. Using common preprocessing techniques, the data was then filtered to reduce extraneous spectral content and partitioned into time windows for analysis. Within each window, the data is modeled as a linear combination of latent functions, \(\bm{f}^w_l\), with additive white noise, \(\bm{\epsilon}\). 
\begin{equation}
\bm{y}^w_n = \sum_{l=1}^Ls_{wl}\bm{f}^l_w(t^w_n) + \bm{\epsilon}
\end{equation}
We will refer to the weights on each latent function, \(s_{wl}\), as the factor scores. Each latent function is drawn from its own gaussian process with a CSM kernel[1].
\begin{equation}
\bm{f}^l_w(t)\sim\mathcal{GP}(\bm{0},\bm{K}_{CSM}(t,t';\bm{\theta}_l))
\end{equation}
Although the latent function drawn for each factor is different between windows, the underlying spectral properties, represented by the parameters of the CSM kernel, \(\bm{\theta}_l\), are shared for a single factor between all windows.

Among the benefits of using the CSM-FA model is the ability to represent the information contained in high dimensional neural data as a handful of factor scores. Converting raw LFP data to factor scores via the CSM-FA model is a form of non-linear, kernel dimensionality reduction, and can reduce the dimensionality of the data by several orders of magnitude.  This non-linearity is key.  Unlike traditional linear dimensionality reduction methods, the factors discovered using CSM-FA are not isolated to specific frequency bands but instead can discover complex relationship between frequency bands in different brain regions.  Reassuringly, however, we have observed that discovered features often discover links between the traditional frequency bands described in the literature (i.e. alpha, theta, etc.).

We applied the CSM-FA model to discover latent features of neural activity during a tail suspension test.  Here the relevant behavioral output is the relative immobility of a suspended mouse, which is taken to be indicative of a depression-like state[2]. The neural data consisted of LFP's recorded from 15 different cortical and sub cortical regions.  We tested the behavioral relevance of discovered latent factors by comparing the performance of a binary SVM with a linear kernel applied to both factor scores and the spectra of the original LFP data.  Binary labels were chosen based on whether the total amount of movement within a time window was above or below a preset threshold.  We found that performance on this task, as measured by the F1 statistic, is significantly better when using factors scores, as opposed to the original LFP data.  This is despite the fact that the factor scores are a much lower dimensional representation of the data set and are necessarily less informative.

\begin{figure}
\centering
	\begin{subfigure}{0.45\textwidth}
	\includegraphics[width=\linewidth]{scoreVsIm}
	\end{subfigure}
	\begin{subfigure}{0.45\textwidth}
	\includegraphics[width=\linewidth]{factor2}
	\end{subfigure}
	\caption{A factor score that visibly varies with behavior}
\end{figure}

Additionally, we were able to identify trends in factors that appear strongly related to the behavior of interest. Figure 1 displays a factor that appears to 'turn off' during periods of immobility. We see that this particular factor is related to strong components in the theta (2-8Hz) and beta (13-30Hz) bands in all three regions shown, and that theta activity in the basolateral amygdala (BLA) shows coherence with theta activity in the core and shell of Nucleus Accumbens (AcbCore, AcbShell) along with a phase shift of about 90\(^o\). Based on this, we can conclude that this pattern of communication between these areas is down-regulated during immobility and identify it as a potential candidate for further investigation of depression-like symptoms. Regardless, this demonstrates that the CSM-FA model is a versatile tool for finding the most significant dimensions of network spectral activity in multi-channel recordings.
\vfil 

%%%----------------------------------------------------------------- 
{\bf References} \\
$[1]$ K Ulrich, D Carlson, K Dzirasa, L Carin. 2015. GP Kernels for Cross-Spectrum Analysis. Neural and Information Processing Systems. 

$[2]$ CL Bergner, AN Smolinsky, PC Hart, BD Dufour, RJ Egan, JL LaPorte, AV Kalueff. 2010. Mouse Models for Studying Depression-Like States and Antidepressant Drugs.  In: Mouse Models for Drug Discovery, Methods in Molecular Biology 602: 267-282.

\end{document}