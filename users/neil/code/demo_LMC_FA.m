% demo_LMC_FA
% demonstration of CSM factor modeling of LFP data

clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

% Path to LFP data & main repository folder
addpath(genpath('/Users/Neil/Brain States/data/'))
addpath(genpath('/Users/Neil/Brain States/CSM/code'))

%% Set Parameters
% data/pre-proccessing parameters
dataOpts.numSeconds = 1; % window size
dataOpts.highFreq = 120; % band pass
dataOpts.lowFreq = 1.5; % ^ (keep >1.5)
dataOpts.sampFact = 5; % subsampling (keep <~5)
dataOpts.mouseName = 'MouseCKX1_112114';

C = 2; % n brain regions

% model parameters
Q = 2;
R = 2*ones(Q,1);
gamma = 100;
L = 2;

% fit/training parameeters
opts.iters = 10;
opts.evalInterval = 2;
plotOpts.plot = true; % plot marginal likelihood during training
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};

moveThreshold = 0.15; % threshold of time required to be considered moving


%% Load and preprocess Data
[X,regionNames,pointLabels] = loadTailSuspensionData(dataOpts);

% assign escape behavior label to each window as median movement label for
% all points in window
pointLabels = squeeze(pointLabels);
windowLabels = (mean(pointLabels)>moveThreshold)';

N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

% take fourier transform
Ns = ceil(N/2);
Xfft = 1/sqrt(N)*fft(X);
Xfft = 2*(Xfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

% select subset (or all) of data for training
xt = X;
xtfft = Xfft(:,1:C,:);
W = size(X,3);


%% Create model
% create Q Gaussian basis functions
%k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);
LMCmodels = cell(L,1);
for l = 1:L
    %kernels = GP.kernels.gaborKernels(Q,1.5,40);
    %means = num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq,Q));
    means = num2cell(dataOpts.lowFreq+(dataOpts.highFreq-dataOpts.lowFreq)*...
        rand(1,Q));
    k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,...
        dataOpts.highFreq],[eps,1e2]),means,num2cell(rand(1,Q)), 'un', 0);
    kernels = GP.kernels.kernels(Q, k);
    
    % generate kernels (weights and phase of basis functions) for each reagion
    B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
    %B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
    coregs = GP.coregs.mats(C,Q,R,B);
    
    % form GP with CSM kernel
    LMCmodels{l} = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);
end
model = GP.LMC_FA(LMCmodels,W,C,Q);


%% Fit model

evals = algorithms.rprop(s,xtfft,model,opts,plotOpts);


%% Visualize Data
figure(2), clf, plot(tau,xt(:,:,30));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:C))


% %% Display all kernel factors
% for l = 1:L
%     figure, model.LMCkernels{l}.plotpsd(0,100,regionNames)
% %     ax = subplot(7,7,(C-3)*C+2);
% %     text(0.5,0.5,['Factor ' num2str(l)],'FontSize',16)
% %     set( ax, 'visible', 'off')
% %     drawnow
%     fprintf('Factor %d\n',l)
%     for q = 1:Q
%        kern = model.LMCkernels{l}.kernels.k{q};
%        fprintf('mu: %.2f - var: %.2f\n', kern.mu, kern.var) 
%     end
% end

