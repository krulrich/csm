% get performance w/ error bars
N = 20;

f1Csm = zeros(N,1);
f1Lfp = zeros(N,1);

for i = 1:N
    
    [iTrain,~,iTest] = dividerand(size(X,3),.7,0,.3);
    xTrain = Xfft(:,1:C,iTrain);
    xTest = Xfft(:,1:C,iTest);
    lTrain = windowLabels(iTrain);
    lTest = windowLabels(iTest);
    
    
    %% get inputs for training
    
    % get CSM scores for test data
    modelRefit = GP.LMC_FA(model.LMCkernels,length(lTest),C,Q);
    modelRefit.updateKernels = false;
    evalsNoB2 = algorithms.rprop(s,xTest,modelRefit,opts,plotOpts);
    
    % get fft magnitudes
    magniTrain = abs(reshape(xTrain,[],length(lTrain)));
    magniTest = abs(reshape(xTest,[],length(lTest)));
    
    %% train & test
    % train & test SVM model using LFP inputs
    csmModel = fitcsvm(model.scores(:,iTrain)',lTrain);
    labTestHatCsm = predict(csmModel, modelRefit.scores');
    CmatCsm = confusionmat(lTest,labTestHatCsm);
    
    % train & test SVM model using LFP inputs
    lfpModel = fitcsvm(magniTrain',lTrain);
    labTestHatLfp = predict(lfpModel, magniTest');
    CmatLfp = confusionmat(lTest,labTestHatLfp);
    
    
    %% calculate f1 for CSM and LFP classification
    precisionCsm = CmatCsm(1,1)/sum(CmatCsm(:,1));
    recallCsm = CmatCsm(1,1)/sum(CmatCsm(1,:));
    f1Csm(i) = 2*precisionCsm*recallCsm/(precisionCsm + recallCsm);
    
    precisionLfp = CmatLfp(1,1)/sum(CmatLfp(:,1));
    recallLfp = CmatLfp(1,1)/sum(CmatLfp(1,:));
    f1Lfp(i) = 2*precisionLfp*recallLfp/(precisionLfp + recallLfp);
end

%% plot

set(gca,'fontsize',14);

boxplot([f1Lfp,f1Csm])
ylabel('F1')



