function [x,regionNames,movementLabels] = loadTailSuspensionData(opts)
% loadTailSuspensionData
% opts: options
% x: preprocessed LFP data
% regionNames: strings naming regions of corresponding indicies in data
% accelData: acceleration data from tail suspension task
% labels: labels indicating whether escape behavior is occuring (1-no 2-yes)
    if ~isfield(opts, 'behaviorIn')
       opts.behaviorIn = 0; 
    end

    ACCEL_SAMPLING_RATE = 2000;
    %% Load data
    load([opts.mouseName '_HCOFTS_LFP'])
    load([opts.mouseName '_HCOFTS_TIME'])
    
    
    %% Extract indices of channels from the same regions
    % this is a roundabout method, but remains consistent with different
    % methods of labelling channels within the stress datasets (for now!)
    
    vars = who;
    isChannel = ~cellfun(@isempty,regexp(vars,'_\d\d'));
    acn = vars(isChannel);
    % get n samples to use at bottom!!!
    
    % add indicator to start of string
    acn2 = cellfun(@(x)['*' x],acn,'un',0);
    % remove '*L_' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*L_',''),acn2,'un',0); 
    % remove '*R_' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*R_',''),acn2,'un',0);
    % remove '*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); 
    
    % remove the last 3 characters
    acn2 = cellfun(@(x)x(1:numel(x)-3),acn2,'un',0);
    
    % add indicator to end of string
    acn2 = cellfun(@(x)[x '*'],acn2,'un',0);
    % remove '_L*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'_L*',''),acn2,'un',0); 
    % remove '_R*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'_R*',''),acn2,'un',0); 
    % remove '*' from start of strings
    acn2 = cellfun(@(x)strrep(x,'*',''),acn2,'un',0); 
    
    % automatically sorts names alphabetically
    [regionNames,~,acn_inds] = unique(acn2); 
    R = numel(regionNames); % number of channels
    
    
    %% Organize LFP data into matrix
    % parameters for bandpass filter
    sampRateOrig = 1000; highFreq = 400; lowFreq = 1;
    
    x = zeros(eval(['numel(' acn{1} ')']),R);
    for r = 1:R
        % average all channels from the same region
        acn_inds_r = find(acn_inds==r)';
        for ac_ind = acn_inds_r;
            chan = double(eval(acn{ac_ind}))';
            
            % bandpass filter
            highpass = highFreq/sampRateOrig*2;
            lowpass = lowFreq/sampRateOrig*2;
            [butterb,buttera] = butter(2,[lowpass,highpass]);
            chan = filtfilt(butterb,buttera,chan); % bandpass filter
            
            x(:,r) = x(:,r) + chan;
        end
        x(:,r) = x(:,r) / numel(find(acn_inds==r));
    end
    
    
    %% Add behavioral data to inputs, if necessary
    if opts.behaviorIn
        accelData = downsample(double(TailSuspension),2);
        
        % bandpass filter data
        highpass = highFreq/sampRateOrig*2;
        lowpass = lowFreq/sampRateOrig*2;
        [butterb,buttera] = butter(2,[lowpass,highpass]);
        accelData = filtfilt(butterb,buttera,accelData); % bandpass filter
        
        % add behavioral data as first column of x
        x = cat(2, accelData', x);
        
%         regionNames = ['Movement'; regionNames{:}]; fix this
    end
    
    %% Use only tail suspention task portion of recordings
    % Select relevant portion of LFP data
    xIntervals = ceil(INT_TIME*sampRateOrig);
    xStart = xIntervals(3);
    xEnd = xIntervals(3)+xIntervals(4)-1;
    x = x(xStart:xEnd,:);
    
    
    %% Filter and subsample LFP data
    numSeconds = opts.numSeconds;
    highFreq = opts.highFreq;
    lowFreq = opts.lowFreq;
    sampFact = opts.sampFact;

    % only process a fixed amount of time
    sampRate = sampRateOrig/sampFact;
    N = sampRate*numSeconds;

    % notch filter 60Hz signal
    filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
    filtbw=filtfreq/60; % 1Hz Bandwidth
    [filtb,filta]=iirnotch(filtfreq,filtbw);
    x = filtfilt(filtb,filta,x);

    % bandpass filter
    highpass = highFreq/sampRateOrig*2;
    lowpass = lowFreq/sampRateOrig*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    x = filtfilt(butterb,buttera,x); % bandpass filter

    % subsample by sample factor
    x = x(1:sampFact:end,:);

    % normalize data
    x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x)),std(x));

    % separate into windows
    W = floor(size(x,1)/(N));
    if opts.behaviorIn
        x = separateWindows(x,W,N,R+1);
    else
        x = separateWindows(x,W,N,R);
    end
    
    %% Calculate movement labels (heuristic)
    % choose relevant data
    TailSuspension = TailSuspension(2*xStart:2*xEnd);
    
    % run tail suspension data throught bandpass(butterworth) filter
    [b, a] = butter(4, [4/(ACCEL_SAMPLING_RATE/2) 15/(ACCEL_SAMPLING_RATE/2)]); 
    filteredData = filtfilt(b, a, double(TailSuspension));
    % get analytic component of signal
    analyticData = util.analyticSignal(filteredData);

    sampRatio = ACCEL_SAMPLING_RATE/sampRateOrig;
    % moving average smoothing over amplitude of analytic signal
    smoothData = smooth(analyticData.amplitude, 201);
    smoothData = downsample(smoothData, sampFact*sampRatio);
    
    % set labels
    movementLabels = (smoothData>50);  
    movementLabels = separateWindows(movementLabels,W,N,1);
    
end


function x = separateWindows(x,nWindows,spw,nRegions)
% separateWindows
% x: data; input is a matrix w/ nRegions columns; truncated and reshaped to
%    become a 3D array (spwxnRegionsxnWindows)
% nWindows: number of windows
% spw: samples per window
% nRegions: number of regions (for LFP data)

    x = x(1:nWindows*spw,:);
    x = permute(reshape(x',[nRegions,spw,nWindows]),[2,1,3]);
end