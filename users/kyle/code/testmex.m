x=randn(7,7,5000) + 1i*randn(7,7,5000);
mex matrixInverse.c -lmwlapack -largeArrayDims % CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
% xi=inv(x);
% xi2=x;
% xi2(1)
% return
%%
tic
x2=x;
for n=1:size(x2,3);
    x2(:,:,n)=inv(x(:,:,n));
end
toc

%%
x3=x;
tic

matrixInverse(x3);
toc
%%
if numel(find(~(x2==x3)))==0
    fprintf('All the same!\n');
end

% xi2(1)