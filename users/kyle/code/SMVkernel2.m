classdef SMVkernel2 < handle
    properties
        % model parameters
        R % length of vector, (# observations)
        Q
        kernels % one kernel for each observation
        bases
        gamma
    end
    
    methods
        function self = SMVkernel2(R,Q,bases,kernels,gamma)
            self.R = R;
            self.Q = Q;
            
            if ~exist('bases','var')
                self.bases = SMbases(Q);
                self.kernels = SMkernels(R,Q);
                self.gamma = 1;
            else
                self.kernels = kernels;
                self.bases = bases;
                self.gamma = gamma;
            end
        end
        
        function res = K(self,tau)
            N = numel(tau);
            res = zeros(self.R*N);
            for q = 1:self.Q
                B = self.kernels.B(q);
                k = toeplitz(self.bases.basis{q}.covFun(tau));
                res = res + (kron(B,k));
            end
            res = res + 1/self.gamma*eye(numel(tau)*self.R);
        end
        
        function res = UKU(self,s,N)
            %N = numel(s);
            res = 1/self.gamma*kron(eye(self.R),eye(numel(s))); % white noise component
            
            SD = self.bases.specDens(s);
            
            for q = 1:self.Q
                B = self.kernels.B(q);
                res = res + N*kron(B,diag(SD(:,q)));
            end
        end
        
        
        function res = UKU2(self,s,N)
            %N = numel(s);
            res = 1/self.gamma*kron(eye(self.R),fliplr(eye(numel(s)))); % white noise component
            
            SD = self.bases.specDens(s);
            
            for q = 1:self.Q
                B = self.kernels.B(q);
                res = res + N*kron(B,fliplr(diag(SD(:,q))));
            end
        end
        
        function res = logevidence(self,tau,y,K,Kinv)
            logdetK = 2*sum(log(diag(chol(K))));
            N = numel(tau); W = size(y,2);
            
            res = -1/2*N*self.R*W*log(2*pi) - 1/2*W*logdetK;
            res = res - 1/2*sum(sum(y.*(Kinv*y)));
            %res = trace(-1/2*y'*Kinv*y - 1/2*logdetK - numel(tau)/2*log(2*pi));
        end
        
        function res = logevidenceDFT(self,s,x,S,Sinv)
            N = numel(s); W = size(x,2);           
            logdetS = 2*sum(log(diag(chol(S))));
            
            res = -1/2*N*self.R*W*log(2*pi) - 1/2*W*logdetS;
            res = res - 1/2*sum(sum(x.*(Sinv*x)));
        end
        
        function res = specDens(self,s)
            res = zeros(numel(s),self.R,self.R);
            
            W = self.kernels.weights;
            %weights = bsxfun(@rdivide,weights,sum(weights,2));
            SD = self.bases.specDens(s);
            
            for r1 = 1:self.R
                for r2 = 1:self.R
                    res(:,r1,r2) = sum(bsxfun(@times,(W(r1,:).*W(r2,:)),SD),2);
                end
            end
            
            % add white noise to spectral density
            res = res + 1/self.gamma;
        end
        
        function res = fourierWeights(self,tau)
            delta = tau(2) - tau(1); N = numel(tau);
            s = 1/(N*delta):1/(N*delta):1/(2*delta); s = [s,s(end:-1:1)];
            res = self.specDens(s);
        end
        
        function plotK(self,tau,names)
            if ~exist('names','var')
                labs = 1:self.R;
            else
                labs = names;
            end
            
            K = self.K(tau);
            N = numel(tau);
            imagesc(K), colorbar
            title('Covariance Matrix','FontSize',16)
            inds = round((N/2):N:(N*self.R));
            set(gca,'XTick',inds,'XTickLabel',labs)
            set(gca,'YTick',inds,'YTickLabel',labs)
        end
        
        function res = update(self,tau,y,iters,showkernel)
            if ~exist('iters','var'), iters = 100; end
            g.var = zeros(self.Q,1);    g.mu = zeros(self.Q,1);
            g.w = zeros(self.Q,self.R); g.psi = zeros(self.Q,self.R);
            g.gamma = 0;
            g_old = g;
            step.var = .01*ones(self.Q,1);    step.mu = .01*ones(self.Q,1);
            step.w = .01*ones(self.Q,self.R); step.psi = .01*ones(self.Q,self.R);
            step.gamma = .1;
            
            W = size(y,2);
            
            res = zeros(iters,1);
            
            for iter = 1:iters
                basesnew = self.bases; kernelsnew = self.kernels; gammanew = self.gamma;
                
                K = self.K(tau);
                K_inv = inv(K);
                alpha = K_inv*y;
                A = alpha*alpha' - W*K_inv; At = A'; At = At(:)';
                res(iter) = self.logevidence(tau,y,K,K_inv);
                
                fprintf('iteration %d, log p(y|X) = %d, gamma = %d\n',iter,res(iter),self.gamma);
                
                if showkernel
                    figure(3), clf;
                    self.plotlogpsd(0,20,-6,0); 
                    %self.plotK(tau);
                end
                
                % wrt gamma
                dK = -1/self.gamma^2*eye(numel(tau)*self.R);
                g.gamma = 1/2*sum(sum(A.*dK'));
                [gammanew, g_old.gamma, step.gamma] = ...
                    self.rprop(gammanew,g.gamma,g_old.gamma, ...
                    step.gamma,1e-4,1e4,1e-4,1e2);
                
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    %[Ub,Sb] = eigs(B,rank(B));
                    
                    % wrt variance
                    dk = toeplitz(self.bases.derivCovFunVar(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.var(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    g.var(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.var, g_old.var(q), step.var(q)] = ...
                        self.rprop(basesnew.basis{q}.var,g.var(q),g_old.var(q), ...
                        step.var(q),1e-2,1e2,1e-2,.5);
                    
                    %wrt mean
                    dk = toeplitz(self.bases.derivCovFunMean(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.mu(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    g.mu(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.mu, g_old.mu(q), step.mu(q)] = ...
                        self.rprop(basesnew.basis{q}.mu,g.mu(q),g_old.mu(q), ...
                        step.mu(q),0,1/4/(tau(2)-tau(1)),1e-4,.5);
                    
                    k = toeplitz(self.bases.covFun(tau,q));
                    %[Uk,Sk] = eigs(k,rank(k));
                    for r = 1:self.R
                        %wrt weight
                        dB = self.kernels.dBdw(q,r);
                        
                        %[Udb,Sdb] = eigs(dB,1);
                        %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                        %g.w(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                        
                        dK = real(kron(dB,k));
                        g.w(q,r) = 1/2*At*dK(:);
                        [kernelsnew.k{r}.w(q), g_old.w(q,r), step.w(q,r)] = ...
                            self.rprop(kernelsnew.k{r}.w(q),g.w(q,r),g_old.w(q,r), ...
                            step.w(q,r),eps,1e2,eps,1e2);
                        
                        %wrt phase
                        if r > 1
                            dB = self.kernels.dBdpsi(q,r);
                            
                            %[Udb,Sdb] = eigs(dB,1);
                            %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                            %g.psi(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                            
                            dK = real(kron(dB,k));
                            g.psi(q,r) = 1/2*At*dK(:);
                            [kernelsnew.k{r}.psi(q), g_old.psi(q,r), step.psi(q,r)] = ...
                                self.rprop(kernelsnew.k{r}.psi(q),g.psi(q,r),g_old.psi(q,r), ...
                                step.psi(q,r),-inf,inf,1e-4,1e0);
                        end
                    end
                end
                self.kernels = kernelsnew;
                self.bases = basesnew;
                self.gamma = gammanew;
            end
        end
        
        function res = updateDFT(self,tau,y,iters,showkernel)
            if ~exist('iters','var'), iters = 100; end
            g.var = zeros(self.Q,1);    g.mu = zeros(self.Q,1);
            g.w = zeros(self.Q,self.R); g.psi = zeros(self.Q,self.R);
            g.gamma = 0;
            g_old = g;
            step.var = .01*ones(self.Q,1);    step.mu = .01*ones(self.Q,1);
            step.w = .01*ones(self.Q,self.R); step.psi = .01*ones(self.Q,self.R);
            step.gamma = .1;
            
            N = numel(tau); W = size(y,2); Ns = ceil(N/2);
            
            U = 1/sqrt(N)*fft(eye(N)); G = kron(eye(self.R),U);
            Ut = U'; Gtranshalf = kron(eye(self.R),Ut(2:Ns+1,:));
            Uy = Gtranshalf*y;   
            x = [real(Uy); imag(Uy)];
            delta = tau(2) - tau(1);
            s = 1/(N*delta):1/(N*delta):1/(2*delta); %s=[s,0,1000*ones(1,numel(s)-1)];%,fliplr(s)];% s = [0,s,s(end-1:-1:1)]; %
            
            res = zeros(iters,1);
            
            for iter = 1:iters
                basesnew = self.bases; kernelsnew = self.kernels; gammanew = self.gamma;

                S = self.complexNormalCov(1/2*self.UKU(s,N),1/2*self.UKU2(s,N));
                Sinv = pinv(S);
                
                %alpha2 = inv(UKU)*Uy;
                %A = self.complexNormalCov(alpha2*alpha2') - W*Sinv;
                
                alpha = Sinv*x;
                A = alpha*alpha' - W*Sinv; 
                
                At = A'; At = At(:)';
                %res(iter) = self.logevidenceDFT(s,x,S,Sinv);
                
                fprintf('iteration %d, log p(y|X) = %d, gamma = %d\n',iter,res(iter),self.gamma);
                
                if showkernel
                    figure(3), clf;
                    self.plotlogpsd(0,20,-6,0); 
                    %self.plotK(tau);
                end
                
                % wrt gamma
                %dS = -1/self.gamma^2*blkdiag(kron(eye(self.R),eye(Ns)),zeros(self.R*Ns));
                %g.gamma = 1/2*At*dS(:);
                g.gamma = self.getGrad(eye(self.R),-1/self.gamma^2*eye(Ns),At);
                [gammanew, g_old.gamma, step.gamma] = ...
                    self.rprop(gammanew,g.gamma,g_old.gamma, ...
                    step.gamma,1e-4,1e4,1e-4,1e2);
                
                for q = 1:self.Q
                    B = self.kernels.B(q);

                    %wrt variance
                    dw = N*diag(self.bases.derivSpecDensVar(s,q));
                    g.var(q) = self.getGrad(B,dw,At);
                    [basesnew.basis{q}.var, g_old.var(q), step.var(q)] = ...
                        self.rprop(basesnew.basis{q}.var,g.var(q),g_old.var(q), ...
                        step.var(q),1e-2,1e2,1e-2,.5);
                    
                    %wrt mean
                    dw = N*diag(self.bases.derivSpecDensMean(s,q));
                    g.mu(q) = self.getGrad(B,dw,At);
                    [basesnew.basis{q}.mu, g_old.mu(q), step.mu(q)] = ...
                        self.rprop(basesnew.basis{q}.mu,g.mu(q),g_old.mu(q), ...
                        step.mu(q),0,1/4/(tau(2)-tau(1)),1e-4,.5);
                    
                    w = N*diag(self.bases.specDens(s,q));
                    for r = 1:self.R
                        %wrt weight
                        dB = self.kernels.dBdw(q,r);
                        g.w(q,r) = self.getGrad(dB,w,At);
                        [kernelsnew.k{r}.w(q), g_old.w(q,r), step.w(q,r)] = ...
                            self.rprop(kernelsnew.k{r}.w(q),g.w(q,r),g_old.w(q,r), ...
                            step.w(q,r),eps,1e2,eps,1e2);
                        
                        %wrt phase
                        if r > 1
                            dB = self.kernels.dBdpsi(q,r);     
                            g.psi(q,r) = self.getGrad(dB,w,At);
                            [kernelsnew.k{r}.psi(q), g_old.psi(q,r), step.psi(q,r)] = ...
                                self.rprop(kernelsnew.k{r}.psi(q),g.psi(q,r),g_old.psi(q,r), ...
                                step.psi(q,r),-inf,inf,1e-4,1e0);
                        end
                    end
                end
                self.kernels = kernelsnew;
                self.bases = basesnew;
                self.gamma = gammanew;
            end
        end
               
        function res = getGrad(self,B,w,At)
            dS = self.complexNormalCov(1/2*kron(B,w),1/2*kron(B,fliplr(w)));
            res = 1/2*At*dS(:);
        end
        
        function res = plotpsd(self,minFreq,maxFreq)
            ha = util.tight_subplot(self.R,self.R,[.03 .03],[.1 .05],[.2 .2]);
            s= linspace(minFreq,maxFreq,1e3)';
            maxphi = 0;
            
            phi = zeros(1e3,self.R,self.R);
            means = self.bases.means;
            
            for r1 = 1:self.R
                weights_r1 = self.kernels.k{r1}.w;
                weights_r1 = weights_r1/sum(weights_r1);
                for r2 = 1:self.R
                    weights_r2 = self.kernels.k{r2}.w;
                    weights_r2 = weights_r2/sum(weights_r2);
                    phi(:,r1,r2) = sum(bsxfun(@times,weights_r1.*weights_r2, ...
                        self.bases.spectraldensities(s)),1);
                    maxphi = max(maxphi,max(phi(:,r1,r2)));
                end
            end
            
            for r1 = 1:self.R
                shifts_r1 = self.kernels.k{r1}.psi;
                weights_r1 = self.kernels.k{r1}.w;
                for r2 = 1:self.R
                    shifts_r2 = self.kernels.k{r2}.psi;
                    weights_r2 = self.kernels.k{r2}.w;
                    weights = weights_r1.*weights_r2/sum(weights_r1)/sum(weights_r2);
                    inds = (weights > 2e-2);
                    
                    [haxes,hline1,hline2] = ...
                        plotyy(ha(r1+self.R*(r2-1)),...
                        s,phi(:,r1,r2), means(inds),...
                        wrapToPi(shifts_r2(inds) - shifts_r1(inds)));
                    set(hline1,'LineWidth',2);
                    set(hline2,'LineStyle','*','LineWidth',5);
                    ylim(haxes(2),[-pi,pi])
                    ylim(haxes(1),[0,maxphi])
                    xlim(haxes(1),[minFreq,maxFreq])
                    xlim(haxes(2),[minFreq,maxFreq])
                    
                    if r1 == self.R
                        set(haxes(2),'YTick',linspace(-pi,pi,5))
                        ylabel(haxes(2),'phase','FontSize',14)
                    else set(haxes(2),'YTick',[]);
                    end
                    
                    if r1 == 1
                        set(haxes(1),'YTick',linspace(0,maxphi,5))
                        ylabel(haxes(1),'PSD','FontSize',14)
                    else set(haxes(1),'YTick',[]);
                    end
                    
                    if r2 ~= self.R
                        set(haxes(1),'XTick',[]); set(haxes(2),'XTick',[]);
                    else
                        xlabel(haxes(1),'frequency','FontSize',14)
                        xlabel(haxes(2),'frequency','FontSize',14)
                    end
                end
            end
            
            for ii = 1:self.R^2
                %axes(ha(ii)); axis([minFreq,maxFreq,0,maxphi+.02]);
            end
            %set(ha(1:(self.R*(self.R-1))),'XTickLabel','');
            %set(ha(1:self.R^2),'FontSize',14)
            %set(ha,'YTickLabel','')
            
            
            res = 0;
        end
        
        
        
        function plotlogpsd(self,minFreq,maxFreq,minlogphi,maxlogphi)
            ha = util.tight_subplot(self.R,self.R,[.03 .03],[.1 .05],[.2 .2]);
            s= linspace(minFreq,maxFreq,1e3)';
            
            phi = zeros(1e3,self.R,self.R);
            means = self.bases.mean;
            weights = self.kernels.weights;
            weights = bsxfun(@rdivide,weights,sum(weights,2));
            shifts = self.kernels.shifts;
            
            for r1 = 1:self.R
                for r2 = 1:self.R
                    phi(:,r1,r2) = log(sum(bsxfun(@times,weights(r1,:).*weights(r2,:), ...
                        self.bases.specDens(s)),2));
                    phi(:,r1,r2) = max(phi(:,r1,r2),minlogphi);
                    
                    inds = (weights(r1,:).*weights(r2,:) > 1e-2);
                    
                    [haxes,hline1,hline2] = ...
                        plotyy(ha(r1+self.R*(r2-1)), ...
                        s,phi(:,r1,r2), means(inds),...
                        wrapToPi(shifts(r2,inds) - shifts(r1,inds)));
                    set(hline1,'LineWidth',2);
                    set(hline2,'LineStyle','*','LineWidth',5);
                    ylim(haxes(2),[-pi,pi])
                    ylim(haxes(1),[minlogphi,maxlogphi])
                    xlim(haxes(1),[minFreq,maxFreq])
                    xlim(haxes(2),[minFreq,maxFreq])
                    
                    if r1 == self.R
                        set(haxes(2),'YTick',linspace(-pi,pi,5))
                        ylabel(haxes(2),'phase','FontSize',14)
                    else set(haxes(2),'YTick',[]);
                    end
                    
                    if r1 == 1
                        set(haxes(1),'YTick',minlogphi:maxlogphi)
                        ylabel(haxes(1),'PSD','FontSize',14)
                    else set(haxes(1),'YTick',[]);
                    end
                    
                    if r2 ~= self.R
                        set(haxes(1),'XTick',[]); set(haxes(2),'XTick',[]);
                    else
                        xlabel(haxes(1),'frequency','FontSize',14)
                        xlabel(haxes(2),'frequency','FontSize',14)
                    end
                end
            end
            
        end
    end
    
    methods (Static)
        function [val,g,step] = rprop(val,g,g_old,step,minval,maxval,minstep,maxstep)
            %if ~exist('minval','var'), minval = -inf; maxval = inf; end
            
            if g*g_old > 0
                step = 1.2*step;
            elseif g*g_old < 0
                step = 0.5*step;
                g = 0;
            end
            
            step = max(min(step,maxstep),minstep);
            val = val + step*sign(g);
            if val > maxval
                val = maxval - (val - maxval);
                step = 0.5*step;
            elseif val < minval
                val = minval + (minval - val);
                step = 0.5*step;
            end
            %val = max(min(val,maxval),minval);
        end
        
        function res = complexNormalCov(Gamma,C)
            if ~exist('C','var')
                res = 1/2*[real(Gamma) imag(-Gamma); imag(Gamma) real(Gamma)];
            else
                res = 1/2*[real(Gamma+C) imag(-Gamma+C); ...
                    imag(Gamma+C) real(Gamma-C)];
            end
        end
    end
end
