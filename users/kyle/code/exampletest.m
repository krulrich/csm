%%

N = 200;
t = linspace(0,1,N);
tau = t(2) - t(1);

phi = .1;
Q = 2;
mu = [5,10];
sig = [.1,.1];
w = [.8,.2];

lags = phi/(2*pi)./mu %time

K11 = zeros(1,N); K12 = zeros(1,N); K122 = zeros(1,N);

for q = 1:Q
    K11 = K11 + w(q)*exp(-2*pi^2*t.^2*sig(q)^2).*cos(2*pi*t*mu(q));
    K12 = K12 + w(q)*exp(-2*pi^2*t.^2*sig(q)^2).*cos(2*pi*(t+phi)*mu(q));
    K122 = K122 + w(q)*exp(-2*pi^2*t.^2*sig(q)^2).*cos(2*pi*(-t+phi)*mu(q));
end
K12mat = toeplitz(K122,K12);
K11mat = toeplitz(K11);

K = [K11mat K12mat; K12mat' K11mat];
figure(1), clf, imagesc(K)

y = mvnrnd(zeros(400,1),K);
figure(2), clf, plot((1:N)*tau,reshape(y,N,2))

maxlag = 50;
figure(3), plot((-maxlag:maxlag)*tau,xcov(y(1:200),y(201:400),maxlag))
