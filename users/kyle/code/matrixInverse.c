#include <mex.h>
#include "matrix.h"
#include <lapack.h>
#include <omp.h>
/* This function inverts a square matrix A, of size N x N.
 * This function returns the inverse in *inv_A. */
void inverse(double* A, size_t N)
{
    size_t *IPIV;
    IPIV= (size_t *) mxMalloc((N+1)*sizeof(size_t));
    size_t LWORK = N*N;
    double *WORK;
    WORK =(double *) mxMalloc(LWORK*sizeof(double));
    size_t INFO;
    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);
    mxFree(IPIV);
    mxFree(WORK);
    
}

void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    double *A;
    size_t dims, N, mat, numMats;
    const size_t *dim_array;
    A=mxGetPr(prhs[0]);
    dims = (size_t)mxGetNumberOfDimensions(prhs[0]);
    dim_array = (const size_t*)mxGetDimensions(prhs[0]);
    N=dim_array[0];
    
    if(dims<3){
        inverse(A,N);
        
    }
    else {
        numMats=dim_array[2];
#define CHUNKSIZE 1
#pragma omp parallel private(mat)
        {
#pragma omp for schedule(dynamic, CHUNKSIZE)
            for (mat=0;mat<numMats;mat++)
            {
                inverse(A+N*N*mat,N);
            }
        }
    }
    
    
    /* inverse(A,N);*/
    
}
