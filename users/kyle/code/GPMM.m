classdef GPMM < handle
    % Gaussian Process Mixture Model
    properties
        % model parameters
        L % number of mixture components
        W % number of observation vectors
        kernels % store kernels
        Ez % latent cluster assignment probabilities
    end
    
    methods
        function self = GPMM(L,W,kernels,xfft)
            self.L = L;
            self.W = W;
            
            if numel(kernels) == L
                self.kernels = kernels;
            else
                for l = 1:L
                    self.kernels{l} = kernels.copy();
                end
            end
            
            if ~exist('x','var')
                self.Ez = rand(L,W);
                self.Ez = bsxfun(@rdivide,self.Ez,sum(self.Ez,1));
            else
                Nxfft=abs(xfft.^2);
                Nxfft=bsxfun(@rdivide,Nxfft,sum(Nxfft));
                upperBound=size(Nxfft,1)/5;
                [idx,~] = kmeans(reshape(Nxfft(1:upperBound,:,:), ...
                    [upperBound*size(Nxfft,2),W])',L);

                self.Ez = zeros(L,W);
                self.Ez(sub2ind(size(self.Ez),idx',1:W)) = 1;
            end
            
        end
        
        function res = update(self,tau,y,iters)           
            res = zeros(iters,1);
            for iter = 1:iters
                % M-step
                for l = 1:self.L
                    self.kernels{l}.update( tau, y, self.Ez(l,:) );
                end

                % E-step
                ll = self.logEvidenceAll( y, tau );
                self.Ez = exp(bsxfun(@minus,ll,max(ll,[],1)));
                self.Ez = bsxfun(@rdivide,self.Ez,sum(self.Ez,1));
                
                figure(3), imagesc(self.Ez), colorbar
                figure(4), self.kernels{1}.plotpsd(0,20,{1,2,3,4})
                figure(5), self.kernels{2}.plotpsd(0,20,{1,2,3,4})
                pause(.5)
                
                res(iter) = sum(sum(ll.*self.Ez));
            end
        end
        
        function res = logEvidenceAll(self,y,tau)
            res = zeros(self.L,self.W);
            for l = 1:self.L
                res(l,:) = self.kernels{l}.logEvidence(y,tau);
            end
        end
    end
end