%tracematmultkron

M = 6;
N = 200;
A = randn(M*N);
B = rand(M);
C = randn(N);

tic;
At = A'; At = At(:)';
BkronC = kron(B,C);
res1 = At*BkronC(:);
toc

tic;
res2 = 0;
Ar = reshape(A,M,N,M*N);
for m = 1:M
    for n = 1:N
        i = (m-1)*N + n;
        res2 = res2 + C(n,:)*Ar(:,:,i)'*B(m,:)';
        
    end
end
toc


res3 = 0;
for i = 1:M*N
    res3 = res3 + BkronC(i,:)*A(:,i);
end


res1
res2
res3