clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/climate'))
addpath('../../../code')

%% Set Parameters
highFreq = 5; %90
lowFreq = .1;
fileName = 'NA-1990-2002-Monthly.csv';

C = 4;

%% Read climate data
fid = fopen(fileName);
TData = textscan(fid, ...
    '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s', ...
    'delimiter',',');
fclose(fid);
data = [TData{:}];
years = unique(data(2:end,1));
locs = data(2:end,3:4);
data = str2double( data(2:end,5:20) );

numYears = numel(years);
sampRate = 12;
N = numYears*sampRate;
Nlocs = size(data,1)/N;
Nagents = size(data,2);

X = reshape(data,[N,Nlocs,Nagents]);

Xt = squeeze(X(:,1,5:8));
Xt = bsxfun(@minus,Xt,mean(Xt));
Xt = bsxfun(@rdivide,Xt,std(Xt));

% take fourier transform
Ns = ceil(N/2);
Xtfft = 1/sqrt(N)*fft(Xt);
Xtfft = 2*(Xtfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(numYears-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

figure, plot(tau,Xt)
%% Create model
Q = 10;
R = ones(Q,1);
gamma = 100;

% create Q basis functions
k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[lowFreq,highFreq],[eps,10]), ...
    num2cell(lowFreq + (highFreq-lowFreq)*rand(1,Q)), ...
    num2cell(.1*rand(1,Q)), 'un', 0);
kernels = GP.kernels.kernels(Q, k);

% generate Q rank R coregionalization matrices
B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
%B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
coregs = GP.coregs.mats(C,Q,R,B);

% form GP with CSM kernel
model = GP.LMC(C,Q,R,coregs,kernels,gamma);

%% Fit model
opts.iters = 200;
opts.evalInterval = 1;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
figure(2); clf;

%evals = algorithms.rprop(s,Xtfft,model,opts,plotOpts);
evals = algorithms.rprop(tau,Xt,model,opts,plotOpts);

%% Results

figure, model.plotCsd('minFreq',0,'maxFreq',13)