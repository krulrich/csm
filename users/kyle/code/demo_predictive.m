clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath('/home/kru2/Documents/data/neuroscience/lfp/')
datasets = {'sleepHour1234Animal5'};%,'sleepHour5678Animal5','sleepHour9101112Animal5'};

%% Set Parameters
gamma = 10;
Q = 20;
iters = 20;
GPiters = [1 1 70];
numSeconds = 5;
R = 5;
highFreq = 50; %90
lowFreq = 1.5;
sampFact = 10; %10
sampStart = 1;

%% Preprocess Data
x = [];
for datasetNum = 1:numel(datasets)
    load(datasets{datasetNum});
    regionNames = strrep(regionNames,'_','');
    sampRateOrig = sampRate;
    % only process a fixed amount of time
    sampRate = sampRateOrig/sampFact;
    N = sampRate*numSeconds;

    xt = X(1:R,:)';

    % notch filter 60Hz signal
    filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
    filtbw=filtfreq/60; % 1Hz Bandwidth
    [filtb,filta]=iirnotch(filtfreq,filtbw);
    xt = filtfilt(filtb,filta,xt);

    % bandpass filter
    highpass = highFreq/sampRateOrig*2;
    lowpass = lowFreq/sampRateOrig*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    xt = filtfilt(butterb,buttera,xt); % bandpass filter

    % subsample by sample factor
    xt = xt(1:sampFact:end,:);
    
    x = [x; xt];
    clearvars xt X
end

% normalize data
x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x)),std(x));

x = x(1:numSeconds*sampRate,:);

% store sample locations
tau = 0:1/sampRate:(numSeconds-1/sampRate);
delta = tau(2) - tau(1);

clear('filtfreq','filtbw','filtb','filta','highpass','lowpass', ...
    'butterb','buttera');

%% Visualize Data
figure(1), clf, plot(tau,x);%plot(tau(1:min(1*sampRate,N)),x(1:min(1*sampRate,N),:,1));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:R))


%% Fit model
Q = 3;

Ntrain = sampRate*1;
xtrain = x(1:Ntrain,:);
tautrain = tau(1:Ntrain);

gamma = 10;

% create Q Gaussian basis functions
bases = SMbases(Q,linspace(3,50,Q),1*ones(Q,1));

% generate kernels (weights and phase of basis functions) for each reagion
kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function

% form GP SMV kernel
GPkernel = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    GPiters, [lowFreq,25], false) ;

%logevidence = GPkernel.updateSlow(tautrain,xtrain);

% take fourier transform
Ns = ceil(Ntrain/2);
xfft = 1/sqrt(Ntrain)*fft(xtrain);
xfft = 2*(xfft(2:Ns+1,:,:));
% store sample locations
delta = tautrain(2) - tautrain(1);
s = 1/(Ntrain*delta):1/(Ntrain*delta):1/(2*delta);
logevidence = GPkernel.update(s,xfft);

figure(2), clf, GPkernel.plotpsd(0,30,regionNames)

figure(3), clf, plot(logevidence);


%% Predict the future!
Ntest = 2*sampRate;
interpScale = 3;
Robs = R-1;

yall = x(Ntrain+1:Ntrain+Ntest,:);
yobs = yall(:,1:Robs);
ytest = yall(:,end);

tautest = 0:1/sampRate/interpScale:((Ntest-1/interpScale)/sampRate);
tauobs = tautest(interpScale:interpScale:end);

ytemp = inf(Ntest*interpScale,R);
ytemp(interpScale:interpScale:end,1:R-1) = yobs;
ytemp(:,R) = nan;
ytemp = reshape(ytemp,[Ntest*interpScale*R,1]);

inds_obs = find(~isnan(ytemp) & ~isinf(ytemp));
inds_test = find(isnan(ytemp));

ytemp(isnan(ytemp)) = [];
ytemp(isinf(ytemp)) = [];

% get kernel
[K,K_nonoise] = GPkernel.K(tautest);
K = real(K); K_nonoise = real(K_nonoise);
K11 = K(inds_obs,inds_obs);
K12 = K(inds_obs,inds_test);
K21 = K(inds_test,inds_obs);
K22 = K_nonoise(inds_test,inds_test);

% predict
y_pred = K21*(K11\ytemp);
K_pred = K22 - K21*(K11\K12);
K_pred_marg = 2*sqrt(diag(K_pred));


% repeat using the marginal SM kernel
inds_obs = Ntest*interpScale*(R-1) + interpScale*(1:Ntest/2);
inds_test = inds_test(Ntest*interpScale/2+1:end);
ytemp = ytest(1:Ntest/2);
K11 = K(inds_obs,inds_obs);
K12 = K(inds_obs,inds_test);
K21 = K(inds_test,inds_obs);
K22 = K_nonoise(inds_test,inds_test);
y_pred_SM = K21*(K11\ytemp);
K_pred_SM = K22 - K21*(K11\K12);
K_pred_marg_SM = 2*sqrt(diag(K_pred_SM));
tauSM = tautest(Ntest*interpScale/2+1:end);


% compare with standard linear regression of a channel on the others
lrX = xtrain(:,1:end-1); lry = xtrain(:,end);
lrbeta = (lrX'*lrX)\lrX'*lry;
lryhat = yobs*lrbeta;


% make figure
figure(4); clf
plot(tauobs,ytest(:,end),'kx','LineWidth',1,'LineWidth',2)
hold on
plot(tauobs, lryhat, 'r-','LineWidth',2)
plot(tauSM, y_pred_SM, 'g-', 'LineWidth', 2);
plot(tautest, y_pred, 'b-', 'LineWidth', 2);
plot(tauSM, y_pred_SM - 2*K_pred_marg_SM, 'g--')
plot(tauSM, y_pred_SM + 2*K_pred_marg_SM, 'g--')
plot(tautest, y_pred - 2*K_pred_marg, 'b--')
plot(tautest, y_pred + 2*K_pred_marg, 'b--')
hold off
xlim([1,1.5]);
set(gca,'XTick',1:.1:1.5,'XTickLabel',0:.1:.5);
legend('Data','Linear Reg.','GP Reg., SM kernel', 'GP Reg., CSM kernel',0)
xlabel('Time (seconds)','FontSize',14,'Interpreter','latex')
ylabel('Normalized Potential','FontSize',14,'Interpreter','latex')


mselr = 1/Ntest*sum((ytest - lryhat).^2)
msecsm = 1/Ntest*sum((ytest - y_pred(interpScale*(1:Ntest))).^2)





% util.ciplot(y_pred_SM(:,1)-2*K_pred_marg_SM(:,1), ...
%     y_pred_SM(:,1)+2*K_pred_marg_SM(:,1), tauobs','r');
% hold on
% util.ciplot(y_pred(:,1)-2*K_pred_marg(:,1), ...
%     y_pred(:,1)+2*K_pred_marg(:,1), tauobs','b');


% 
% 
% % make another figure!
% 
% y_pred2 = [xtrain(end,end);y_pred];
% K_pred_marg2 = [0;K_pred_marg];
% figure(3), clf;
% plot([tautrain(end) tauobs],[xtrain(end,1:end-1); y_(:,1:end-1)],'LineStyle','-.')
% hold on
% util.ciplot(y_pred2(:,1)-2*K_pred_marg2(:,1), ...
%     y_pred2(:,1)+2*K_pred_marg2(:,1), [tautrain(end); tauobs'],'m');
% 
% plot([tautrain(end) tauobs],[xtrain(end,end); y_(:,end)],'k--','LineWidth',3)
% 
% plot(tautrain,xtrain)
% 
% 
% %plot(Nobs+1:N,y_pred,'LineWidth',2)
% %figure
% % C = {'b','g','r','c','m'};
% % for r = 1:Robs
% %     z = zeros(1,Nobs); x_ = tauobs;
% %     S = surface([x_;x_],[y_(:,r)';y_(:,r)'],[z;z],...
% %         'facecol','no',...
% %         'edgecol','interp',...
% %         'linew',1,...
% %         'edgealpha',.15,...
% %         'edgecolor',C{r});
% % end
% 
% xlabel('seconds','FontSize',14)
% title(['Prediction of channel ' regionNames{Rint}],'FontSize',16)
% 
% 
% 
