clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')


addpath(genpath('/home/kru2/Documents/data/neuroscience/StressData'))

miceNames = {'MouseGS007_090914'};
nMice = numel(miceNames);

%% Set Parameters
gamma = 10;
L = 3;
Q = 20;
iters = 5;
GPiters = [50 0 0];


R = 4;
numSeconds = 3;
highFreq = 50; %90
lowFreq = 1.5;

sampRateOrig = 1000;

%% Preprocess Data
x = cell(2,1); xfft = cell(2,1);
for datasetNum = 1:numel(miceNames)
    % load data
    [x{1},regionNames] = loadStressData(miceNames{datasetNum},'_PredictionIso_Homecage_LFP');
    [x{2},~] = loadStressData(miceNames{datasetNum},'_PredictionIso_FIT_LFP');
    [N,~,W] = size(x{1});
    
    x{1} = x{1}(:,1:R,:);
    x{2} = x{2}(:,1:R,:);
    
    % take fourier transform
    Ns = ceil(N/2);
    xfft{1} = 1/sqrt(N)*fft(x{1});
    xfft{1} = 2*(xfft{1}(2:Ns+1,:,:));
    xfft{2} = 1/sqrt(N)*fft(x{2});
    xfft{2} = 2*(xfft{2}(2:Ns+1,:,:));
end

sampRate = N/numSeconds;

% store sample locations
tau = 0:1/sampRate:(numSeconds-1/sampRate);
delta = tau(2) - tau(1);
s = 1/(N*delta):1/(N*delta):1/(2*delta);

%% Visualize Data
figure(1), clf, plot(tau(1:min(1*sampRate,N)),x{1}(1:min(1*sampRate,N),:,1));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:R))


%% Fit HMM to Data with GP observation dist
% create Q Gaussian basis functions
bases = SMbases(Q,linspace(3,highFreq/4,Q),1*ones(Q,1));

% generate kernels (weights and phase of basis functions) for each reagion
kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function

% form GP SMV kernel
GPkernel = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    GPiters, [lowFreq,highFreq/2], false) ;

% create a collection of L GPs
GPkernels = GPs(L, GPkernel);





% Nxfft=abs(xfft.^2);
% Nxfft=bsxfun(@rdivide,Nxfft,sum(Nxfft));
% upperBound=size(Nxfft,1)/5;
% [init.idx,~] = kmeans(reshape(Nxfft(1:upperBound,:,:), ...
%     [upperBound*size(Nxfft,2),W])',L);

%[init.idx,init.mu,init.sumd] = kmeans(y',L);
%init.idx = rand(L,W)'; init.idx = bsxfun(@rdivide,init.idx,sum(init.idx,2));
init.E_A = 0.01*ones(L) + (1 - 0.01*L)*eye(L);

sequences = cell(numel(x),1);
for m = 1:numel(x)
    W = size(x{m},3);
    sequences{m}.states = HMMstates(L,W); %,init.idx);      % HMM state model
    sequences{m}.A = dists.dirichletrows(L,L,init.E_A); % Transition matrix
    sequences{m}.pi = dists.expfam.dirichlet(L);        % Initial state probabilities
end

% create a multi-observation sequence HMM with GPs as observation distributions
addpath('../HSMM/VB/')
model = HMMmultiVB(L,2*nMice,GPkernels,sequences);
clear('bases','kernel','kernels','GPkernel','GPkernels','init','sequences');



% update the model
lbound = zeros(1,iters);
for iter = 1:iters
    fprintf('Iteration %d/%d \n',iter,iters);
    model.update(xfft,s);
    lbound(iter) = model.lowerbound();
    
    figure(2), clf, imagesc(model.seqs{1}.states.r);
    figure(3), clf, model.obsdists.kernels{1}.plotpsd(0,20,regionNames); 
    pause(.5);
end

% model = HMMmodelVB(L,W, ...
%     GPkernels, ...   % Observation distributions
%     HMMstates(L,W,init.idx), ...       % HMM state model
%     dists.dirichletrows(L,L,init.E_A), ...   % Transition matrix
%     dists.expfam.dirichlet(L) );      % Initial state probabilities
% 
% 
% clear('bases','kernel','kernels','GPkernel','GPkernels');
% 
% % update the model
% input = {xfft,s};
% lbound = zeros(1,iters);
% for iter = 3:iters
%     fprintf('Iteration %d/%d \n',iter,iters);
%     model.update(input);
%     lbound(iter) = model.lowerbound();
%     
%     figure(2), clf, imagesc(model.states.r); pause(.5);
%     pause(.5);
% end

%% Display Results
% % Plot log evidence
% figure(3), clf, plot(lbound)
% 
% % Sample example data
% % Nsamp = 2*sampRate;
% % ysamp = GPkernel.sampleKernel(Nsamp,delta);
% % ysamp = bsxfun(@rdivide,bsxfun(@minus,ysamp,mean(ysamp)),std(ysamp));
% % figure(4), clf, plot(delta:delta:Nsamp*delta,ysamp)
% % title('Sampled LFP Data from GP','FontSize',16)
% % xlabel('time (s)','FontSize',14)
% % legend(regionNames(1:R))
% 
% figure(4), clf, imagesc(model.A.geomean), colorbar
% 
%% Plot PSD
for l = 1:L
    figure(l+5), clf, model.obsdists.kernels{l}.plotpsd(0,20,regionNames);
end

%% Generate cross spectrum comparisons between states
% 
% srange= linspace(0,15,1e3);
% r1 = 4; r2 = 3;
% colors = util.distinguishable_colors(L);
% h1 = figure(5); clf;
% xlabel('Frequency (\emph{Hz})','Interpreter','latex','FontSize',14)
% ylabel('Amplitude','Interpreter','latex','FontSize',14)
% hold on
% h2 = figure(6); clf;
% xlabel('Frequency (\emph{Hz})','Interpreter','latex','FontSize',14)
% ylabel('Phase','Interpreter','latex','FontSize',14)
% hold on
% for l = 1:L
%     UKU = model.obsdists.kernels{l}.UKU(srange,1,0);
%     Amps = squeeze(abs(UKU(r1,r2,:)));
%     Phases = wrapToPi(squeeze(angle(UKU(r1,r2,:))));
%     figure(h1), plot(srange,Amps,'Color',colors(l,:),'LineWidth',2);
%     figure(h2), plot(srange,Phases,'Color',colors(l,:),'LineWidth',2);
%     hold on
% end


%%
% r1 = 4; r2 = 3;
% l = 12;
% [~,stateinds] = max(model.states.r,[],1);
% windinds = find(stateinds == l);
% crosscov = zeros(2*N-1,numel(windinds));
% crossspec = zeros(N,numel(windinds));
% for wind = 1:numel(windinds)
%     w = windinds(wind);
%     crosscov(:,wind) = xcov(squeeze(x(:,r1,w)),squeeze(x(:,r2,w)));
%     crossspec(:,wind) = fft(crosscov(N:end,wind));
% end
% figure, errorbar(1:N,mean(smoothts(abs(crossspec),'g'),2),std(abs(crossspec),0,2))
% figure, errorbar(1:N,mean(smoothts(angle(crossspec),'g'),2),std(angle(crossspec),0,2))
% 
% figure, imagesc(crosscov), colorbar
% figure, imagesc(abs(crossspec)), colorbar
% figure, imagesc(angle(crossspec)), colorbar

%% Plot States
% [~,inds] = max(model.states.r,[],1);
% 
% inds2 = repmat(inds,[numSeconds,1]); 
% inds2 = reshape(inds2,[1,numSeconds*W]);
% inds2 = inds2(1:end-2);
% 
% load M5_Score;
% start = 60*60+1;
% M5_Score = M5_Score(start:start+numel(inds2)-1)+1;
% 
% % align colors
% stateMap = 1:L;
% for currind = 2:numel(unique(M5_Score))
%     [~,maxind] = max(histc(inds2(M5_Score==currind),.5:L-.5));
%     stateMap(currind) = maxind; stateMap(maxind) = currind;
%     inds2(inds2 == currind) = L+1;
%     inds2(inds2 == maxind) = currind;
%     inds2(inds2 == L+1) = maxind;
% end
% 
% figure, imagesc(0:W*numSeconds/60,[],[M5_Score; inds2]), colorbar
% colormap(util.distinguishable_colors(L));
% hcb = colorbar;
% legendCell = cellstr(num2str((1:L)', 'State %-d'));
% set(hcb,'YTick',linspace(1+(L-1)/2/L,L-(L-1)/2/L,L),'YTickLabel',legendCell, ...
%     'FontSize',14)
% set(gca,'ytick',1:3,'yticklabel',{'Dzirasa et al.','CSM Kernel'},'FontSize',14)
% %ylabel('Animal')
% xlabel('Minutes','Interpreter','latex','FontSize',14)
% % 


