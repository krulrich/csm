clear; format short e; %close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))
addpath('../../../code')

%% Set Parameters
dataOpts.numSeconds = 4;
dataOpts.highFreq = 90; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5; %10
dataOpts.mouseName = 'MouseGS007_090914';

C = 3;


%% Load and preprocess Data
[X,regionNames] = loadStressData(dataOpts,'_PredictionIso_FIT_LFP');
N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

% take fourier transform
Ns = ceil(N/2);
Xfft = 1/sqrt(N)*fft(X);
Xfft = 2*(Xfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

% select subset of data for training
ws = 105;
W = 3;
xt = X(:,1:C,ws:(ws+W-1));
xtfft = Xfft(:,1:C,ws:(ws+W-1));

%% Visualize Data
figure(1), clf, plot(tau,xt(:,:,1));%plot(tau(1:min(1*sampRate,N)),x(1:min(1*sampRate,N),:,1));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:C))

%% Create model
Q = 20;
R = 2*ones(Q,1);
gamma = 100;

% create Q Gaussian basis functions
%k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);

k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,dataOpts.highFreq],[eps,1e4]), ...
    num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq,Q)),num2cell(ones(1,Q)), 'un', 0);

kernels = GP.kernels.kernels(Q, k);

% generate kernels (weights and phase of basis functions) for each reagion
B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
%B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
coregs = GP.coregs.mats(C,Q,R,B);

% form GP with CSM kernel
model = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);

%% Fit model
opts.iters = 200;
opts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
figure(2); clf;

evals = algorithms.rprop(s,xtfft,model,opts,plotOpts);
%evals = algorithms.rprop(tau,xt,model,opts,plotOpts);

%% Results

%model.logevidenceTrue(tau,reshape(xt,[numel(xt),1]))

figure, model.plotCsd('minFreq',0,'maxFreq',30,'names',regionNames)

figure, 
ax = model.plotCsdComp('minFreq',0,'maxFreq',30,'chan1',1,'chan2',2);
model.plotCsdComp('minFreq',0,'maxFreq',30,'chan1',1,'chan2',3,'ax',ax, ...
    'styles',{'--','--'});

%% Experiment
% 
% margLike = zeros(3,2,3,20);
% BIC = zeros(3,2,3,20);
% for kType = [3]%1:3
%     for BType = 1%1:2
%         for RVal = 2%1:3
%             for iter = 1%20
%                 Q = 20;
%                 R = RVal*ones(Q,1);
%                 gamma = 100;
%                 
%                 xt = X(:,1:C,iter);
%                 xtfft = Xfft(:,1:C,iter);
%                 
%                 % initialize kernels
%                 switch kType
%                     case 1
%                         k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);
%                         kernels = GP.kernels.kernels(Q, k);
%                     case 2
%                         k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,dataOpts.highFreq],[eps,1e4]), ...
%                             num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq,Q)),num2cell(ones(1,Q)), 'un', 0);
%                         kernels = GP.kernels.kernels(Q, k);
%                     case 3
%                         kernels = GP.kernels.gaborKernels(Q,dataOpts.lowFreq,dataOpts.highFreq);
%                         %kernels.nu = 500; 
%                 end
%                 
%                 
%                 % initialize coregionalization matrices
%                 if BType == 1
%                     B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
%                 else
%                     B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
%                 end
%                 coregs = GP.coregs.mats(C,Q,R,B);
%                 
%                 % form GP with CSM kernel
%                 model = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);
%                 %model.updateKernels = 0;
%                 
%                 % Fit model
%                 opts.iters = 400;
%                 opts.evalInterval = 20;
%                 plotOpts.plot = false;
%                 plotOpts.color = 'k';
%                 plotOpts.legendNames = {'CSM Kernel'};
%                 figure(2); clf;
%                 
%                 evals = algorithms.rprop(s,xtfft,model,opts,plotOpts);
%                 
%                 margLike(kType,BType,RVal,iter) = ...
%                     model.logevidenceTrue(tau,reshape(xt,[numel(xt),1]));
%                 
%                 nParams = model.coregs.nParams + model.kernels.nParams + 1;
%                 BIC(kType,BType,RVal,iter) = -2*margLike(kType,BType,RVal,iter) ...
%                     + nParams*2;
%             end
%         end
%     end
% end


%%
% nW = 20; %size(X,3);
% model.updateKernels = 0;
% params = zeros(numel(model.getParams),nW);
% 
% for w = 1:nW
%     xt = X(:,1:C,w);
%     xfft = Xfft(:,1:C,w);
% 
%     opts.iters = 120;
%     algorithms.rprop(s,xfft,model,opts,plotOpts);
%     params(:,w) = model.getParams;
%     
% end
% 
% figure, imagesc(params(2:end,:)), colorbar
% 
% %%
% params2=params(2:end,:);
% qq=std(params2')';
% params3=bsxfun(@rdivide,params2,qq);
% [u,v] = nnmf(params3,10);
% H=u(:,4).*qq;
% figure(6), imagesc(reshape(H,7,[]),[0 1]*max(abs(H)));colorbar
% %%
% model2 = model;
% model2.setParams([100;u(:,3).*qq]);
% figure, model2.plotpsd(0,30);
