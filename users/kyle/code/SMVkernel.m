classdef SMVkernel < handle
    properties
        % model parameters
        R % length of vector, (# observations)
        Q
        kernels % one kernel for each observation
        bases
        gamma
        pink
    end
    
    methods
        function self = SMVkernel(R,Q,bases,kernels,gamma,pink)
            self.R = R;
            self.Q = Q;
            
            if ~exist('bases','var')
                self.bases = SMbases(Q);
                self.kernels = SMkernels(R,Q);
                self.gamma = 1;
                self.pink = 1;
            else
                self.kernels = kernels;
                self.bases = bases;
                self.gamma = gamma;
                self.pink = pink;
            end
        end
        
        function res = K(self,tau)
            N = numel(tau);
            res = zeros(self.R*N);
            for q = 1:self.Q
                B = self.kernels.B(q);
                k = toeplitz(self.bases.basis{q}.covFun(tau));
                res = res + real(kron(B,k));
            end
            res = res + 1/self.gamma*eye(numel(tau)*self.R);
        end
        
        function res = UKU(self,s,N,smallflag)
            SD = self.bases.specDens(s);
            svals = s; svals(s < 1) = 1;
            if exist('smallflag','var') && smallflag
                res = 4*bsxfun(@times,1/self.gamma*eye(self.R), ...
                    permute(svals.^(-self.pink),[1,3,2]));
                for q = 1:self.Q
                    B = diag(diag(self.kernels.B(q)));
                    res = res + N/4*bsxfun(@times,B,reshape(SD(:,q),[1,1,numel(s)]));
                end
                res = res/10;
            else
                res = 4*kron(1/self.gamma*eye(self.R),diag(svals.^(-self.pink))); % pink noise component
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    res = res + N/4*kron(B,diag(SD(:,q)));
                end
            end
        end
        
        function res = logevidence(self,tau,y,K,Kinv)
            logdetK = 2*sum(log(diag(chol(K))));
            N = numel(tau); W = size(y,2);
            
            res = -1/2*N*self.R*W*log(2*pi) - 1/2*W*logdetK;
            res = res - 1/2*sum(sum(y.*(Kinv*y)));
            %res = trace(-1/2*y'*Kinv*y - 1/2*logdetK - numel(tau)/2*log(2*pi));
        end
        
        function res = logevidenceDFT(self,x,Gammainv)
            [N,W] = size(x);
            logDetTerm = 0;
            for n = 1:N
                logDetTerm = logDetTerm + 2*sum(log(diag(Gammainv(:,:,n))) + ...
                    log(diag(conj(Gammainv(:,:,n)))));
            end
            xGx = bsxfun(@times,bsxfun(@times,Gammainv, ...
                permute(conj(x),[2,4,1,3])),permute(x,[4,2,1,3]));
            res = -N*self.R*W*log(pi) + W/2*logDetTerm - real(sum(xGx(:)));
        end
        
        function plotK(self,tau,names)
            if ~exist('names','var')
                labs = 1:self.R;
            else
                labs = names;
            end
            
            K = self.K(tau);
            N = numel(tau);
            imagesc(K), colorbar
            title('Covariance Matrix','FontSize',16)
            inds = round((N/2):N:(N*self.R));
            set(gca,'XTick',inds,'XTickLabel',labs)
            set(gca,'YTick',inds,'YTickLabel',labs)
        end
        
        function res = update(self,tau,y,iters,showkernel)
            if ~exist('iters','var'), iters = 100; end
            g.var = zeros(self.Q,1);    g.mu = zeros(self.Q,1);
            g.w = zeros(self.Q,self.R); g.psi = zeros(self.Q,self.R);
            g.gamma = 0;
            g_old = g;
            step.var = .01*ones(self.Q,1);    step.mu = .01*ones(self.Q,1);
            step.w = .01*ones(self.Q,self.R); step.psi = .01*ones(self.Q,self.R);
            step.gamma = .1;
            
            W = size(y,2);
            
            res = zeros(iters,1);
            
            for iter = 1:iters
                basesnew = self.bases; kernelsnew = self.kernels; gammanew = self.gamma;
                
                K = self.K(tau);
                K_inv = inv(K);
                alpha = K_inv*y;
                A = alpha*alpha' - W*K_inv; At = A'; At = At(:)';
                res(iter) = self.logevidence(tau,y,K,K_inv);
                
                fprintf('iteration %d, log p(y|X) = %d, gamma = %d\n',iter,res(iter),self.gamma);
                
                if showkernel
                    figure(3), clf;
                    self.plotlogpsd(0,20,-6,0);
                    %self.plotK(tau);
                end
                
                % wrt gamma
                dK = -1/self.gamma^2*eye(numel(tau)*self.R);
                g.gamma = 1/2*sum(sum(A.*dK'));
                [gammanew, g_old.gamma, step.gamma] = ...
                    self.rprop(gammanew,g.gamma,g_old.gamma, ...
                    step.gamma,1e-4,1e4,1e-4,1e2);
                
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    %[Ub,Sb] = eigs(B,rank(B));
                    
                    % wrt variance
                    dk = toeplitz(self.bases.derivCovFunVar(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.var(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    g.var(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.var, g_old.var(q), step.var(q)] = ...
                        self.rprop(basesnew.basis{q}.var,g.var(q),g_old.var(q), ...
                        step.var(q),1e-2,1e2,1e-2,.5);
                    
                    %wrt mean
                    dk = toeplitz(self.bases.derivCovFunMean(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.mu(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    g.mu(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.mu, g_old.mu(q), step.mu(q)] = ...
                        self.rprop(basesnew.basis{q}.mu,g.mu(q),g_old.mu(q), ...
                        step.mu(q),0,1/4/(tau(2)-tau(1)),1e-4,.5);
                    
                    k = toeplitz(self.bases.covFun(tau,q));
                    %[Uk,Sk] = eigs(k,rank(k));
                    for r = 1:self.R
                        %wrt weight
                        dB = self.kernels.dBdw(q,r);
                        
                        %[Udb,Sdb] = eigs(dB,1);
                        %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                        %g.w(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                        
                        dK = real(kron(dB,k));
                        g.w(q,r) = 1/2*At*dK(:);
                        [kernelsnew.k{r}.w(q), g_old.w(q,r), step.w(q,r)] = ...
                            self.rprop(kernelsnew.k{r}.w(q),g.w(q,r),g_old.w(q,r), ...
                            step.w(q,r),eps,1e2,eps,1e2);
                        
                        %wrt phase
                        if r > 1
                            dB = self.kernels.dBdpsi(q,r);
                            
                            %[Udb,Sdb] = eigs(dB,1);
                            %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                            %g.psi(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                            
                            dK = real(kron(dB,k));
                            g.psi(q,r) = 1/2*At*dK(:);
                            [kernelsnew.k{r}.psi(q), g_old.psi(q,r), step.psi(q,r)] = ...
                                self.rprop(kernelsnew.k{r}.psi(q),g.psi(q,r),g_old.psi(q,r), ...
                                step.psi(q,r),-inf,inf,1e-4,1e0);
                        end
                    end
                end
                self.kernels = kernelsnew;
                self.bases = basesnew;
                self.gamma = gammanew;
            end
        end
        
        function res = updateDFT(self,tau,y,iters,showkernel)
            if ~exist('iters','var'), iters = 100; end
            g.var = zeros(self.Q,1);    g.mu = zeros(self.Q,1);
            g.w = zeros(self.Q,self.R); g.psi = zeros(self.Q,self.R);
            g.gamma = 0; g.pink = 0;
            g_old = g;
            step.var = .01*ones(self.Q,1);    step.mu = .01*ones(self.Q,1);
            step.w = .01*ones(self.Q,self.R); step.psi = .01*ones(self.Q,self.R);
            step.gamma = .1; step.pink = 0.0001;
            
            N = numel(tau); W = size(y,3); Ns = ceil(N/2);
            
            yfft = 1/sqrt(N)*fft(y);
            yfft = (yfft(2:Ns+1,:,:));
            yfftri = cat(4,real(yfft),imag(yfft));
            
            x = reshape(yfft,[Ns*self.R,W]);
            x = [real(x); imag(x)];
            
            delta = tau(2) - tau(1);
            s = 1/(N*delta):1/(N*delta):1/(2*delta);
            
            res = zeros(iters,1);
            
            for iter = 1:iters
                basesnew = self.bases; kernelsnew = self.kernels;
                gammanew = self.gamma; pinknew = self.pink;
                
                [Akeep,Gammainv] = self.getA(yfftri,s,N);
                
                res(iter) = self.logevidenceDFT(yfft,Gammainv);
                
                fprintf('iteration %d, log p(y|X) = %d, gamma = %d, pink = %d\n', ...
                    iter, res(iter), self.gamma, self.pink);
                
                if showkernel
                    figure(3), clf;
                    self.plotlogpsd(0,50,-9,-3);
                    %self.plotK(tau);
                end
                
                % wrt gamma
                svals = s; svals(svals < 1) = 1;
                g.gamma = self.getGradFast(-1/self.gamma^2*eye(self.R), ...
                    svals.^(-self.pink),Akeep);
                [gammanew, g_old.gamma, step.gamma] = ...
                    self.rprop(gammanew,g.gamma,g_old.gamma, ...
                    step.gamma,0,1e6,1e-4,1e4);
                
                % wrt pink
                dpink = -self.pink*s.^(-self.pink-1); dpink(s < 1) = 0;
                g.pink = self.getGradFast(1/self.gamma*eye(self.R), ...
                    dpink,Akeep);
                [pinknew, g_old.pink, step.pink] = ...
                    self.rprop(pinknew,g.pink,g_old.pink, ...
                    step.pink,0,3,.0001,10);
                %pinknew = 0;
                for q = 1:self.Q
                    B = diag(diag(self.kernels.B(q)));
                    
                    %wrt variance
                    g.var(q) = self.getGradFast(B,self.bases.derivSpecDensVar(s,q),Akeep);
                    [basesnew.basis{q}.var, g_old.var(q), step.var(q)] = ...
                        self.rprop(basesnew.basis{q}.var,g.var(q),g_old.var(q), ...
                        step.var(q),1e-2,1e2,eps,.5);
                    
                    %wrt mean
                    g.mu(q) = self.getGradFast(B,self.bases.derivSpecDensMean(s,q),Akeep);
                    [basesnew.basis{q}.mu, g_old.mu(q), step.mu(q)] = ...
                        self.rprop(basesnew.basis{q}.mu,g.mu(q),g_old.mu(q), ...
                        step.mu(q),0,Ns/2-1,eps,1);%1/4/(tau(2)-tau(1))
                    
                    w = self.bases.specDens(s,q);
                    for r = 1:self.R
                        %wrt weight
                        g.w(q,r) = self.getGradFast(diag(diag(self.kernels.dBdw(q,r))),w,Akeep);
                        [kernelsnew.k{r}.w(q), g_old.w(q,r), step.w(q,r)] = ...
                            self.rprop(kernelsnew.k{r}.w(q),g.w(q,r),g_old.w(q,r), ...
                            step.w(q,r),eps,1e1,eps,1e2);
                        
                        %wrt phase
                        if r > 1
                            g.psi(q,r) = self.getGradFast(diag(diag(self.kernels.dBdpsi(q,r))),w,Akeep);
                            [kernelsnew.k{r}.psi(q), g_old.psi(q,r), step.psi(q,r)] = ...
                                self.rprop(kernelsnew.k{r}.psi(q),g.psi(q,r),g_old.psi(q,r), ...
                                step.psi(q,r),-inf,inf,eps,1e-1);
                        end
                    end
                end
                self.kernels = kernelsnew;
                self.bases = basesnew;
                self.gamma = gammanew;
                self.pink = pinknew;
            end
        end
        
        function [Akeep,Gammainv] = getA(self,yfftri,s,N)
            Ns = numel(s); W = size(yfftri,3);
            % get matrix A = a*a' - W*K^{-1} for GP gradient steps
            Gamma = self.UKU(s,N,1);
            Gammainv = zeros(self.R,self.R,Ns);
            for n = 1:Ns
                Gammainv(:,:,n) = inv(Gamma(:,:,n));
            end
            Gammainvri = cat(5,cat(4,real(Gammainv),imag(-Gammainv)),...
                cat(4,imag(Gammainv),real(Gammainv)));
            
            alpha = sum(sum(bsxfun(@times,Gammainvri,...
                permute(yfftri,[5,2,1,6,4,3])),2),5);
            Akeep2 = sum(bsxfun(@times,alpha, ...
                permute(alpha,[2,1,3,5,4,6])),6) - W*Gammainvri;
            Akeep = reshape(Akeep2,[self.R,self.R,Ns,4]);
        end      
        
        function res = getGrad(self,B,w,At)
            dS = self.complexNormalCov(kron(B,w));
            res = 1/2*At*dS(:);
        end
        
        function plotlogpsd(self,minFreq,maxFreq,minlogphi,maxlogphi)
            ha = util.tight_subplot(self.R,self.R,[.03 .03],[.1 .05],[.2 .2]);
            s= linspace(minFreq,maxFreq,1e3)';
            
            phi = zeros(1e3,self.R,self.R);
            means = self.bases.mean;
            weights = self.kernels.weights;
            weights = bsxfun(@rdivide,weights,sum(weights,2));
            shifts = self.kernels.shifts;
            
            for r1 = 1:self.R
                for r2 = 1:self.R
                    phi(:,r1,r2) = log(sum(bsxfun(@times,weights(r1,:).*weights(r2,:), ...
                        self.bases.specDens(s)),2));
                    phi(:,r1,r2) = max(phi(:,r1,r2),minlogphi);
                    
                    inds = (weights(r1,:).*weights(r2,:) > 3e-4);
                    
                    [haxes,hline1,hline2] = ...
                        plotyy(ha(r1+self.R*(r2-1)), ...
                        s,phi(:,r1,r2), means(inds),...
                        wrapToPi(shifts(r2,inds) - shifts(r1,inds)));
                    set(hline1,'LineWidth',2);
                    set(hline2,'LineStyle','*','LineWidth',5);
                    ylim(haxes(2),[-pi,pi])
                    ylim(haxes(1),[minlogphi,maxlogphi])
                    xlim(haxes(1),[minFreq,maxFreq])
                    xlim(haxes(2),[minFreq,maxFreq])
                    
                    if r1 == self.R
                        set(haxes(2),'YTick',linspace(-pi,pi,5))
                        ylabel(haxes(2),'phase','FontSize',14)
                    else set(haxes(2),'YTick',[]);
                    end
                    
                    if r1 == 1
                        set(haxes(1),'YTick',minlogphi:maxlogphi)
                        ylabel(haxes(1),'PSD','FontSize',14)
                    else set(haxes(1),'YTick',[]);
                    end
                    
                    if r2 ~= self.R
                        set(haxes(1),'XTick',[]); set(haxes(2),'XTick',[]);
                    else
                        xlabel(haxes(1),'frequency','FontSize',14)
                        xlabel(haxes(2),'frequency','FontSize',14)
                    end
                end
            end
            
        end
    end
    
    methods (Static)
        function [val,g,step] = rprop(val,g,g_old,step,minval,maxval,minstep,maxstep)
            %if ~exist('minval','var'), minval = -inf; maxval = inf; end
            
            if g*g_old > 0
                step = 1.2*step;
            elseif g*g_old < 0
                step = 0.5*step;
                g = 0;
            end
            
            step = max(min(step,maxstep),minstep);
            val = val + step*sign(g);
            if val > maxval || val < minval
                val = val - step*sign(g);
                step = 0.5*step;
                g = 0;
            end
        end
        
        function res = complexNormalCov(Gamma)
            res = 1/2*[real(Gamma) imag(-Gamma); imag(Gamma) real(Gamma)];
        end
        
        function res = getGradFast(B,w,A)
            w = reshape(w,[1,1,numel(w)]);
            dS = bsxfun(@times,B,w); dS = dS(:);
            dS2 = [real(dS);imag(-dS);imag(dS);real(dS)];
            res = 1/2*A(:)'*dS2;
        end
    end
end

%% Backup updateDFT function (just in case)

% function res = logevidenceDFT(self,s,x,S,Sinv)
%             N = numel(s); W = size(x,2);
%             logdetS = 2*sum(log(diag(chol(S))));
%
%             res = -1/2*N*self.R*W*log(2*pi) - 1/2*W*logdetS;
%             res = res - 1/2*sum(sum(x.*(Sinv*x)));
% end
%




%                 function res = updateDFT(self,tau,y,iters,showkernel)
        %                     if ~exist('iters','var'), iters = 100; end
        %                     N = numel(tau); W = size(y,3); Ns = ceil(N/2);
        %                     delta = tau(2) - tau(1);
        %                     s = 1/(N*delta):1/(N*delta):1/(2*delta);
        %                     res = zeros(iters,1);
        %
        %                     % initialize BFGS variables
        %                     nvars = self.Q*(1+2*self.R)+1;
        %                     H = 1*eye(nvars); % approximate Hessian matrix
        %                     %ss = 1e-7; % step size (fixed for now?)
        %
        %                     % reformat data for algorithm
        %                     yfft = 1/sqrt(N)*fft(y);
        %                     yfft = 2*(yfft(2:Ns+1,:,:));
        %                     yfftri = cat(4,real(yfft),imag(yfft));
        %
        %                     for iter = 1:iters
        %                         [Akeep,Gammainv] = self.getA(yfftri,s,N);
        %
        %                         res(iter) = self.logevidenceDFT(yfft,Gammainv);
        %
        %                         fprintf('iteration %d, log p(y|X) = %d, gamma = %d\n',iter,res(iter),self.gamma);
        %
        %                         if showkernel
        %                             figure(3), clf;
        %                             self.plotlogpsd(0,30,-7,-1);
        %                         end
        %
        %                         % BFGS
        %                         g = self.getGrads(s,Akeep);
        %                         x = self.getCurrentValues();
        %                         p = -H\g;
        %
        %                         % RPROP line search
        %         %                 for iter2 = 1:50
        %         %                     if g*g_old > 0
        %         %                         step = 1.2*step;
        %         %                     elseif g*g_old < 0
        %         %                         step = 0.5*step;
        %         %                         g = 0;
        %         %                     end
        %         %                 end
        %
        %                         % backtracking line search
        %                         ss = 1/max(abs(p)); m = p'*g; c = .5; d = .5; conv = false;
        %                         f = self.logevidenceDFT(yfft,Gammainv);
        %                         while ~conv
        %                             %ss
        %                             xnew = abs(x + ss*p);
        %                             self.updateValues(xnew);
        %                             [Akeep,Gammainv] = self.getA(yfftri,s,N);
        %                             fnew = self.logevidenceDFT(yfft,Gammainv);
        %                             %[f-fnew ss*(-c*m)]
        %                             if (f-fnew) < abs(ss*(-c*m)), conv = true;
        %                             else ss = d*ss; end
        %                         end
        %
        %                         gnew = self.getGrads(s,Akeep);
        %                         d = ss*p; q = gnew - g;
        %                         H = H + (q*q')/(q'*d) - (H*(d*d')*H')/(d'*H*d);
        %                     end
        %                 end


%         function x = getCurrentValues(self)
%             nvars = self.Q*(1+2*self.R)+1;  x = zeros(nvars,1); cntr = 1;
%             x(cntr) = self.gamma; cntr = cntr+1;
%             for q = 1:self.Q
%                 x(cntr) = self.bases.basis{q}.var; cntr = cntr+1;
%                 x(cntr) = self.bases.basis{q}.mu; cntr = cntr+1;
%                 for r = 1:self.R
%                     x(cntr) = self.kernels.k{r}.w(q); cntr = cntr+1;
%                     if r > 1
%                         x(cntr) = self.kernels.k{r}.psi(q); cntr = cntr+1;
%                     end
%                 end
%             end
%         end
%         
%         function updateValues(self,x)
%             cntr = 1;
%             self.gamma = x(cntr); cntr = cntr+1;
%             for q = 1:self.Q
%                 self.bases.basis{q}.var = x(cntr); cntr = cntr+1;
%                 self.bases.basis{q}.mu = x(cntr); cntr = cntr+1;
%                 for r = 1:self.R
%                     self.kernels.k{r}.w(q) = x(cntr); cntr = cntr+1;
%                     if r > 1
%                         self.kernels.k{r}.psi(q) = x(cntr); cntr = cntr+1;
%                     end
%                 end
%             end
%         end
%         
%         function g = getGrads(self,s,Akeep)
%             nvars = self.Q*(1+2*self.R)+1;  g = zeros(nvars,1); cntr = 1;
%             Ns = numel(s);
%             
%             % wrt gamma
%             g(1) = self.getGradFast(eye(self.R),-1/self.gamma^2*ones(Ns,1),Akeep);
%             cntr = cntr+1;
%             
%             for q = 1:self.Q
%                 B = self.kernels.B(q);
%                 
%                 %wrt variance
%                 g(cntr) = self.getGradFast(B,self.bases.derivSpecDensVar(s,q),Akeep);
%                 cntr = cntr+1;
%                 
%                 %wrt mean
%                 g(cntr) = self.getGradFast(B,self.bases.derivSpecDensMean(s,q),Akeep);
%                 cntr = cntr+1;
%                 
%                 w = self.bases.specDens(s,q);
%                 for r = 1:self.R
%                     %wrt weight
%                     g(cntr) = self.getGradFast(self.kernels.dBdw(q,r),w,Akeep);
%                     cntr = cntr+1;
%                     
%                     %wrt phase
%                     if r > 1
%                         g(cntr) = self.getGradFast(self.kernels.dBdpsi(q,r),w,Akeep);
%                         cntr = cntr+1;
%                     end
%                 end
%             end
%         end
% 
