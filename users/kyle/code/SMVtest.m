clear; clc; format short e; close all;


sampRate = 100; numSeconds = 7;
N = sampRate*numSeconds;
Q = 2;
R = 2;
gamma = 1;
W = 1;

means = [5 10]'; vars = [1 1]';


SMVk_true = SMVkernelDFT(R,Q, ...
    SMbases(Q,means,vars), ...
    SMkernels(R,Q), ...
    gamma, 0 ) ;

tau = linspace(0,numSeconds,N);

K = SMVk_true.K(tau);

%figure(1), clf, imagesc(K), colorbar;

y = mvnrnd(zeros(R*N,1),real(K),W)';
y_R = reshape(y,[N,R,W]);
figure(2), clf, plot(tau,y_R(:,:,1))
xlabel('time','FontSize',14);
title('Random Signals','FontSize',16)
figure(5), clf, SMVk_true.plotpsd(0,30);

delta = tau(2) - tau(1);
s = 1/(N*delta):1/(N*delta):1/(2*delta);

%% Test stuff
% spectral domain stuff
%U = 1/sqrt(N)*fft(eye(N)); G = kron(eye(R),U);
N = numel(tau); W = size(y,3); Ns = ceil(N/2);
yfft = 1/sqrt(N)*fft(y_R);
yfft = 2*(yfft(2:Ns+1,:,:));
%figure, imagesc(abs(yfft.^2)), colorbar
%UKU= squeeze(SMVk_true.UKU(s,1,0));
%figure, imagesc(UKU,[0,max(UKU)]), colorbar
delta = tau(2) - tau(1);
s = 1/(N*delta):1/(N*delta):1/(2*delta); %s = [s, fliplr(s(1:end-1))];
% Ut = U'; Gtranshalf = kron(eye(R),Ut(2:ceil(N/2+1),:));
% figure, imagesc(real(Gtranshalf*K*Gtranshalf')), colorbar
% figure, imagesc(real(SMVk_true.UKU(s,N))), colorbar

%% Fit GP
Q = 4;
iters = [0 0 50];

bases = SMbases(Q,linspace(3,25,Q),1*ones(Q,1));
    
% generate kernels (weights and phase of basis functions) for each reagion
kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function

% form GP SMV kernel
SMVk_fit = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    iters, [1,50], false) ;
clear('basesSlow','kernelSlow','kernelsSlow')


logevidence = SMVk_fit.update(s,yfft);


%% Plot Covariance
%figure(3), clf, SMVk_fit.plotK(tau);

%% Sample draw from fit
y_draw = mvnrnd(zeros(R*N,1),real(SMVk_fit.K(tau)));
y_draw_R = reshape(y_draw,[N,R]);
figure(4), clf, plot(tau,y_draw_R)

%% Compare
figure(5), clf, SMVk_true.plotpsd(0,15);
figure(6), clf, SMVk_fit.plotpsd(0,15);

%% Plot log evidence
figure(7), clf, plot(logevidence)

%figure(8), clf, imagesc(real(SMVk_fit.UKU(s,0))), colorbar


%% Compare
%UKUtrue= squeeze(SMVk_true.UKU(s,1,0));
%UKUfit= squeeze(SMVk_fit.UKU(s,1,0));
%figure, imagesc([UKUtrue UKUfit],[0,max(max(UKUtrue),max(UKUfit))]), colorbar