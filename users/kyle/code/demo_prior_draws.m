clear; close all; format short e;

gamma = 10000;

Q = 2; R = 2;
mu = [4,5]'; nu = [.1,.1]';

delta = 1/100;
N = 100;
Nfft = 2^nextpow2(2*N-1);
tau = 0:delta:(N-1)*delta;

U = 1/sqrt(N)*fft(eye(N));
IU = kron(eye(R),U);

%% CSM Kernel
% create Q Gaussian basis functions
bases = SMbases(Q,mu,nu);

% generate kernels (weights and phase of basis functions) for each reagion
kernel1 = SMkernel(Q,[1;1],[0;0]);
kernel2 = SMkernel(Q,[.5;1],[0;pi/2]);
kernels = SMkernels(R, Q, {kernel1,kernel2} ); % TODO give kernel copy function

% form GP SMV kernel
GPkernel_CSM = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    [20,20,20], [2,25], false) ;

K_CSM = GPkernel_CSM.K(tau);
S = IU'*K_CSM*IU;

y_CSM = mvnrnd(zeros(1,R*N),real(K_CSM));
y_CSM = reshape(y_CSM,[N,R]);

xspec_CSM = fft(y_CSM(:,1),Nfft) .* conj(fft(y_CSM(:,2),Nfft));

%% LMC-SM Kernel
% create Q Gaussian basis functions
bases = SMbases(Q,mu,nu);

% generate kernels (weights and phase of basis functions) for each reagion
kernel1 = SMkernel(Q,[1;1],[0;0]);
kernel2 = SMkernel(Q,[.5;1],[0;0]);
kernels = SMkernels(R, Q, {kernel1,kernel2} ); % TODO give kernel copy function

% form GP SMV kernel
GPkernel_LMCSM = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    [20,20,20], [2,25], false) ;

K_LMCSM = real(GPkernel_LMCSM.K(tau));

y_LMCSM = mvnrnd(zeros(1,R*N),K_LMCSM);
y_LMCSM = reshape(y_LMCSM,[N,R]);

xspec_LMCSM = fft(y_LMCSM(:,1),Nfft) .* conj(fft(y_LMCSM(:,2),Nfft));

%% make plots

figure(1), clf;
ha = util.tight_subplot(1,3,[.04 .04],[.23 .05],[.03 .07]);

plot(ha(1),tau,y_CSM,'LineWidth',2)
xlabel(ha(1),'Time','FontSize',11)
ylim(ha(1),[-4.5,4.5])
legend(ha(1),'f_1(x)','f_2(x)')

plot(ha(2),tau,y_LMCSM,'LineWidth',2)
xlabel(ha(2),'Time','FontSize',11)
ylim(ha(2),[-4.5,4.5])
legend(ha(2),'f_1(x)','f_2(x)')

colors = get(groot,'DefaultAxesColorOrder');
ax = GPkernel_CSM.plotcsm(3,6,1,2,{'-','-'},colors([4,5],:),0);
GPkernel_LMCSM.plotcsm(3,6,1,2,{'--','--'},colors([4,5],:),1,ax);