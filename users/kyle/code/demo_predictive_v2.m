clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))

%% Set Parameters
dataOpts.numSeconds = 8;
dataOpts.highFreq = 80; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5; %10
dataOpts.mouseName = 'MouseGS007_090914';

C = 5;

nsTrain = 4;

%% Load and preprocess Data
[X,regionNames] = loadStressData(dataOpts,'_PredictionIso_Homecage_LFP');
N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

W = 10;
X = X(:,:,W);

Ntrain = nsTrain*sampRate;

% select subset of data for training
xt = X(1:Ntrain,1:C);
tau = 0:1/sampRate:(nsTrain-1/sampRate);

% take fourier transform
Ns = ceil(Ntrain/2);
xtfft = 1/sqrt(Ntrain)*fft(xt);
xtfft = 2*(xtfft(2:Ns+1,:,:));
s = (sampRate/Ntrain):(sampRate/Ntrain):(sampRate/2);

%% Visualize Data
figure(1), clf, plot(tau,xt(:,:,1));%plot(tau(1:min(1*sampRate,N)),x(1:min(1*sampRate,N),:,1));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:C))

%% Create model
Q = 10;
R = 2*ones(Q,1);
gamma = 100;

% create Q Gaussian basis functions
%k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);

k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,dataOpts.highFreq],[eps,1e4]), ...
    num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq,Q)),num2cell(ones(1,Q)), 'un', 0);

kernels = GP.kernels.kernels(Q, k);

% generate kernels (weights and phase of basis functions) for each reagion
B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
%B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
coregs = GP.coregs.mats(C,Q,R,B);

% form GP with CSM kernel
model = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);

%% Fit model
opts.iters = 200;
opts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
figure(2); clf;

evals = algorithms.rprop(s,xtfft,model,opts,plotOpts);
%evals = algorithms.rprop(tau,xt,model,opts,plotOpts);

%% Show results
figure, clf, model.plotpsd(0,30,regionNames)

%% Predict the future!
Ntest = 2*sampRate;
interpScale = 3;
Cobs = C-1;

yall = X(Ntrain+1:Ntrain+Ntest,:);
yobs = yall(:,1:Cobs);
ytest = yall(:,end);

tautest = 0:1/sampRate/interpScale:((Ntest-1/interpScale)/sampRate);
tauobs = tautest(interpScale:interpScale:end);

ytemp = inf(Ntest*interpScale,C);
ytemp(interpScale:interpScale:end,1:C-1) = yobs;
ytemp(:,C) = nan;
ytemp = reshape(ytemp,[Ntest*interpScale*C,1]);

inds_obs = find(~isnan(ytemp) & ~isinf(ytemp));
inds_test = find(isnan(ytemp));

ytemp(isnan(ytemp)) = [];
ytemp(isinf(ytemp)) = [];

% get kernel
[K,K_nonoise] = model.K(tautest);
K = real(K); K_nonoise = real(K_nonoise);
K11 = K(inds_obs,inds_obs);
K12 = K(inds_obs,inds_test);
K21 = K(inds_test,inds_obs);
K22 = K_nonoise(inds_test,inds_test);

% predict
y_pred = K21*(K11\ytemp);
K_pred = K22 - K21*(K11\K12);
K_pred_marg = 2*sqrt(diag(K_pred));


% repeat using the marginal SM kernel
inds_obs = Ntest*interpScale*(C-1) + interpScale*(1:Ntest/2);
inds_test = inds_test(Ntest*interpScale/2+1:end);
ytemp = ytest(1:Ntest/2);
K11 = K(inds_obs,inds_obs);
K12 = K(inds_obs,inds_test);
K21 = K(inds_test,inds_obs);
K22 = K_nonoise(inds_test,inds_test);
y_pred_SM = K21*(K11\ytemp);
K_pred_SM = K22 - K21*(K11\K12);
K_pred_marg_SM = 2*sqrt(diag(K_pred_SM));
tauSM = tautest(Ntest*interpScale/2+1:end);


% compare with standard linear regression of a channel on the others
lrX = xt(:,1:end-1); lry = xt(:,end);
lrbeta = (lrX'*lrX)\lrX'*lry;
lryhat = yobs*lrbeta;


% make figure
figure(4); clf
plot(tauobs,ytest(:,end),'kx','LineWidth',1,'LineWidth',2)
hold on
plot(tauobs, lryhat, 'r-','LineWidth',2)
plot(tauSM, y_pred_SM, 'g-', 'LineWidth', 2);
plot(tautest, y_pred, 'b-', 'LineWidth', 2);
plot(tauSM, y_pred_SM - 2*K_pred_marg_SM, 'g--')
plot(tauSM, y_pred_SM + 2*K_pred_marg_SM, 'g--')
plot(tautest, y_pred - 2*K_pred_marg, 'b--')
plot(tautest, y_pred + 2*K_pred_marg, 'b--')
hold off
xlim([1,1.5]);
set(gca,'XTick',1:.1:1.5,'XTickLabel',0:.1:.5);
legend('Data','Linear Reg.','GP Reg., SM kernel', 'GP Reg., CSM kernel',0)
xlabel('Time (seconds)','FontSize',14,'Interpreter','latex')
ylabel('Normalized Potential','FontSize',14,'Interpreter','latex')


mselr = 1/Ntest*sum((ytest - lryhat).^2)
msecsm = 1/Ntest*sum((ytest - y_pred(interpScale*(1:Ntest))).^2)





% util.ciplot(y_pred_SM(:,1)-2*K_pred_marg_SM(:,1), ...
%     y_pred_SM(:,1)+2*K_pred_marg_SM(:,1), tauobs','r');
% hold on
% util.ciplot(y_pred(:,1)-2*K_pred_marg(:,1), ...
%     y_pred(:,1)+2*K_pred_marg(:,1), tauobs','b');


% 
% 
% % make another figure!
% 
% y_pred2 = [xtrain(end,end);y_pred];
% K_pred_marg2 = [0;K_pred_marg];
% figure(3), clf;
% plot([tautrain(end) tauobs],[xtrain(end,1:end-1); y_(:,1:end-1)],'LineStyle','-.')
% hold on
% util.ciplot(y_pred2(:,1)-2*K_pred_marg2(:,1), ...
%     y_pred2(:,1)+2*K_pred_marg2(:,1), [tautrain(end); tauobs'],'m');
% 
% plot([tautrain(end) tauobs],[xtrain(end,end); y_(:,end)],'k--','LineWidth',3)
% 
% plot(tautrain,xtrain)
% 
% 
% %plot(Nobs+1:N,y_pred,'LineWidth',2)
% %figure
% % C = {'b','g','r','c','m'};
% % for r = 1:Robs
% %     z = zeros(1,Nobs); x_ = tauobs;
% %     S = surface([x_;x_],[y_(:,r)';y_(:,r)'],[z;z],...
% %         'facecol','no',...
% %         'edgecol','interp',...
% %         'linew',1,...
% %         'edgealpha',.15,...
% %         'edgecolor',C{r});
% % end
% 
% xlabel('seconds','FontSize',14)
% title(['Prediction of channel ' regionNames{Rint}],'FontSize',16)
% 
% 
% 
