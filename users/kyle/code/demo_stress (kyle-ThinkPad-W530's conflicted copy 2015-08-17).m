clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))

%% Set Parameters
dataOpts.numSeconds = 3;
dataOpts.highFreq = 50; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 5; %10
dataOpts.miceNames = {'MouseGS007_090914','MouseGS016_091014', ...
    'MouseII006_010115','MouseII007_010115','MouseII010_010115', ...
    'MouseII011_010115','MouseII016_010215','MouseII018_010215', ...
    'MouseII019_010215','MouseIII003_031215','MouseIII024_031215', ...
    'MouseIII025_031215','MouseIII026_031215','MouseIII027_031315', ...
    'MouseIII028_031315','MouseIII029_031415','MouseIII030_031415', ...
    'MouseIII031_031315','MouseIII032_031315','MouseRH002_090914', ...
    'MouseRH009_090914','MouseRH015_090914','MouseRH019_091214', ...
    'MouseRH022_091014','MouseRH024_091014','MouseSK028_091014', ...
    'MouseSK029_091114','MouseSK030_091214','MouseSK031_091114', ...
    'MouseSK032_091114','MouseSK033_091114','MouseSK034_091214', ...
    'MouseSK035_091214'};
    
    % CHANS Files DNE: {'MouseIV035_041315', ...
    %'MouseIV036_041315','MouseIV037_041315','MouseIV038_041415', ...
    %'MouseIV039_041415','MouseIV042_041515','MouseIV043_041415', ...
    %'MouseIV044_041515','MouseIV045_041515','MouseSK036_091214'};
dataOpts.taskTypes = {'_PredictionIso_Homecage_LFP','_PredictionIso_FIT_LFP'};

modelOpts.C = 7;
modelOpts.Q = 20;
modelOpts.R = 2*ones(modelOpts.Q,1);
modelOpts.gamma = 100;

algOpts.iters = 300;
algOpts.evalInterval = 400;

plotOpts.plot = false;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};

%% Loop through all datasets
params = cell(numel(dataOpts.miceNames),numel(dataOpts.taskTypes));

for ds = 1%:numel(params)
    mouseNum = ceil(ds/2); taskNum = mod(ds-1,2)+1;
    
    % get data for mouse and task
    dataOpts.mouseName = dataOpts.miceNames{mouseNum};
    [X,regionNames] = loadStressData(dataOpts,dataOpts.taskTypes{taskNum});
    N = size(X,1); Ns = ceil(N/2);
    sampRate = N/dataOpts.numSeconds;
    Xfft = 1/sqrt(N)*fft(X); Xfft = 2*(Xfft(2:Ns+1,:,:));
    tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
    s = (sampRate/N):(sampRate/N):(sampRate/2);

    % create model
    kernels = GP.kernels.gaborKernels(modelOpts.Q,dataOpts.lowFreq,40);
    kernels.nu = 225;
    %B = arrayfun(@(r)GP.coregs.matComplex(modelOpts.C,r),modelOpts.R,'un',0);
    B = arrayfun(@(r)GP.coregs.matReal(modelOpts.C,r),modelOpts.R,'un',0);
    coregs = GP.coregs.mats(modelOpts.C,modelOpts.Q,modelOpts.R,B);
    model = GP.LMC_DFT(modelOpts.C,modelOpts.Q,modelOpts.R,coregs,kernels,modelOpts.gamma);
    model.updateKernels = 0;
    
    % fit model
    nW = size(X,3);
    params{mouseNum,taskNum} = zeros(modelOpts.Q*modelOpts.C^2,nW);
    tic
    for w = 1:10%nW
        model.gamma = 100;
        algorithms.rprop(s, Xfft(:,1:modelOpts.C,w), model, algOpts, plotOpts);
        params{mouseNum,taskNum}(:,w) = model.coregs.getMatsVec();
        fprintf('Mouse %d/%d   Task %d/%d   Window %d/%d   Time %0.2f\n', ...
            mouseNum,numel(dataOpts.miceNames),taskNum,numel(dataOpts.taskTypes), ...
            w,nW,toc)
    end
end

%save results_MiceGStoSK

%%
%params = params(1:18,:);
paramsAll = horzcat(params{:});
figure, imagesc(min(paramsAll,1.5)), colorbar

figure, model.plotpsd(0,30,regionNames)

%[u,ls,v] = svd(paramsAll);

[u,v] = nnmf(min(paramsAll,1.5),5);

coregmats = reshape(u(:,2),[modelOpts.C,modelOpts.C,modelOpts.Q]);
coregmats = squeeze(mat2cell(coregmats,modelOpts.C,modelOpts.C,ones(modelOpts.Q,1)));
figure, model.plotpsd(0,30,regionNames,coregmats);


figure, imagesc(u), colorbar
figure, imagesc(v), colorbar

figure, imagesc(u*v), colorbar

%% Classify
[~,~,v] = svd(paramsAll);
paramsProj = v';
paramsProj = paramsProj(1:200,:);

Y = cellfun(@(x,z)z*ones(1,size(x,2)),params,num2cell([ones(size(params,1),1) 2*ones(size(params,1),1)]),'un',0);
Y = horzcat(Y{:});

k=10;
cvFolds = crossvalind('Kfold', Y, k);   %# get indices of 10-fold CV
cp = classperf(Y);                      %# init performance tracker

for i = 1:k                                  %# for each fold
    ninds = numel(cvFolds);
    testIdx = ((1:ninds)' > (ninds/k*(i-1)+1)) & ((1:ninds)' < ninds/k*i);
    %testIdx = (cvFolds == i);                %# get indices of test instances
    trainIdx = ~testIdx;                     %# get indices training instances

    %# train an SVM model over training instances
    svmModel = fitcsvm(paramsAll(:,trainIdx)', Y(trainIdx));

    %# test using test instances
    pred = predict(svmModel, paramsAll(:,testIdx)');

    %# evaluate and update performance object
    cp = classperf(cp, pred, testIdx);
    
    i
end

%# get accuracy
cp.CorrectRate

%# get confusion matrix
%# columns:actual, rows:predicted, last-row: unclassified instances
cp.CountingMatrix


%% Compare to FFT Data
paramsFFT = cell(numel(dataOpts.miceNames),numel(dataOpts.taskTypes));

for ds = 1:18*2%:numel(params)
    mouseNum = ceil(ds/2); taskNum = mod(ds-1,2)+1;
    
    % get data for mouse and task
    dataOpts.mouseName = dataOpts.miceNames{mouseNum};
    [X,regionNames] = loadStressData(dataOpts,dataOpts.taskTypes{taskNum});
    N = size(X,1); Ns = ceil(N/2);
    sampRate = N/dataOpts.numSeconds;
    Xfft = 1/sqrt(N)*fft(X); Xfft = 2*(Xfft(2:Ns+1,:,:));
    paramsFFT{mouseNum,taskNum} = reshape(abs(Xfft),[size(Xfft,1)*size(Xfft,2),size(Xfft,3)]);
end

paramsFFTAll = horzcat(paramsFFT{:});
[uFFT,sFFT,vFFT] = svd(paramsFFTAll);
paramsFFTProj = vFFT';
paramsFFTProj = paramsFFTProj(1:100,:);

Y = cellfun(@(x,z)z*ones(1,size(x,2)),params,num2cell([ones(size(params,1),1) 2*ones(size(params,1),1)]),'un',0);
Y = horzcat(Y{:});

k = 10;
cvFoldsFFT = crossvalind('Kfold', Y, k);   %# get indices of 10-fold CV
cpFFT = classperf(Y);                      %# init performance tracker

for i = 1:k                                  %# for each fold
    testIdx = (cvFoldsFFT == i);                %# get indices of test instances
    trainIdx = ~testIdx;                     %# get indices training instances

    %# train an SVM model over training instances
    svmModelFFT = fitcsvm(paramsFFTProj(:,trainIdx)', Y(trainIdx));

    %# test using test instances
    predFFT = predict(svmModelFFT, paramsFFTProj(:,testIdx)');

    %# evaluate and update performance object
    cpFFT = classperf(cpFFT, predFFT, testIdx);
    
    i
end

%# get accuracy
cpFFT.CorrectRate

%# get confusion matrix
%# columns:actual, rows:predicted, last-row: unclassified instances
cpFFT.CountingMatrix

