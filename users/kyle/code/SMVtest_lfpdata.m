clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')


addpath('/home/kru2/Documents/data/neuroscience/StressData/LFPData')
datasets = {'MouseGS007_090914_PredictionIso_FIT_LFP'};%,'sleepHour5678Animal5','sleepHour9101112Animal5'};

%% Set Parameters
gamma = 10;
L = 14;
Q = 20;
iters = 20;
GPiters = [1 1 50];
numSeconds = 2;
R = 4;
highFreq = 100; %90
lowFreq = 1.5;
sampFact = 20; %10
sampStart = 1;

%% Preprocess Data
x = [];
for datasetNum = 1:numel(datasets)
    load(datasets{datasetNum});
    regionNames = strrep(regionNames,'_','');
    sampRateOrig = sampRate;
    % only process a fixed amount of time
    sampRate = sampRateOrig/sampFact;
    N = sampRate*numSeconds;

    xt = X(1:R,:)';

    % notch filter 60Hz signal
    filtfreq=60/sampRateOrig*2; % Filter out 60Hz Signal
    filtbw=filtfreq/60; % 1Hz Bandwidth
    [filtb,filta]=iirnotch(filtfreq,filtbw);
    xt = filtfilt(filtb,filta,xt);

    % bandpass filter
    highpass = highFreq/sampRateOrig*2;
    lowpass = lowFreq/sampRateOrig*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    xt = filtfilt(butterb,buttera,xt); % bandpass filter

    % subsample by sample factor
    xt = xt(1:sampFact:end,:);
    
    x = [x; xt];
    clearvars xt X
end

% normalize data
x = bsxfun(@rdivide,bsxfun(@minus,x,mean(x)),std(x));

% separate into windows
W = floor(size(x,1)/(numSeconds*sampRate));
x = x(1:W*numSeconds*sampRate,:);
x = permute(reshape(x',[R,N,W]),[2,1,3]);

% take fourier transform
Ns = ceil(N/2);
xfft = 1/sqrt(N)*fft(x);
xfft = 2*(xfft(2:Ns+1,:,:));

% store sample locations
tau = 0:1/sampRate:(numSeconds-1/sampRate);
delta = tau(2) - tau(1);
s = 1/(N*delta):1/(N*delta):1/(2*delta);

clear('filtfreq','filtbw','filtb','filta','highpass','lowpass', ...
    'butterb','buttera');

%% Visualize Data
figure(1), clf, plot(tau(1:min(1*sampRate,N)),x(1:min(1*sampRate,N),:,1));
title('Example LFP Data','FontSize',16)
xlabel('time (s)','FontSize',14)
legend(regionNames(1:R))

%% Fit Model to Data
% y is of size N x R x W
% define a Vector Spectral Mixture kernel
%logspace(log10(4),log10(maxFreq),Q)
% x = x(:,:,1:20);
% xfft = xfft(:,:,1:20);
% 
% gamma = 10;
% 
% % create Q Gaussian basis functions
% bases = SMbases(Q,linspace(3,25,Q),1*ones(Q,1));
% 
% % generate kernels (weights and phase of basis functions) for each reagion
% kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
% kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function
% 
% % form GP SMV kernel
% GPkernel = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
%     GPiters, [lowFreq,highFreq/2], false) ;
% 
% %logevidence = SMVk_fit.update(tau,x,iters,[lowFreq,highFreq/2],false);
% logevidence = GPkernel.update(s,xfft);
% %logevidence = GPkernel.updateSlow(tau,x);
% 
% figure(2), clf, GPkernel.plotpsd(0,30,regionNames)
% 
% figure(3), clf, plot(logevidence);

%% Fit GP MM to Data
% % create Q Gaussian basis functions
% bases = SMbases(Q,linspace(3,highFreq/2,Q),1*ones(Q,1));
% 
% % generate kernels (weights and phase of basis functions) for each reagion
% kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
% kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function
% 
% % form GP SMV kernel
% GPkernel = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
%     GPiters, [lowFreq,highFreq/2], false) ;
% 
% % create a GP mixture model
% GPMM_fit = GPMM(L,W,GPkernel,xfft);
% clear('bases','kernel','kernels','GPkernel');
% 
% % update the model
% logevidence = GPMM_fit.update(s,xfft,iters);




%% Fit HMM to Data with GP observation dist
% create Q Gaussian basis functions
bases = SMbases(Q,linspace(3,highFreq/4,Q),1*ones(Q,1));

% generate kernels (weights and phase of basis functions) for each reagion
kernel = SMkernel(Q,1/Q*ones(Q,1),zeros(Q,1));
kernels = SMkernels(R, Q, repmat({kernel},[R,1])); % TODO give kernel copy function

% form GP SMV kernel
GPkernel = SMVkernelDFT(R, Q, bases, kernels, gamma , 0, ...
    GPiters, [lowFreq,highFreq/2], false) ;

% create a collection of L GPs
GPkernels = GPs(L, GPkernel);

% create a HMM with the GPs as observation distributions
addpath('../../HSMM/VB/')

Nxfft=abs(xfft.^2);
Nxfft=bsxfun(@rdivide,Nxfft,sum(Nxfft));
upperBound=size(Nxfft,1)/5;
[init.idx,~] = kmeans(reshape(Nxfft(1:upperBound,:,:), ...
    [upperBound*size(Nxfft,2),W])',L);

%[init.idx,init.mu,init.sumd] = kmeans(y',L);
%init.idx = rand(L,W)'; init.idx = bsxfun(@rdivide,init.idx,sum(init.idx,2));
init.E_A = 0.01*ones(L) + (1 - 0.01*L)*eye(L);

% maxD = 300;
% %durdist = dists.poissgammadur(maxD,L,1,1,500*ones(L,1),ones(L,1)); % Poisson Gamma
% durdist = dists.dirichletrows(maxD,L,ones(L,maxD)); % Dirichlet
% 
% model = HSMMmodelVB(L,W,maxD, ...
%     GPkernels, ...   % Observation distributions
%     HSMMstates(L,W,maxD,init.idx), ...       % HMM state model
%     dists.dirichletrows(L,L,init.E_A,init.E_A), ...   % Transition matrix
%     dists.expfam.dirichlet(L*maxD,ones(L,maxD),ones(L,maxD)), ...
%     durdist);      % Initial state probabilities

model = HMMmodelVB(L,W, ...
    GPkernels, ...   % Observation distributions
    HMMstates(L,W,init.idx), ...       % HMM state model
    dists.dirichletrows(L,L,init.E_A), ...   % Transition matrix
    dists.expfam.dirichlet(L) );      % Initial state probabilities


clear('bases','kernel','kernels','GPkernel','GPkernels');

% update the model
input = {xfft,s};
lbound = zeros(1,iters);
for iter = 1:iters
    fprintf('Iteration %d/%d \n',iter,iters);
    model.update(input);
    lbound(iter) = model.lowerbound();
    
    %figure(2), clf, imagesc(model.D.geomean)
    %title('Duration Distribution')
    
    %figure(3), clf, imagesc(squeeze(sum(model.states.gamma,2)))
    %[~,lbound(:,iter)] = model.lowerbound();
    figure(2), clf, imagesc(model.states.r); pause(.5);
    pause(.5);
end

%% Display Results
% % Plot log evidence
% figure(3), clf, plot(lbound)
% 
% % Sample example data
% % Nsamp = 2*sampRate;
% % ysamp = GPkernel.sampleKernel(Nsamp,delta);
% % ysamp = bsxfun(@rdivide,bsxfun(@minus,ysamp,mean(ysamp)),std(ysamp));
% % figure(4), clf, plot(delta:delta:Nsamp*delta,ysamp)
% % title('Sampled LFP Data from GP','FontSize',16)
% % xlabel('time (s)','FontSize',14)
% % legend(regionNames(1:R))
% 
% figure(4), clf, imagesc(model.A.geomean), colorbar
% 
%% Plot PSD
for l = 1:L
    figure(l+5), clf, model.obsdists.kernels{l}.plotpsd(0,20,regionNames);
end

%% Generate cross spectrum comparisons between states

srange= linspace(0,15,1e3);
r1 = 4; r2 = 3;
colors = util.distinguishable_colors(L);
h1 = figure(5); clf;
xlabel('Frequency (\emph{Hz})','Interpreter','latex','FontSize',14)
ylabel('Amplitude','Interpreter','latex','FontSize',14)
hold on
h2 = figure(6); clf;
xlabel('Frequency (\emph{Hz})','Interpreter','latex','FontSize',14)
ylabel('Phase','Interpreter','latex','FontSize',14)
hold on
for l = stateMap %1:L
    UKU = model.obsdists.kernels{l}.UKU(srange,1,0);
    Amps = squeeze(abs(UKU(r1,r2,:)));
    Phases = wrapToPi(squeeze(angle(UKU(r1,r2,:))));
    figure(h1), plot(srange,Amps,'Color',colors(l,:),'LineWidth',2);
    figure(h2), plot(srange,Phases,'Color',colors(l,:),'LineWidth',2);
    hold on
end


%%
r1 = 4; r2 = 3;
l = 12;
[~,stateinds] = max(model.states.r,[],1);
windinds = find(stateinds == l);
crosscov = zeros(2*N-1,numel(windinds));
crossspec = zeros(N,numel(windinds));
for wind = 1:numel(windinds)
    w = windinds(wind);
    crosscov(:,wind) = xcov(squeeze(x(:,r1,w)),squeeze(x(:,r2,w)));
    crossspec(:,wind) = fft(crosscov(N:end,wind));
end
figure, errorbar(1:N,mean(smoothts(abs(crossspec),'g'),2),std(abs(crossspec),0,2))
figure, errorbar(1:N,mean(smoothts(angle(crossspec),'g'),2),std(angle(crossspec),0,2))

figure, imagesc(crosscov), colorbar
figure, imagesc(abs(crossspec)), colorbar
figure, imagesc(angle(crossspec)), colorbar

%% Plot States
[~,inds] = max(model.states.r,[],1);

inds2 = repmat(inds,[numSeconds,1]); 
inds2 = reshape(inds2,[1,numSeconds*W]);
inds2 = inds2(1:end-2);

load M5_Score;
start = 60*60+1;
M5_Score = M5_Score(start:start+numel(inds2)-1)+1;

% align colors
stateMap = 1:L;
for currind = 2:numel(unique(M5_Score))
    [~,maxind] = max(histc(inds2(M5_Score==currind),.5:L-.5));
    stateMap(currind) = maxind; stateMap(maxind) = currind;
    inds2(inds2 == currind) = L+1;
    inds2(inds2 == maxind) = currind;
    inds2(inds2 == L+1) = maxind;
end

figure, imagesc(0:W*numSeconds/60,[],[M5_Score; inds2]), colorbar
colormap(util.distinguishable_colors(L));
hcb = colorbar;
legendCell = cellstr(num2str((1:L)', 'State %-d'));
set(hcb,'YTick',linspace(1+(L-1)/2/L,L-(L-1)/2/L,L),'YTickLabel',legendCell, ...
    'FontSize',14)
set(gca,'ytick',1:3,'yticklabel',{'Dzirasa et al.','CSM Kernel'},'FontSize',14)
%ylabel('Animal')
xlabel('Minutes','Interpreter','latex','FontSize',14)
% 


