classdef GPs < handle
    % class that contains L GP kernels
    properties
        L
        kernels
    end
    
    methods
        function self = GPs(L,kernels)
            self.L = L;
            
            if numel(kernels) == L
                self.kernels = kernels;
            else
                % kernels class must contain copy method
                for l = 1:L
                    self.kernels{l} = kernels.copy();
                end
            end     
        end
        
        function update(self,input,z)
            y = input{1}; tau = input{2};
            for l = 1:self.L
                fprintf('  -- Updating GP %d/%d ',l,self.L);
                self.kernels{l}.update(tau, y, z(l,:));
                fprintf('\n');
            end
        end
        
        function res = expectloglike(self,input)
            y = input{1}; tau = input{2};
            W = size(y,3);
            res = zeros(self.L,W);
            for l = 1:self.L
                res(l,:) = self.kernels{l}.logEvidence( y, tau );
            end
        end
                
        function res = lowerboundcontrib(self)
            % currently, point mass on posterior of parameters in GPs give
            % them zero contribution to the ELBO
            res = 0;
        end
    end
end