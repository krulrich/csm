close all;

gamma = SMVk_fit.gamma;
B = SMVk_fit.bases.basis{2}.k_imag(tau);
C = SMVk_fit.kernels.B(2);

rC = rank(C); rB = rank(B);

[Uc,Sc] = eig(C);%,rC);
[Ub,Sb] = eig(B);%,rB);
rC = numel(diag(Sc)); rB = numel(diag(Sb));

Kinv = gamma*eye(N*R);

for rc = 1:rC
    for rb = 1:rB
        sc = Sc(rc,rc); sb = Sb(rb,rb);
        U = kron(Uc(:,rc),Ub(:,rb));
        g = 1./(1+sc*sb*U'*Kinv*U);
        %BkronCk = real(sc*sb*(U*U'));
        %g = 1./(1+sum(sum(Kinv.*BkronCk.')));
        KU = Kinv*U*sqrt(sc*sb);
        Kinv = Kinv - g*(KU*KU');
        %Kinv = Kinv - g*Kinv*BkronCk*Kinv;
    end
end
Kinv = real(Kinv);

Kinv2 = inv(real(kron(C,B)) + 1/gamma*eye(N*R));

figure, imagesc(Kinv), colorbar
figure, imagesc(Kinv2), colorbar
figure, imagesc(Kinv - Kinv2), colorbar