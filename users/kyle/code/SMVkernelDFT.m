classdef SMVkernelDFT < handle
    properties
        % model parameters
        R % length of vector, (# observations)
        Q
        kernels % one kernel for each observation
        bases
        gamma
        pink
        
        iters
        freqRange
        showKernel
        
        g
        gold
        step
    end
    
    methods
        function self = SMVkernelDFT(R,Q,bases,kernels,gamma,pink, ...
                iters,freqRange,showKernel)
            if exist('R','var')
                self.R = R;
                self.Q = Q;
                
                if ~exist('bases','var')
                    self.bases = SMbases(Q);
                    self.kernels = SMkernels(R,Q);
                    self.gamma = 1;
                    self.pink = 1;
                else
                    self.kernels = kernels;
                    self.bases = bases;
                    self.gamma = gamma;
                    self.pink = pink;
                end
                
                if ~exist('iters','var'), iters = 100*ones(3,1);
                elseif numel(iters) ~= 3, iters = iters(1)*ones(3,1); end
                if ~exist('freqRange','var'), freqRange = [2,40]; end
                if ~exist('showKernel','var'), showKernel = 0; end
                
                self.iters = iters;
                self.freqRange = freqRange;
                self.showKernel = showKernel;
                
                
                self.g.var = zeros(self.Q,1);    self.g.mu = zeros(self.Q,1);
                self.g.w = zeros(self.Q,self.R); self.g.psi = zeros(self.Q,self.R);
                self.g.gamma = 0; self.g.pink = 0;
                self.gold = self.g;
                self.step.var = .01*ones(self.Q,1);    self.step.mu = .01*ones(self.Q,1);
                self.step.w = .01*ones(self.Q,self.R); self.step.psi = .01*ones(self.Q,self.R);
                self.step.gamma = .1; self.step.pink = 0.0001;
            end
        end
        
        function [res_noise,res] = K(self,tau)
            N = numel(tau);
            res = zeros(self.R*N);
            for q = 1:self.Q
                B = self.kernels.B(q);
                k = toeplitz(self.bases.basis{q}.covFun(tau)); % look into this...should be symmetric....
                res = res + kron(B,k);
            end
            res_noise = res + 1/self.gamma*eye(numel(tau)*self.R);
        end
        
        function res = UKU(self,s,smallflag,indepflag)
            Ns = numel(s); delta = 1/(2*Ns*(s(2)-s(1)));
            
            SD = self.bases.specDens(s);
            svals = s; svals(s < 1) = 1;
            if exist('smallflag','var') && smallflag
                res = bsxfun(@times,1/self.gamma*eye(self.R), ...
                    permute(svals.^(-self.pink),[1,3,2]));
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    if indepflag, B = diag(diag(B)); end
                    res = res + 1/delta/2*bsxfun(@times,B,reshape(SD(:,q),[1,1,numel(s)]));
                end
            else
                res = kron(1/self.gamma*eye(self.R),diag(svals.^(-self.pink))); % pink noise component
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    res = res + 1/delta/2*kron(B,diag(SD(:,q)));
                end
            end
        end
        
        function ll = logEvidence(self,yfft,s,UKUinvri)
            yfftri = cat(4,real(yfft),imag(yfft));
            Ns = size(yfftri,1);
            if ~exist('UKUinvri','var'), [~,UKUinvri] = self.getUKUinv(s,0); end
            
            logDetTerm = 0;
            for n = 1:Ns
                Gammainvnri = [UKUinvri(:,:,n,1,1) UKUinvri(:,:,n,1,2); ...
                    UKUinvri(:,:,n,2,1) UKUinvri(:,:,n,2,2)];
                logDetTerm = logDetTerm + log(det(Gammainvnri));
            end
            
            Kinvy = sum(sum(bsxfun(@times,UKUinvri,...
                permute(yfftri,[5,2,1,6,4,3])),2),5);
            ytKinvy = squeeze(sum(sum(bsxfun(@times,Kinvy,...
                permute(yfftri,[2,5,1,4,6,3])),1),4));
            
            ll = -Ns*self.R/2*log(2*pi) + 1/2*logDetTerm - 1/2*sum(ytKinvy,1);
        end
        
        function ll = logEvidenceSlow(self,tau,y,K,Kinv)
            logdetK = 2*sum(log(diag(chol(K))));
            N = numel(tau); W = size(y,2);
            
            ll = -1/2*N*self.R*W*log(2*pi) - 1/2*W*logdetK;
            ll = ll - 1/2*sum(sum(y.*(Kinv*y)));
            %res = trace(-1/2*y'*Kinv*y - 1/2*logdetK - numel(tau)/2*log(2*pi));
        end
        
        function res = updateSlow(self,tau,y)            
            W = size(y,3);
            N = size(y,1);
            
            y = reshape(y,[N*self.R,W]);
            
            res = zeros(self.iters(end),1);
            
            for iter = 1:self.iters(end)
                basesnew = self.bases; kernelsnew = self.kernels; gammanew = self.gamma;
                
                K = real(self.K(tau));
                K_inv = inv(K);
                alpha = K_inv*y;
                A = alpha*alpha' - W*K_inv; At = A'; At = At(:)';
                res(iter) = self.logEvidenceSlow(tau,y,K,K_inv);
                
                fprintf('iteration %d, log p(y|X) = %d, gamma = %d\n',iter,res(iter),self.gamma);
                
                if self.showKernel
                    figure(3), clf;
                    self.plotpsd(0,20);
                    pause(.1);
                    %self.plotK(tau);
                end
                
                % wrt gamma
                dK = -1/self.gamma^2*eye(numel(tau)*self.R);
                self.g.gamma = 1/2*sum(sum(A.*dK'));
                [gammanew, self.gold.gamma, self.step.gamma] = ...
                    self.rprop(gammanew,self.g.gamma,self.gold.gamma, ...
                    self.step.gamma,1e-4,1e4,1e-4,1e2);
                
                for q = 1:self.Q
                    B = self.kernels.B(q);
                    %[Ub,Sb] = eigs(B,rank(B));
                    
                    % wrt variance
                    dk = toeplitz(self.bases.derivCovFunVar(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.var(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    self.g.var(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.var, self.gold.var(q), self.step.var(q)] = ...
                        self.rprop(basesnew.basis{q}.var,self.g.var(q),self.gold.var(q), ...
                        self.step.var(q),1e-2,1e2,1e-2,.5);
                    
                    %wrt mean
                    dk = toeplitz(self.bases.derivCovFunMean(tau,q));
                    
                    %[Udk,Sdk] = eigs(dk,1);
                    %S = kron(Sb,Sdk); U = kron(Ub,Udk);
                    %g.mu(q) = 1/2*real(sum(sum((S*U'*A).*U.')));
                    
                    dK = real(kron(B,dk));
                    self.g.mu(q) = 1/2*At*dK(:);
                    [basesnew.basis{q}.mu, self.gold.mu(q), self.step.mu(q)] = ...
                        self.rprop(basesnew.basis{q}.mu,self.g.mu(q),self.gold.mu(q), ...
                        self.step.mu(q),0,1/4/(tau(2)-tau(1)),1e-4,.5);
                    
                    k = toeplitz(self.bases.covFun(tau,q));
                    %[Uk,Sk] = eigs(k,rank(k));
                    for r = 1:self.R
                        %wrt weight
                        dB = self.kernels.dBdw(q,r);
                        
                        %[Udb,Sdb] = eigs(dB,1);
                        %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                        %g.w(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                        
                        dK = real(kron(dB,k));
                        self.g.w(q,r) = 1/2*At*dK(:);
                        [kernelsnew.k{r}.w(q), self.gold.w(q,r), self.step.w(q,r)] = ...
                            self.rprop(kernelsnew.k{r}.w(q),self.g.w(q,r),self.gold.w(q,r), ...
                            self.step.w(q,r),eps,1e2,eps,1e2);
                        
                        %wrt phase
                        if r > 1
                            dB = self.kernels.dBdpsi(q,r);
                            
                            %[Udb,Sdb] = eigs(dB,1);
                            %S = kron(Sdb,Sk); U = kron(Udb,Uk);
                            %g.psi(q,r) = 1/2*real(sum(sum((S*U'*A).*U.')));
                            
                            dK = real(kron(dB,k));
                            self.g.psi(q,r) = 1/2*At*dK(:);
                            [kernelsnew.k{r}.psi(q), self.gold.psi(q,r), self.step.psi(q,r)] = ...
                                self.rprop(kernelsnew.k{r}.psi(q),self.g.psi(q,r),self.gold.psi(q,r), ...
                                self.step.psi(q,r),-inf,inf,1e-4,1e0);
                        end
                    end
                end
                self.kernels = kernelsnew;
                self.bases = basesnew;
                self.gamma = gammanew;
            end
        end
        
        function res = update(self,s,yfft,Ez)
            if ~exist('Ez','var'), Ez = ones(1,size(yfft,3)); end
            
            yfftri = cat(4,real(yfft),imag(yfft));
            
            res = zeros(sum(self.iters),1);
            
            for iter = 1:sum(self.iters)
                if iter <= self.iters(1), phaseflag = 0; basesflag = 1; indepflag = 1;
                elseif iter < sum(self.iters(1:2))+1, phaseflag = 1; basesflag = 0; indepflag = 0;
                elseif iter == sum(self.iters(1:2))+1
                    phaseflag = 1; basesflag = 1;  indepflag = 0;
                    self.step.var = .01*ones(self.Q,1);    self.step.mu = .01*ones(self.Q,1);
                    self.step.w = .01*ones(self.Q,self.R); self.step.psi = .01*ones(self.Q,self.R);
                    self.step.gamma = .1; self.step.pink = 0.0001;
                end
                
                basesnew = self.bases; kernelsnew = self.kernels;
                
                [~,UKUinvri] = self.getUKUinv(s,indepflag);
                Akeep = self.getA(yfftri,Ez,UKUinvri);
                
                if basesflag
                    gammanew = self.updateGamma(s,Akeep);
                    pinknew = self.updatePink(s,Akeep);
                end
                for q = 1:self.Q
                    if basesflag
                        basesnew.basis{q}.var = self.updateVariance(s,q,Akeep,indepflag);
                        basesnew.basis{q}.mu = self.updateMean(s,q,Akeep,self.freqRange,indepflag);
                        for r = 1:self.R
                            kernelsnew.k{r}.w(q) = self.updateWeight(s,q,r,Akeep,indepflag); end
                    end
                    if phaseflag
                        for r = 1:self.R
                            kernelsnew.k{r}.psi(q) = self.updatePhase(s,q,r,Akeep,indepflag); end
                    end
                end
                
                res(iter) = sum(Ez.*self.logEvidence(yfft,s,UKUinvri));
                fprintf('.');
                %fprintf('iteration %d, log p(y|X) = %d, gamma = %d, pink = %d\n', ...
                %    iter, res(iter), self.gamma, self.pink);
                if self.showKernel && iter > 5, figure(3), clf, self.plotpsd(0,30); pause(.1); end
                
                self.kernels = kernelsnew; self.bases = basesnew;
                self.gamma = gammanew; self.pink = pinknew;
            end
            
            self.iters = [0,0,25];
        end
        
        function res = updateGamma(self,s,Akeep)
            svals = s; svals(svals < 1) = 1;
            self.g.gamma = self.getGradFast(-1/self.gamma^2*eye(self.R), ...
                svals.^(-self.pink),Akeep);
            [res, self.gold.gamma, self.step.gamma] = ...
                self.rprop(self.gamma,self.g.gamma,self.gold.gamma, ...
                self.step.gamma,0,1e6,1e-4,1e4);
        end
        
        function res = updatePink(self,s,Akeep)
            % wrt pink
            dpink = -self.pink*s.^(-self.pink-1); dpink(s < 1) = 0;
            self.g.pink = self.getGradFast(1/self.gamma*eye(self.R), ...
                dpink,Akeep);
            [res, self.gold.pink, self.step.pink] = ...
                self.rprop(self.pink,self.g.pink,self.gold.pink, ...
                self.step.pink,0,3,.0001,10);
            %res = 0;
        end
        
        function res = updateVariance(self,s,q,Akeep,indepflag)
            B = self.kernels.B(q);
            if indepflag, B = diag(diag(B)); end
            self.g.var(q) = self.getGradFast(B,self.bases.derivSpecDensVar(s,q),Akeep);
            [res, self.gold.var(q), self.step.var(q)] = ...
                self.rprop(self.bases.basis{q}.var,self.g.var(q),self.gold.var(q), ...
                self.step.var(q),1e-4,2e2,eps,.5);
        end
        
        function res = updateMean(self,s,q,Akeep,freqRange,indepflag)
            B = self.kernels.B(q);
            if indepflag, B = diag(diag(B)); end
            self.g.mu(q) = self.getGradFast(B,self.bases.derivSpecDensMean(s,q),Akeep);
            [res, self.gold.mu(q), self.step.mu(q)] = ...
                self.rprop(self.bases.basis{q}.mu,self.g.mu(q),self.gold.mu(q), ...
                self.step.mu(q),freqRange(1),inf,eps,1);
        end
        
        function res = updateWeight(self,s,q,r,Akeep,indepflag)
            w = self.bases.specDens(s,q);
            dBdw = self.kernels.dBdw(q,r);
            if indepflag, dBdw = diag(diag(dBdw)); end
            self.g.w(q,r) = self.getGradFast(dBdw,w,Akeep);
            [res, self.gold.w(q,r), self.step.w(q,r)] = ...
                self.rprop(self.kernels.k{r}.w(q),self.g.w(q,r),self.gold.w(q,r), ...
                self.step.w(q,r),eps,1e1,eps,1e2);
        end
        
        function res = updatePhase(self,s,q,r,Akeep,indepflag)
            w = self.bases.specDens(s,q);
            dBdpsi = self.kernels.dBdpsi(q,r);
            if indepflag, dBdpsi = diag(diag(dBdpsi)); end
            self.g.psi(q,r) = self.getGradFast(dBdpsi,w,Akeep);
            [res, self.gold.psi(q,r), self.step.psi(q,r)] = ...
                self.rprop(self.kernels.k{r}.psi(q),self.g.psi(q,r),self.gold.psi(q,r), ...
                self.step.psi(q,r),-inf,inf,eps,1e-1);
        end
        
        function y = sampleKernel(self,N,delta)
            Ns = ceil(N/2);
            s = 1/(N*delta):1/(N*delta):1/(2*delta);
            
            UKU = self.UKU(s,1,0);
            Uy = zeros(Ns,self.R);
            for n = 1:Ns
                UKUn = UKU(:,:,n);
                UKUnri = 2*[real(UKUn) imag(-UKUn); imag(UKUn) real(UKUn)];
                Uyri = mvnrnd(zeros(2*self.R,1),UKUnri);
                Uy(n,:) = Uyri(1:self.R) + 1i*Uyri(self.R+1:end);
            end
            y = real(ifft([Uy; flipud(Uy)]));
        end
        
        function Akeep = getA(self,yfftri,Ez,UKUinvri)
            % get matrix A = a*a' - W*K^{-1} for GP gradient steps
            
            Ns = size(yfftri,1);
            data = bsxfun(@times,yfftri,reshape(Ez,[1,1,numel(Ez)]));
            W = sum(Ez);
            
            alpha1 = sum(sum(bsxfun(@times,UKUinvri,...
                permute(data,[5,2,1,6,4,3])),2),5);
            alpha2 = sum(sum(bsxfun(@times,UKUinvri,...
                permute(data,[2,5,1,4,6,3])),1),4);
            
            Akeep2 = sum(bsxfun(@times,alpha1,alpha2),6) - W*UKUinvri;
            Akeep = reshape(Akeep2,[self.R,self.R,Ns,4]);
        end
        
        function [UKUinv,UKUinvri] = getUKUinv(self,s,indepflag)
            Ns = numel(s);
            UKU = self.UKU(s,1,indepflag);
            %UKUinv = zeros(self.R,self.R,Ns);
            if self.R == 1
                UKUinv = 1./UKU;
            else
                UKUcell = mat2cell(UKU,self.R,self.R,ones(Ns,1));
                UKUinvcell = cellfun( @(X) pinv(X), UKUcell, 'UniformOutput', false );
                UKUinv = cell2mat(UKUinvcell);
                %parfor n = 1:Ns
                %    UKUinv(:,:,n) = pinv(UKU(:,:,n));
                %end
            end
            UKUinvri = 1/2*cat(5,cat(4,real(UKUinv),imag(-UKUinv)),...
                cat(4,imag(UKUinv),real(UKUinv)));
        end
        
        function plotpsd(self,minFreq,maxFreq,names)
            if ~exist('names','var'), names = num2cell(num2str(1:self.R)); end
            
            ha = util.tight_subplot(self.R,self.R,[.01 .01],[.1 .05],[.2 .2]);
            s= linspace(minFreq,maxFreq,1e3);
            
            UKU = self.UKU(s,1,0);
            allAmplitudes = abs(UKU);
            allPhases = wrapToPi(angle(UKU));
            
            maxAmplitude = max(allAmplitudes(:));
            
            for r1 = 1:self.R
                for r2 = 1:self.R
                    amplitude = squeeze(allAmplitudes(r1,r2,:));
                    phase = squeeze(allPhases(r1,r2,:));
                    
                    % replace phase in discontinuity function with NaNs
                    [sp,phase] = util.removeDiscontinuities(s,phase);
                    
                    [haxes,hline1,hline2] = plotyy(ha(r1+self.R*(r2-1)), ...
                        s,amplitude,sp,phase);
                    
                    set(hline1,'LineWidth',2);
                    set(hline2,'LineWidth',2);
                    ylim(haxes(2),[-pi,pi])
                    ylim(haxes(1),[0,maxAmplitude])
                    xlim(haxes(1),[minFreq,maxFreq])
                    xlim(haxes(2),[minFreq,maxFreq])
                    
                    if r1 == self.R
                        set(haxes(2),'YTick', linspace(-pi+.2,pi-.2,5), ...
                            'YTickLabel',fix(100*linspace(-pi,pi,5))/100, ...
                            'TickLength',[0 0]);
                        ylabel(haxes(2),'Phase','FontSize',14);
                    else set(haxes(2),'YTick',[]);
                    end
                    
                    if r1 == 1,
                        set(haxes(1),'YTick',linspace(.05*maxAmplitude,.95*maxAmplitude,5), ...
                            'YTickLabel',fix(100*linspace(0,maxAmplitude,5)/100), ...
                            'TickLength',[0 0]);
                        ylabel(haxes(1),'Amplitude','FontSize',14)
                    else set(haxes(1),'YTick',[]); 
                    end

                    if  r1 == r2
                        text(.95*maxFreq,.95*maxAmplitude,names{r1},...
                            'FontSize',16, ...
                            'FontName','Ubuntu', ...
                            'FontWeight','bold', ...
                            'HorizontalAlignment','right', ...
                            'VerticalAlignment','top', ...
                            'Interpreter','latex');
                    end
                    
                    if r2 == self.R
                        xlabel(haxes(1),'Frequency','FontSize',14)
                        set(haxes(1),'TickLength',[0 0]);
                        set(haxes(2),'TickLength',[0 0]);
                    else set(haxes(1),'XTick',[]); set(haxes(2),'XTick',[]);
                    end
                end
            end
        end
        
        function ax = plotcsm(self,minFreq,maxFreq,r1,r2,styles,colors,holdax,ax)
            s= linspace(minFreq,maxFreq,1e3);
            
            UKU = self.UKU(s,1,0);
            allAmplitudes = abs(UKU);
            allPhases = wrapToPi(angle(UKU));
            
            maxAmplitude = max(allAmplitudes(:));
            
            amplitude = squeeze(allAmplitudes(r1,r2,:));
            phase = squeeze(allPhases(r1,r2,:));
            
            % replace phase in discontinuity function with NaNs
            [sp,phase] = util.removeDiscontinuities(s,phase);
            
            if holdax
                %colors = get(groot,'DefaultAxesColorOrder');
                hold(ax(1)); plot(ax(1),s,amplitude, ...
                    'LineStyle',styles{1},'Color',colors(1,:),'LineWidth',2);
                hold(ax(2)); plot(ax(2),sp,phase, ...
                    'LineStyle',styles{2},'Color',colors(2,:),'LineWidth',2);
            else
                [ax,h1,h2] = plotyy(s,amplitude,sp,phase);
                
                set(h1,'LineWidth',2,'LineStyle',styles{1},'Color',colors(1,:));
                set(h2,'LineWidth',2,'LineStyle',styles{2},'Color',colors(2,:));
                ylim(ax(2),[-pi,pi])
                ylim(ax(1),[0,maxAmplitude])
                xlim(ax(1),[minFreq,maxFreq])
                xlim(ax(2),[minFreq,maxFreq])
                
                set(ax(1),'YTick',linspace(.05*maxAmplitude,.95*maxAmplitude,5), ...
                    'YTickLabel',fix(100*linspace(0,maxAmplitude,5)/100), ...
                    'TickLength',[0 0],'YColor',colors(1,:));
                ylabel(ax(1),'Amplitude','FontSize',11,'Color',colors(1,:))
                
                set(ax(2),'YTick', linspace(-pi+.2,pi-.2,5), ...
                    'YTickLabel',fix(100*linspace(-pi,pi,5))/100, ...
                    'TickLength',[0 0],'YColor',colors(2,:));
                ylabel(ax(2),'Phase','FontSize',11,'Color',colors(2,:));
                               
                xlabel(ax(1),'Frequency','FontSize',11)
                set(ax(1),'TickLength',[0 0]);
                set(ax(2),'TickLength',[0 0]);
            end
        end
        
        % Make a copy of a handle object.
        function new = copy(self)
            % Instantiate new object of the same class.
            new = feval(class(self));
            
            % Copy all non-hidden properties.
            p = properties(self);
            for i = 1:length(p)
                new.(p{i}) = self.(p{i});
            end
        end
    end
    
    methods (Static)
        function [val,g,step] = rprop(val,g,g_old,step,minval,maxval,minstep,maxstep)
            %if ~exist('minval','var'), minval = -inf; maxval = inf; end
            
            if g*g_old > 0
                step = 1.2*step;
            elseif g*g_old < 0
                step = 0.5*step;
                g = 0;
            end
            
            step = max(min(step,maxstep),minstep);
            val = val + step*sign(g);
            if val > maxval || val < minval
                val = val - step*sign(g);
                step = 0.5*step;
                g = 0;
            end
        end
        
        function res = complexNormalCov(Gamma)
            res = 1/2*[real(Gamma) imag(-Gamma); imag(Gamma) real(Gamma)];
        end
        
        function res = getGradFast(B,w,A)
            w = reshape(w,[1,1,numel(w)]);
            dS = bsxfun(@times,B,w); dS = dS(:);
            dS2 = [real(dS);imag(-dS);imag(dS);real(dS)];
            res = 1/2*A(:)'*dS2;
        end
        
    end
end
