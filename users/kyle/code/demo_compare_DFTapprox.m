clear; format short e; close all;
set(0,'defaulttextinterpreter','none')

%% Set Parameters
gamma = 1;
Q = 1;
W = 100;
C = 1;
R = 1;

iters = 20;
GPiters = [0 0 50];
numSecondsVals = [.4:.2:2, 2.5:.5:5 6:8]; %[.5 1 2 3 5 10 15 20 25 30];%[.3 .4 .5 1 1.5 2 2.5 3 4 5];
meanVals = [.5 1 1.5 3]; %.5:.5:6;
highFreq = 100; %90
lowFreq = .1;

%% Run experiment
nnsv = numel(numSecondsVals);
nmv = numel(meanVals);

kl = zeros(nnsv,nmv,iters);
kltrue = zeros(nnsv,nmv);
entropytrue = zeros(nnsv,nmv);
fittime = zeros(nnsv,nmv,iters);

for nsind = 1:nnsv
    for meanind = 1:nmv
        for iter = 1:iters
            numSeconds = numSecondsVals(nsind);
            m = meanVals(meanind); 
            v = 1;
            
            fprintf('\n Run %d/%d for %1.1f seconds %1.1f mean ',iter,iters,numSeconds,m);
            
            % Generate Data
            sampRate = 200;
            N = round(sampRate*numSeconds);
            Qtrue = 1;
            
            GPkerneltrue = GP.LMC_DFT(C,Qtrue,R, ...
                GP.coregs.mats(C,Qtrue,R,{GP.coregs.matComplex(C,R,1,0,[0,100],[-inf,inf])}), ...
                GP.kernels.kernels(Qtrue,{GP.kernels.SG(m,v,[lowFreq,highFreq],[eps,1e4])}), ...
                gamma) ;
           
            tau = linspace(0,numSeconds,N);
            
            Ktrue = real(GPkerneltrue.K(tau));
            
            % draw data
            x = mvnrnd(zeros(C*N,1),Ktrue,W)';
            x_R = reshape(x,[N,C,W]);
            Ns = ceil(N/2);
            xfft = 1/sqrt(N)*fft(x_R);
            xfft = 2*(xfft(2:Ns+1,:,:));
            
            
            delta = tau(2) - tau(1);
            s = 1/(N*delta):1/(N*delta):1/(2*delta);
            
            
            % Update Model with DFT method
            GPkernelDFT = GP.LMC_DFT(C,Qtrue,R, ...
                GP.coregs.mats(C,Qtrue,R,{GP.coregs.matComplex(C,R)}), ...
                GP.kernels.kernels(Qtrue,{GP.kernels.SG(m,v,[lowFreq,highFreq],[eps,1e4])}), ...
                gamma) ;
            
            % update model
            t = tic;
            opts.iters = 200;
            opts.evalInterval = 250;
            plotOpts.plot = false;
            evals = algorithms.rprop(s,xfft,GPkernelDFT,opts,plotOpts);
            fittime(nsind,meanind,iter) = toc(t);
            
            % save results
            Kfit = real(GPkernelDFT.K(tau));
            Ktrue = real(GPkerneltrue.K(tau));
            s2 = [0,s,fliplr(s(1:end-1))];
            %U = 1/sqrt(N)*fft(eye(N)); G = kron(eye(C),U);
            %Kapprox = real(G'*GPkerneltrue.UKU(s2,0)*G);
            
            kl(nsind,meanind,iter) = 1/2*(2*sum(log(diag(chol(Ktrue)))) - ...
                2*sum(log(diag(chol(Kfit)))) - N*C + ...
                trace(Ktrue\Kfit))/(N*C);
            %kltrue(nsind,meanind) = 1/2*(2*sum(log(diag(chol(Ktrue)))) - ...
            %   2*sum(log(diag(chol(Kapprox)))) - N*C + ...
            %   trace(Ktrue\Kapprox))/(N*C);
            entropytrue(nsind,meanind) = N/2*(1+log(2*pi))+1/2*2*sum(log(diag(chol(Ktrue))));
        end
    end
end
%% Compare results
figure, imagesc(meanVals,numSecondsVals,kltrue), colorbar
ylabel('Seconds'); xlabel('Mean');

figure, imagesc(meanVals,numSecondsVals,mean(kl,3)), colorbar
ylabel('Seconds'); xlabel('Mean');

figure, imagesc(meanVals,numSecondsVals,entropytrue), colorbar
ylabel('Seconds'); xlabel('Mean');

figure(3), clf
hold on
colors = util.distinguishable_colors(4);
vals = [1 2 4];
for valind = 1:numel(vals)
    mv = vals(valind);
    errorbar(numSecondsVals,mean(kl(:,mv,:),3),std(kl(:,mv,:),0,3),'Color',colors(mv,:));
    xlim([0,numSecondsVals(end)])
    ylabel('KL Divergence','FontSize',14)
    xlabel('Series Length (seconds)','FontSize',14)
end
leg = legend('\mu = 0.5 \it{Hz}','\mu = 1 \it{Hz}', '\mu = 3 \it{Hz}',0);
set(leg,'FontSize',12)
hold off

save output_compareDFT

%figure, plot(numSecondsRuns,kl2), legend('DFT','Slow')
%
% figure(2), clf, imagesc(real(GPkernelDFT.K(tau))), colorbar;
% figure(3), clf, imagesc(real(GPkernelSlow.K(tau))), colorbar;
%
figure(4), clf, GPkernelDFT.plotpsd(0,50)
figure(5), clf, GPkerneltrue.plotpsd(0,50)
%
% figure(6), clf, plot(1:numel(logevidenceDFT),logevidenceDFT,'b-', ...
%     1:numel(logevidenceSlow),logevidenceSlow,'r-');
% legend('DFT Approximation','No Approximation',0)
