clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))

%% Set Parameters
dataOpts.numSeconds = 10;
dataOpts.highFreq = 300; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 1; %10
dataOpts.mouseName = 'MouseII006_010115';%'MouseGS007_090914';

Cvals = [1,3];


%% Load and preprocess Data
[X,regionNames] = loadStressData(dataOpts,'_PredictionIso_FIT_LFP');
N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

% take fourier transform
Ns = ceil(N/2);
Xfft = 1/sqrt(N)*fft(X);
Xfft = 2*(Xfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

% select subset of data for training
W = 20;

%xcorrfun = zeros(2*N-1,W);
xspec = [];%zeros(Ns,W);
offset = 5;
for w = offset:(W+offset)
    xt = X(:,Cvals,w);
    xtfft = Xfft(:,Cvals,w);

    %%
    %figure, plot(tau,xt)
    %figure, plot(s,abs(xtfft))

    %%
    lowFreq = 2.8;
    highFreq = 3.0;
    highpass = highFreq/sampRate*2;
    lowpass = lowFreq/sampRate*2;
    [butterb,buttera] = butter(2,[lowpass,highpass]);
    xtfilt = filtfilt(butterb,buttera,xt);
    xtfiltfft = 1/sqrt(N)*fft(xtfilt);
    xtfiltfft = 2*(xtfiltfft(2:Ns+1,:));

    %figure, plot(tau,xtfilt)
    %figure, plot(s,abs(xtfiltfft))



    [xspec(:,w-offset+1),F] = cpsd(xt(:,1),xt(:,2),[],[],[],sampRate);
    
    %xcorrtemp = xcorr(xt(:,1),xt(:,2));
    %xcorrfun(:,w-offset+1) = [xcorrtemp(N+1:end); xcorrtemp(1:N)];
    %xspectemp = fft(xcorrfun(:,w-offset+1));
    %xspec(:,w-offset+1) = xspectemp(2:Ns+1);

end

%%
close all
Nlow = find(F>0);
Nhigh = find(F>16)-1;
figure, plot(F(Nlow:Nhigh),smoothts(angle(xspec(Nlow:Nhigh,:))')', ...
    'color',[0,0,.2]+.6)
hold on
plot(F(Nlow:Nhigh),1/W*sum(smoothts(angle(xspec(Nlow:Nhigh,:))')',2), ...
    'color',[0,0,0],'LineWidth',2)
line([4 4],get(gca,'YLim'),'Color',[1 0 0])
line([8 8],get(gca,'YLim'),'Color',[1 0 0])
line([12 12],get(gca,'YLim'),'Color',[1 0 0])
set(gca,'XGrid','off','YGrid','on','GridLineStyle','--')
hold off
xlabel('Frequency (\it{Hz})','fontsize',12)
ylabel('Lag (rad)','fontsize',12)
title('Cross-Phase Spectrum','fontsize',14)

figure, plot(F(Nlow:Nhigh),smoothts(abs(xspec(Nlow:Nhigh,:))')', ...
    'color',[0,0,.2]+.6)
hold on
plot(F(Nlow:Nhigh),1/W*sum(smoothts(abs(xspec(Nlow:Nhigh,:))')',2), ...
    'color',[0,0,0],'LineWidth',2)
hold off
xlabel('Frequency (\it{Hz})','fontsize',12)
ylabel('Amplitude','fontsize',12)
set(gca,'ytick',[])
title('Cross-Amplitude Spectrum','fontsize',14)


figure, plot(tau,xt)
xlabel('Time (sec)','fontsize',12)
ylabel('Potential','fontsize',12)
set(gca,'ytick',[])
title('Raw LFP Data','fontsize',14)
legend('BLA','IL Cortex',0)