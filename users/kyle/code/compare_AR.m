clear; format short e; close all;
set(0,'defaulttextinterpreter','latex')

addpath(genpath('/home/kru2/Documents/data/neuroscience/lfp/StressData'))

%% Set Parameters
dataOpts.numSeconds = 3;
dataOpts.highFreq = 200; %90
dataOpts.lowFreq = 1.5;
dataOpts.sampFact = 2; %10
dataOpts.mouseName = 'MouseGS007_090914';

%Cvals = [1,3,5,6,7];
Cvals = [1,5,6];
%Cvals = 1;
C = numel(Cvals);

%% Load and preprocess Data
[X,regionNames] = loadStressData(dataOpts,'_PredictionIso_Homecage_LFP');
N = size(X,1); 
sampRate = N/dataOpts.numSeconds;

% take fourier transform
Ns = ceil(N/2);
Xfft = 1/sqrt(N)*fft(X);
Xfft = 2*(Xfft(2:Ns+1,:,:));
tau = 0:1/sampRate:(dataOpts.numSeconds-1/sampRate);
s = (sampRate/N):(sampRate/N):(sampRate/2);

% select subset of data for training
W = 1;
xt = X(:,Cvals,1:W);
xtfft = Xfft(:,Cvals,1:W);

figure, plot(xt(:,:,1))

%%
xt1 = xt(:,1,1);
p=2;
[b,~,~,~,sigma] = util.ar(xt1,p);

M = 30;
autocov_ar = zeros(M,1);
% autocov_ar(1) = sigma;
% for m = 2:M
%     k = min(p,m);
%     autocov_ar(m) = sum(b(1:k).*autocov_ar(m-1:-1:1));
% end

f = linspace(0,.5,Ns);
kf = bsxfun(@times,(1:p)',f);
specdens_ar = sigma./(abs(1-sum(bsxfun(@times,b,exp(-2i*pi*kf)),1).^2));
figure, plot(s,specdens_ar)


figure, plot(s,abs(xtfft(1:Ns,1,1)))



%% Create model
Q = 8;
R = 1*ones(Q,1);
gamma = 100;

% create Q Gaussian basis functions
%k = cellfun(@(l) GP.kernels.SE(l,[1,inf]),num2cell(1+rand(1,Q)), 'un', 0);

dataOpts.highFreq = 20;
k = cellfun(@(mu,var) GP.kernels.SG(mu,var,[dataOpts.lowFreq,dataOpts.highFreq],[eps,1e1]), ...
    num2cell(linspace(dataOpts.lowFreq,dataOpts.highFreq/2,Q)), ...
    num2cell(ones(1,Q)), 'un', 0);

kernels = GP.kernels.kernels(Q, k);

% generate kernels (weights and phase of basis functions) for each reagion
B = arrayfun(@(r)GP.coregs.matComplex(C,r),R,'un',0);
%B = arrayfun(@(r)GP.coregs.matReal(C,r),R,'un',0);
coregs = GP.coregs.mats(C,Q,R,B);

% form GP with CSM kernel
model = GP.LMC_DFT(C,Q,R,coregs,kernels,gamma);

% Fit model
opts.iters = 250;
opts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'CSM Kernel'};
figure; clf;

evals = algorithms.rprop(s,xtfft,model,opts,plotOpts);

figure, model.plotpsd(0,30,regionNames(Cvals))

%% Disaggregate the time-series data into spectral components
chans = [1,2,3];
%chans = 1;
[f,w] = model.disaggregate(tau,xt(:,:,1),chans);

muvals = arrayfun(@(q)model.kernels.k{q}.mu,1:Q);   
[muvals,inds] = sort(muvals);

f = reshape(f,[N,numel(chans),Q]);
f = f(:,:,inds);
w = w(:,inds);

colors = {'b-','r-','g-','g-'};
handles = [];
figure, 
for chan = 1:numel(chans)
    ha = plot(tau,[xt(:,chan,1),  ...
        1+bsxfun(@plus,1.5*(1:Q), ...
        bsxfun(@times,w(chan,:),squeeze(f(:,chan,:))))], colors{chan});
    hold on
    handles(chan) = ha(1);
end
hold off

ylabs = arrayfun(@(q)[num2str(muvals(q),3) ' Hz'],1:Q,'un',0);
yticklocs = [0,1+1.5*(1:Q)];
set(gca,'ytick',yticklocs,'yticklabel',{'LFP data',ylabs{:}});
xlabel('Time (seconds)')
legend(handles,regionNames(Cvals));
title('Disaggregation of LFP Data','Fontsize',14)


